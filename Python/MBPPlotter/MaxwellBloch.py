import numpy as np
from matplotlib import pyplot as plt
import qutip as qt
import mayavi.mlab as mlab
import FileOps as FO
from scipy.interpolate import interp1d
from tvtk.api import tvtk, write_data

def LOG(x):
    MIN = x[np.where(x != 0)].min()
    x[np.where(x == 0)] = MIN
    return np.log10(x)

def getRhoFiles(resultsDir, num, filter=""):
    searchFilter = "rho" + str(num) + filter
    return FO.getAFFilesOrderedByIter(resultsDir, searchFilter)

def getPolarizationFiles(resultsDir):
    return FO.getAFFilesOrderedByIter(resultsDir, "pol")

def getElectricFieldFiles(resultsDir):
    return FO.getAFFilesOrderedByIter(resultsDir, "E")

def plotDensityOperator(resultsDir, number, part, key):

    # Load simulation properties
    print("Getting files")
    NumberOfPoints, BoxLims, SSteps, TStep = FO.readMeshFile(resultsDir)
    Levels, Mu, Omega, Gamma = FO.readRhoProperties(resultsDir)

    # Get Rho Files
    rhoFiles = getRhoFiles(resultsDir, number, part)

    # Count iterations
    iterations = FO.getIterations(rhoFiles, "af")

    # Create grids for '1D' plots
    print("Creating meshgrids")
    X = SSteps[0] * np.arange(0, NumberOfPoints[0])
    Y = SSteps[1] * np.arange(0, NumberOfPoints[1])
    Z = SSteps[2] * np.arange(0, NumberOfPoints[2])
    T = TStep * iterations
    X, TX = np.meshgrid(X, T)
    Y, TY = np.meshgrid(Y, T)
    Z, TZ = np.meshgrid(Z, T)
    X = X.transpose()
    Y = Y.transpose()
    Z = Z.transpose()
    TX = TX.transpose()
    TY = TY.transpose()
    TZ = TZ.transpose()

    print("Loading arrays")

    # Get the results array
    rhoEvol = FO.readAFFileListToArray(rhoFiles, key)
    min = np.min(rhoEvol) if np.min(rhoEvol) > 0.0 else 0.0
    max = np.max(rhoEvol) if np.max(rhoEvol) < 1.0 else 1.0
    levels = np.linspace(min, max, 50)
    ticks = np.linspace(min, max, 4)

    # Plots
    print("Plotting")
    if(NumberOfPoints[1] == 1):
        # 1D case
        rhoEvol = rhoEvol.transpose()

        figX = plt.figure(key)
        plt.title(key + ' evolution')
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
        plot = plt.contourf(X, TX, rhoEvol, levels=levels, extend='both', cmap=plt.get_cmap("inferno"))
        figX.colorbar(plot, ticks=ticks)

    elif(NumberOfPoints[1] > 1 and NumberOfPoints[2] == 1):
        # 2D case
        rhoEvolX = rhoEvol[:, :, 0].transpose()
        rhoEvolY = rhoEvol[:, 0, :].transpose()

        figX = plt.figure("r11x")
        plt.title(r'$\rho_{11}$ evolution')
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
        plot = plt.contourf(X, TX, rhoEvolX, levels=levels, extend='both', cmap=plt.get_cmap("inferno"))
        figX.colorbar(plot, ticks=ticks)

        figY = plt.figure("r11y")
        plt.title(r'$\rho_{11}$ evolution')
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
        plot = plt.contourf(Y, TY, rhoEvolY, levels=levels, extend='both', cmap=plt.get_cmap("inferno"))
        figY.colorbar(plot, ticks=ticks)
    else:
        # 3D case
        rhoEvolX = rhoEvol[:, :, 0, 0].transpose()
        rhoEvolY = rhoEvol[:, 0, :, 0].transpose()

        figX = plt.figure("r11x")
        plt.title(r'$\rho_{11}$ evolution')
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
        plot = plt.contourf(X, TX, rhoEvolX, levels=levels, extend='both', cmap=plt.get_cmap("inferno"))
        figX.colorbar(plot, ticks=ticks)

        figY = plt.figure("r11y")
        plt.title(r'$\rho_{11}$ evolution')
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
        plot = plt.contourf(Y, TY, rhoEvolY, levels=levels, extend='both', cmap=plt.get_cmap("inferno"))
        figY.colorbar(plot, ticks=ticks)

    plt.show()

def plotDensityOperator1Atom(resultsDir, number, part, key):
    # Load simulation properties
    print("Getting files")
    NumberOfPoints, BoxLims, SSteps, TStep = FO.readMeshFile(resultsDir)
    Levels, Mu, Omega, Gamma = FO.readRhoProperties(resultsDir)

    # Get Rho Files
    rhoFiles = getRhoFiles(resultsDir, number, part)

    # Count iterations
    iterations = FO.getIterations(rhoFiles, "af")

    # Create grids for '1D' plots
    print("Creating meshgrids")
    T = TStep * iterations

    print("Loading arrays")
    # Get the results array
    rhoEvol = FO.readAFFileListToArray(rhoFiles, key)
    min = np.min(rhoEvol) if np.min(rhoEvol) > 0.0 else 0.0
    max = np.max(rhoEvol) if np.max(rhoEvol) < 1.0 else 1.0
    levels = np.linspace(min, max, 50)
    ticks = np.linspace(min, max, 4)

    # Plots
    print("Plotting")
    if(NumberOfPoints[1] == 1):
        # 1D case
        fig = plt.figure("centralatom_" + key)
        plt.title(r'$\rho_{22}$ Evolution')
        plt.plot(T, rhoEvol[:, int(NumberOfPoints[0]/2)])
        plt.xlabel("T(s)")
        plt.ylabel(r'$\rho_{22}$')

    plt.show()

def plotDensityOperatorABS(resultsDir, number, key):

    # Load simulation properties
    print("Getting files")
    NumberOfPoints, BoxLims, SSteps, TStep = FO.readMeshFile(resultsDir)
    rhoRealFiles = getRhoFiles(resultsDir, number, "real")
    rhoImagFiles = getRhoFiles(resultsDir, number, "imag")

    # Count iterations
    iterations = FO.getIterations(rhoRealFiles, "af")

    # Create grids for '1D' plots
    print("Creating meshgrids")
    X = SSteps[0] * np.arange(0, NumberOfPoints[0])
    Y = SSteps[1] * np.arange(0, NumberOfPoints[1])
    T = TStep * iterations
    X, TX = np.meshgrid(X, T)
    Y, TY = np.meshgrid(Y, T)
    X = X.transpose()
    Y = Y.transpose()
    TX = TX.transpose()
    TY = TY.transpose()

    print("Loading arrays")

    # Get the results array
    rhoRealEvol = FO.readAFFileListToArray(rhoRealFiles, key)
    rhoImagEvol = FO.readAFFileListToArray(rhoImagFiles, key)
    rhoEvol = np.abs(rhoRealEvol + 1.0j*rhoImagEvol)
    min = np.min(rhoEvol) if np.min(rhoEvol) > 0.0 else 0.0
    max = np.max(rhoEvol) if np.max(rhoEvol) < 1.0 else 1.0
    levels = np.linspace(min, max, 50)
    ticks = np.linspace(min, max, 4)

    # Plots
    print("Plotting")
    if(NumberOfPoints[1] == 1):
        # 1D case
        rhoEvol = rhoEvol.transpose()

        figX = plt.figure(key)
        plt.title(key + ' evolution')
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
        plot = plt.contourf(X, TX, rhoEvol, levels=levels, extend='both', cmap=plt.get_cmap("inferno"))
        figX.colorbar(plot, ticks=ticks)

    plt.show()

def plotDensityOperatorABS1Atom(resultsDir, number, key):

    # Load simulation properties
    print("Getting files")
    NumberOfPoints, BoxLims, SSteps, TStep = FO.readMeshFile(resultsDir)
    rhoRealFiles = getRhoFiles(resultsDir, number, "real")
    rhoImagFiles = getRhoFiles(resultsDir, number, "imag")

    # Count iterations
    iterations = FO.getIterations(rhoRealFiles, "af")

    # Create grids for '1D' plots
    print("Creating meshgrids")
    T = TStep * iterations

    # Get the results array
    print("Loading arrays")
    rhoRealEvol = FO.readAFFileListToArray(rhoRealFiles, key)
    rhoImagEvol = FO.readAFFileListToArray(rhoImagFiles, key)
    rhoEvol = np.abs(rhoRealEvol + 1.0j*rhoImagEvol)
    min = np.min(rhoEvol) if np.min(rhoEvol) > 0.0 else 0.0
    max = np.max(rhoEvol) if np.max(rhoEvol) < 1.0 else 1.0
    levels = np.linspace(min, max, 50)
    ticks = np.linspace(min, max, 4)

    # Plots
    print("Plotting")
    if(NumberOfPoints[1] == 1):
        # 1D case
        fig = plt.figure("1atom_"+key+"_abs")
        plt.title(r'$||\rho_{21}||$ Evolution')
        plt.plot(T, rhoEvol[:, int(NumberOfPoints[0] / 2)])
        plt.xlabel("T(s)")
        plt.ylabel(r'$||\rho_{21}||$')

    plt.show()

def plotDensityOperatorFFT1Atom(resultsDir, number, part, key):

    # Load simulation properties
    print("Getting files")
    NumberOfPoints, BoxLims, SSteps, TStep = FO.readMeshFile(resultsDir)
    rhoFiles = getRhoFiles(resultsDir, number, part)

    # Count iterations
    iterations = FO.getIterations(rhoFiles, "af")

    # Create grids for '1D' plots
    # print("Creating meshgrids")
    # T = TStep * iterations

    print("Loading arrays")
    rhoEvol = FO.readAFFileListToArray(rhoFiles, key)

    print("Calculating fft")
    rhoEvolFFT = np.fft.rfft(rhoEvol[:, int(NumberOfPoints[0]/2)] * np.hanning(iterations.shape[0]))
    freq = np.fft.rfftfreq(iterations.shape[0], (iterations[1]-iterations[0])*TStep)

    # Plots
    print("Plotting")
    if(NumberOfPoints[1] == 1):
        # 1D case
        fig = plt.figure(1)
        plt.title(r'$\rho_{11}$ Power Spectrum')
        plt.plot(freq, np.abs(rhoEvolFFT)**2)
        plt.grid()
        plt.xscale("log")
        plt.yscale("log")
        plt.xlabel("f(Hz)")
        plt.ylabel(r'$|\rho_{11}|^2$(f)')

    plt.show()

def plotElectricField(resultsDir):

    # Load simulation properties
    print("Getting files")
    NumberOfPoints, BoxLims, SSteps, TStep = FO.readMeshFile(resultsDir)

    eletricFieldFiles = getElectricFieldFiles(resultsDir)

    # Count iterations
    iterations = FO.getIterations(eletricFieldFiles, "af")

    # Create grids for '1D' plots
    print("Creating meshgrids")
    X = SSteps[0] * np.arange(0, NumberOfPoints[0])
    Y = SSteps[1] * np.arange(0, NumberOfPoints[1])
    Z = SSteps[2] * np.arange(0, NumberOfPoints[2])
    T = TStep * iterations
    X, TX = np.meshgrid(X, T)
    Y, TY = np.meshgrid(Y, T)
    Z, TZ = np.meshgrid(Z, T)
    X = X.transpose()
    Y = Y.transpose()
    Z = Z.transpose()
    TX = TX.transpose()
    TY = TY.transpose()
    TZ = TZ.transpose()

    # Get the results array
    print("Loading arrays")
    electricFieldEvolutionZ = FO.readAFFileListToArray(eletricFieldFiles, "E")
    levels = np.linspace(np.min(electricFieldEvolutionZ), np.max(electricFieldEvolutionZ), 50)
    ticks = np.linspace(np.min(electricFieldEvolutionZ), np.max(electricFieldEvolutionZ), 4)

    # Plot
    print("Plotting")
    if (NumberOfPoints[1] == 1):
        # 1D case
        EzEvolX = electricFieldEvolutionZ[:, :, 0, 0, 2].transpose()

        figX = plt.figure("E")
        plt.title(r'$E_{z}$ evolution')
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
        plot = plt.contourf(X, TX, EzEvolX, levels=levels, extend='both', cmap=plt.get_cmap("inferno"))
        figX.colorbar(plot, ticks=ticks)

    plt.show()

def electricField2D(resultsDir, saveFigDir):
    # Load simulation properties
    print("Getting files")
    NumberOfPoints, BoxLims, SSteps, TStep = FO.readMeshFile(resultsDir)

    eletricFieldFiles = getElectricFieldFiles(resultsDir)

    # Count iterations
    iterations = FO.getIterations(eletricFieldFiles, "af")

    # Create grids for '1D' plots
    print("Creating meshgrids")
    X = SSteps[0] * np.arange(0, NumberOfPoints[0])
    Y = SSteps[1] * np.arange(0, NumberOfPoints[1])
    T = TStep * iterations
    X, Y = np.meshgrid(X, Y)

   # Get the results array
    print("Loading arrays")
    electricFieldEvolutionZ = np.log(np.abs(FO.readAFFileListToArray(eletricFieldFiles, "E")))
    levels = np.linspace(np.min(electricFieldEvolutionZ), np.max(electricFieldEvolutionZ), 50)
    ticks = np.linspace(np.min(electricFieldEvolutionZ), np.max(electricFieldEvolutionZ), 4)

    for i in range(len(T)):
        figName = "E_" + str(i)
        fig = plt.figure(figName)
        plt.xlabel("x(m)")
        plt.ylabel("y(m)")
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
        plot = plt.contourf(X.transpose(), Y.transpose(), electricFieldEvolutionZ[i, :, :, 0, 2], levels=levels, extend='both', cmap=plt.get_cmap("inferno"))
        fig.colorbar(plot, ticks=ticks)
        plt.savefig(saveFigDir + "\\" + figName + ".png")

def electricField2DCutLine(resultsDir, saveFigDir):
    # Load simulation properties
    print("Getting files")
    NumberOfPoints, BoxLims, SSteps, TStep = FO.readMeshFile(resultsDir)

    eletricFieldFiles = getElectricFieldFiles(resultsDir)

    # Count iterations
    iterations = FO.getIterations(eletricFieldFiles, "af")

    # Create grids for '1D' plots
    print("Creating meshgrids")
    X = SSteps[0] * np.arange(0, NumberOfPoints[0])
    Y = SSteps[1] * np.arange(0, NumberOfPoints[1])
    T = TStep * iterations

   # Get the results array
    print("Loading arrays")
    electricFieldEvolutionZ = FO.readAFFileListToArray(eletricFieldFiles, "E")
    levels = np.linspace(np.min(electricFieldEvolutionZ), np.max(electricFieldEvolutionZ), 50)
    ticks = np.linspace(np.min(electricFieldEvolutionZ), np.max(electricFieldEvolutionZ), 4)

    for i in range(len(T)):
        figName = "E_" + str(i)
        fig = plt.figure(figName)
        plt.title("Ez(t="+str(T[i])+")")
        plt.xlabel("x(m)")
        plt.ylabel(r'Ez (V $\cdot m^{-1})$')
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
        plt.xlim(0, 1e-6)
        plt.plot(X, electricFieldEvolutionZ[i, :, int(NumberOfPoints[1]/2), 0, 2])
        plt.grid()
        plt.savefig(saveFigDir + "\\" + figName + ".png")
        plt.close()

def plotPolarization(resultsDir):
    # Load simulation properties
    print("Getting files")
    NumberOfPoints, BoxLims, SSteps, TStep = FO.readMeshFile(resultsDir)

    polarizationFiles = getPolarizationFiles(resultsDir)

    # Count iterations
    iterations = FO.getIterations(polarizationFiles, "af")

    # Create grids for '1D' plots
    print("Creating meshgrids")
    X = SSteps[0] * np.arange(0, NumberOfPoints[0])
    T = TStep * iterations
    X, T = np.meshgrid(X, T)

    # Get the results array
    print("Loading arrays")
    polarizationEvolutionZ = FO.readAFFileListToArray(polarizationFiles, "pol")
    levels = np.linspace(np.min(polarizationEvolutionZ), np.max(polarizationEvolutionZ), 50)
    ticks = np.linspace(np.min(polarizationEvolutionZ), np.max(polarizationEvolutionZ), 4)

    # Plot
    print("Plotting")
    print(X.transpose().shape)
    print(T.transpose().shape)
    print(polarizationEvolutionZ.shape)

    fig = plt.figure("polarization")
    plt.title("Polarization Evolution")
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))
    plot = plt.contourf(X.transpose(), T.transpose(), polarizationEvolutionZ, levels=levels, extend='both', cmap=plt.get_cmap("inferno"))
    fig.colorbar(plot, ticks=ticks)

    plt.show()

def getTrace(resultsDir, number1, number2, key1, key2):
    # Load simulation properties
    print("Getting files")
    NumberOfPoints, BoxLims, SSteps, TStep = FO.readMeshFile(resultsDir)

    rho11Files = getRhoFiles(resultsDir, number1, "real")
    rho22Files = getRhoFiles(resultsDir, number2, "real")

    # Count iterations
    iterations = FO.getIterations(rho11Files, "af")

    # Create grids for '1D' plots
    print("Creating meshgrids")
    T = TStep * iterations

    # Get the arrays
    print("Loading arrays")
    rho11Evolution = FO.readAFFileListToArray(rho11Files, key1)
    rho22Evolution = FO.readAFFileListToArray(rho22Files, key2)

    traceCentral = rho11Evolution[:, int(NumberOfPoints[0]/4)] + rho22Evolution[:, int(NumberOfPoints[0]/4)]

    return (T, traceCentral)

def plotTrace(TList, TraceList, LabelList):

    plt.figure("Trace")
    plt.xlabel("t (fs)")
    plt.ylabel(r'$\rho$ trace error ($\times 10^{-15}$)')
    plt.xlim(0.0, np.max(TList[0]) + (TList[0][1]-TList[0][0]))
    linestyleList=[":", "-.", "--", "-"]
    for i in range(len(TList)):
        plt.plot(TList[i], np.abs(1.0 - TraceList[i])*1e15, label=LabelList[i], color="black", linestyle=linestyleList[i])
    plt.grid()
    plt.legend()
    plt.show()

def plotOnBlochSphere(resultsDir):

    print("Getting files")
    NumberOfPoints, BoxLims, SSteps, TStep = FO.readMeshFile(resultsDir)
    rho11RealFiles = getRhoFiles(resultsDir, 0, "real")
    rho12RealFiles = getRhoFiles(resultsDir, 1, "real")
    rho21ImagFiles = getRhoFiles(resultsDir, 2, "imag")
    rho22RealFiles = getRhoFiles(resultsDir, 3, "real")

    # Count iterations
    iterations = FO.getIterations(rho11RealFiles, "af")

    # Create grids for '1D' plots
    print("Creating meshgrids")
    T = TStep * iterations

    # Get the results array
    print("Loading arrays")
    rho11RealEvol = FO.readAFFileListToArray(rho11RealFiles, "rho0")
    rho12RealEvol = FO.readAFFileListToArray(rho12RealFiles, "rho1")
    rho21ImagEvol = FO.readAFFileListToArray(rho21ImagFiles, "rho2")
    rho22RealEvol = FO.readAFFileListToArray(rho22RealFiles, "rho3")

    # Calculate Bloch vectors
    print("Calculating u v w")
    u = 2.0*np.array(rho12RealEvol[:, 25, 0, 0])
    v = 2.0*np.array(rho21ImagEvol[:, 25, 0, 0])
    w = np.array(rho11RealEvol[:, 25, 0, 0]) - np.array(rho22RealEvol[:, 25, 0, 0])

    print("Interpolating")
    N = 2
    TNew = np.linspace(0, T[-1], len(T)*N, endpoint=True)
    uNew = interp1d(T, u)(TNew)
    vNew = interp1d(T, v)(TNew)
    wNew = interp1d(T, w)(TNew)

    print("Getting array of interpolated points")
    LAux1 = np.arange(0, len(T)) * 4
    LAux2 = np.arange(0, len(TNew))
    interpIndex = []
    for i in LAux2:
        if not(i in LAux1):
            interpIndex.append(i)
    interpIndex = np.array(interpIndex)

    uInterp = uNew[interpIndex]
    vInterp = vNew[interpIndex]
    wInterp = wNew[interpIndex]

    print("Plotting on Bloch sphere")
    sphere = qt.Bloch()
    sphere.sphere_color = "white"
    sphere.point_color = ["blue", "red"]
    sphere.add_points([u, v, w])
    sphere.add_points([uInterp, vInterp, wInterp])
    sphere.show()

def plot3DElectricQuiver(resultsDir):
    # Load simulation properties
    print("Getting files")
    NumberOfPoints, BoxLims, SSteps, TStep = FO.readMeshFile(resultsDir)

    eletricFieldFiles = getElectricFieldFiles(resultsDir)

    # Count iterations
    iterations = FO.getIterations(eletricFieldFiles, "af")

    # Create grids for '1D' plots
    print("Creating meshgrids")
    X = SSteps[0] * np.arange(0, NumberOfPoints[0])
    Y = SSteps[1] * np.arange(0, NumberOfPoints[1])
    Z = SSteps[2] * np.arange(0, NumberOfPoints[2])
    X, Y, Z = np.meshgrid(X, Y, Z)
    X = np.asfortranarray(X).transpose((1, 0, 2))
    Y = np.asfortranarray(Y).transpose((1, 0, 2))
    Z = np.asfortranarray(Z).transpose((1, 0, 2))

    print("Reading files")
    E0 = FO.readAFFileListToArray(eletricFieldFiles, "E")[0]
    Ex = E0[:, :, :, 0]
    Ey = E0[:, :, :, 1]
    Ez = E0[:, :, :, 2]

    mlab.figure(fgcolor=(0., 0., 0.), bgcolor=(1, 1, 1))
    mlab.quiver3d(X, Y, Z, Ex, Ey, Ez, colormap='inferno')
    mlab.colorbar(orientation='v', nb_labels=2, label_fmt='%.1f')
    mlab.outline()
    mlab.axes(line_width=1.0)
    mlab.show()

def getElectricFieldArray(resultsDir, iteration):
    eletricFieldFiles = getElectricFieldFiles(resultsDir)
    file = ""
    for i in range(len(eletricFieldFiles)):
        if (str(iteration) in eletricFieldFiles[i]):
            file = eletricFieldFiles[i]
            break
    E = FO.readAFFileToArray(file, "E")
    return E

def getPolarizationArray(resultsDir, iteration):
    polarizationFiles = getPolarizationFiles(resultsDir)
    file = ""
    for i in range(len(polarizationFiles)):
        if (str(iteration) in polarizationFiles[i]):
            file = polarizationFiles[i]
            break
    P = FO.readAFFileToArray(file, "pol")
    return P

def getRhoArray(resultsDir, number, part, key, iteration):
    rhoFiles = getRhoFiles(resultsDir, number, part)
    file = ""
    for i in range(len(rhoFiles)):
        if (str(iteration) in rhoFiles[i]):
            file = rhoFiles[i]
            break
    Rho = FO.readAFFileToArray(file, key)
    return Rho

def plot3DElectricField(resultsDir, iteration, saveDir, multipliers = (1, 1, 1), axisUnits = ("m", "m", "m")):
    print("Getting files")
    NumberOfPoints, BoxLims, SSteps, TStep = FO.readMeshFile(resultsDir)

    print("Reading Array")
    E = getElectricFieldArray(resultsDir, iteration)

    print("Calculating magnitude")
    EMag = np.sqrt(np.power(E[:, :, :, 0], 2) + np.power(E[:, :, :, 1], 2) + np.power(E[:, :, :, 2], 2))

    print("Creating axis")
    XPts = SSteps[0] * np.arange(0, NumberOfPoints[0]) * multipliers[0]
    YPts = SSteps[1] * np.arange(0, NumberOfPoints[1]) * multipliers[1]
    ZPts = SSteps[2] * np.arange(0, NumberOfPoints[2]) * multipliers[2]

    print("Creating cut planes")
    EMagXCut = EMag[int(NumberOfPoints[0] / 2), :, :]
    EMagYCut = EMag[:, int(NumberOfPoints[1] / 2), :]
    EMagZCut = EMag[:, :, int(NumberOfPoints[2] / 2)]

    print("Plotting X Cut")
    Y, Z = np.meshgrid(YPts, ZPts)
    levels = np.linspace(0.0, np.max(EMagXCut), 100)
    ticks = np.linspace(0.0, np.max(EMagXCut), 4, endpoint=True)
    title = "E_X_Cut_"+str(iteration)
    plt.figure(title)
    plt.xlabel(r"y" + " (" + axisUnits[1] + ")")
    plt.ylabel(r"z" + " (" + axisUnits[2] + ")")
    if not(levels[1] > levels[0]):
        if (EMagXCut[0][0] > 0.0):
            levels = np.linspace(0.0, EMagXCut[0, 0], 100)
            ticks = np.linspace(0.0, EMagXCut[0, 0], 4, endpoint=True)
        elif (EMagXCut[0][0] < 0.0):
            levels = np.linspace(EMagXCut[0, 0], 0.0, 100)
            ticks = np.linspace(EMagXCut[0, 0], 0.0, 4, endpoint=True)
        else:
            levels = np.linspace(0.0, 1.0, 100)
            ticks = np.linspace(0.0, 1.0, 4, endpoint=True)
    plt.contourf(Y.transpose(), Z.transpose(), EMagXCut, extend='both', levels=levels, cmap=plt.get_cmap("inferno"))
    plt.colorbar(ticks=ticks, format='%.2e')
    plt.savefig(saveDir + "\\" + title + ".png")

    print("Plotting Y Cut")
    X, Z = np.meshgrid(XPts, ZPts)
    levels = np.linspace(0.0, np.max(EMagYCut), 100)
    ticks = np.linspace(0.0, np.max(EMagYCut), 4, endpoint=True)
    title = "E_Y_Cut_"+str(iteration)
    plt.figure(title)
    plt.xlabel(r"x" + " (" + axisUnits[0] + ")")
    plt.ylabel(r"z" + " (" + axisUnits[2] + ")")
    if not(levels[1] > levels[0]):
        if (EMagYCut[0][0] > 0.0):
            levels = np.linspace(0.0, EMagYCut[0, 0], 100)
            ticks = np.linspace(0.0, EMagYCut[0, 0], 4, endpoint=True)
        elif (EMagYCut[0][0] < 0.0):
            levels = np.linspace(EMagYCut[0, 0], 0.0, 100)
            ticks = np.linspace(EMagYCut[0, 0], 0.0, 4, endpoint=True)
        else:
            levels = np.linspace(0.0, 1.0, 100)
            ticks = np.linspace(0.0, 1.0, 4, endpoint=True)
    plt.contourf(X.transpose(), Z.transpose(), EMagYCut, extend='both', levels=levels, cmap=plt.get_cmap("inferno"))
    plt.colorbar(ticks=ticks, format='%.2e')
    plt.savefig(saveDir + "\\" + title + ".png")

    print("Plotting Z Cut")
    X, Y = np.meshgrid(XPts, YPts)
    levels = np.linspace(0.0, np.max(EMagZCut), 100)
    ticks = np.linspace(0.0, np.max(EMagZCut), 4, endpoint=True)
    title = "E_Z_Cut_"+str(iteration)
    plt.figure(title)
    plt.xlabel(r"x" + " (" + axisUnits[0] + ")")
    plt.ylabel(r"y" + " (" + axisUnits[1] + ")")
    if not(levels[1] > levels[0]):
        if (EMagZCut[0][0] > 0.0):
            levels = np.linspace(0.0, EMagZCut[0, 0], 100)
            ticks = np.linspace(0.0, EMagZCut[0, 0], 4, endpoint=True)
        elif (EMagZCut[0][0] < 0.0):
            levels = np.linspace(EMagZCut[0, 0], 0.0, 100)
            ticks = np.linspace(EMagZCut[0, 0], 0.0, 4, endpoint=True)
        else:
            levels = np.linspace(0.0, 1.0, 100)
            ticks = np.linspace(0.0, 1.0, 4, endpoint=True)
    plt.contourf(X.transpose(), Y.transpose(), EMagZCut, extend='both', levels=levels, cmap=plt.get_cmap("inferno"))
    plt.colorbar(ticks=ticks, format='%.2e')
    plt.savefig(saveDir + "\\" + title + ".png")

def plot3DPolarization(resultsDir, iteration, saveDir, multipliers = (1, 1, 1), axisUnits = ("m", "m", "m")):
    print("Getting files")
    NumberOfPoints, BoxLims, SSteps, TStep = FO.readMeshFile(resultsDir)

    print("Reading Array")
    P = getPolarizationArray(resultsDir, iteration)

    print("Calculating magnitude")
    PMag = np.sqrt(np.power(P[:, :, :, 0], 2) + np.power(P[:, :, :, 1], 2) + np.power(P[:, :, :, 2], 2))

    print("Creating axis")
    XPts = SSteps[0] * np.arange(0, NumberOfPoints[0]) * multipliers[0]
    YPts = SSteps[1] * np.arange(0, NumberOfPoints[1]) * multipliers[1]
    ZPts = SSteps[2] * np.arange(0, NumberOfPoints[2]) * multipliers[2]

    print("Creating cut planes")
    PMagXCut = PMag[int(NumberOfPoints[0] / 2), :, :]
    PMagYCut = PMag[:, int(NumberOfPoints[1] / 2), :]
    PMagZCut = PMag[:, :, int(NumberOfPoints[2] / 2)]

    print("Plotting X Cut")
    Y, Z = np.meshgrid(YPts, ZPts)
    levels = np.linspace(0.0, np.max(PMagXCut), 100)
    ticks = np.linspace(0.0, np.max(PMagXCut), 4, endpoint=True)
    title = "Pol_X_Cut_"+str(iteration)
    plt.figure(title)
    plt.xlabel(r"y" + " (" + axisUnits[1] + ")")
    plt.ylabel(r"z" + " (" + axisUnits[2] + ")")
    if not(levels[1] > levels[0]):
        if (PMagXCut[0][0] > 0.0):
            levels = np.linspace(0.0, PMagXCut[0, 0], 100)
            ticks = np.linspace(0.0, PMagXCut[0, 0], 4, endpoint=True)
        elif (PMagXCut[0][0] < 0.0):
            levels = np.linspace(PMagXCut[0, 0], 0.0, 100)
            ticks = np.linspace(PMagXCut[0, 0], 0.0, 4, endpoint=True)
        else:
            levels = np.linspace(0.0, 1.0, 100)
            ticks = np.linspace(0.0, 1.0, 4, endpoint=True)
    plt.contourf(Y.transpose(), Z.transpose(), PMagXCut, extend='both', levels=levels, cmap=plt.get_cmap("inferno"))
    plt.colorbar(ticks=ticks, format='%.2e')
    plt.savefig(saveDir + "\\" + title + ".png")

    print("Plotting Y Cut")
    X, Z = np.meshgrid(XPts, ZPts)
    levels = np.linspace(0.0, np.max(PMagYCut), 100)
    ticks = np.linspace(0.0, np.max(PMagYCut), 4, endpoint=True)
    title = "Pol_Y_Cut_"+str(iteration)
    plt.figure(title)
    plt.xlabel(r"x" + " (" + axisUnits[0] + ")")
    plt.ylabel(r"z" + " (" + axisUnits[2] + ")")
    if not(levels[1] > levels[0]):
        if (PMagYCut[0][0] > 0.0):
            levels = np.linspace(0.0, PMagYCut[0, 0], 100)
            ticks = np.linspace(0.0, PMagYCut[0, 0], 4, endpoint=True)
        elif (PMagYCut[0][0] < 0.0):
            levels = np.linspace(PMagYCut[0, 0], 0.0, 100)
            ticks = np.linspace(PMagYCut[0, 0], 0.0, 4, endpoint=True)
        else:
            levels = np.linspace(0.0, 1.0, 100)
            ticks = np.linspace(0.0, 1.0, 4, endpoint=True)
    plt.contourf(X.transpose(), Z.transpose(), PMagYCut, extend='both', levels=levels, cmap=plt.get_cmap("inferno"))
    plt.colorbar(ticks=ticks, format='%.2e')
    plt.savefig(saveDir + "\\" + title + ".png")

    print("Plotting Z Cut")
    X, Y = np.meshgrid(XPts, YPts)
    levels = np.linspace(0.0, np.max(PMagZCut), 100)
    ticks = np.linspace(0.0, np.max(PMagZCut), 4, endpoint=True)
    title = "Pol_Z_Cut_"+str(iteration)
    plt.figure(title)
    plt.xlabel(r"x" + " (" + axisUnits[0] + ")")
    plt.ylabel(r"y" + " (" + axisUnits[1] + ")")
    if not(levels[1] > levels[0]):
        if (PMagZCut[0][0] > 0.0):
            levels = np.linspace(0.0, PMagZCut[0, 0], 100)
            ticks = np.linspace(0.0, PMagZCut[0, 0], 4, endpoint=True)
        elif (PMagZCut[0][0] < 0.0):
            levels = np.linspace(PMagZCut[0, 0], 0.0, 100)
            ticks = np.linspace(PMagZCut[0, 0], 0.0, 4, endpoint=True)
        else:
            levels = np.linspace(0.0, 1.0, 100)
            ticks = np.linspace(0.0, 1.0, 4, endpoint=True)
    plt.contourf(X.transpose(), Y.transpose(), PMagZCut, extend='both', levels=levels, cmap=plt.get_cmap("inferno"))
    plt.colorbar(ticks=ticks, format='%.2e')
    plt.savefig(saveDir + "\\" + title + ".png")

def plot3DRho(resultsDir, number, part, key, iteration, saveDir, multipliers = (1, 1, 1), axisUnits = ("m", "m", "m")):
    print("Getting files")
    NumberOfPoints, BoxLims, SSteps, TStep = FO.readMeshFile(resultsDir)

    print("Reading Array")
    Rho = getRhoArray(resultsDir, number, part, key, iteration)

    print("Creating axis")
    XPts = SSteps[0] * np.arange(0, NumberOfPoints[0]) * multipliers[0]
    YPts = SSteps[1] * np.arange(0, NumberOfPoints[1]) * multipliers[1]
    ZPts = SSteps[2] * np.arange(0, NumberOfPoints[2]) * multipliers[2]

    print("Creating cut planes")
    RhoXCut = Rho[int(NumberOfPoints[0] / 2), :, :]
    RhoYCut = Rho[:, int(NumberOfPoints[1] / 2), :]
    RhoZCut = Rho[:, :, int(NumberOfPoints[2] / 2)]

    print("Plotting X Cut")
    Y, Z = np.meshgrid(YPts, ZPts)
    levels = np.linspace(0.0, np.max(RhoXCut), 100)
    ticks = np.linspace(0.0, np.max(RhoXCut), 4, endpoint=True)
    title = "Rho"+str(number)+"_X_Cut_"+str(iteration)
    plt.figure(title)
    plt.xlabel(r"y" + " (" + axisUnits[1] + ")")
    plt.ylabel(r"z" + " (" + axisUnits[2] + ")")
    if not(levels[1] > levels[0]):
        if (RhoXCut[0][0] > 0.0):
            levels = np.linspace(0.0, RhoXCut[0, 0], 100)
            ticks = np.linspace(0.0, RhoXCut[0, 0], 4, endpoint=True)
        elif (RhoXCut[0][0] < 0.0):
            levels = np.linspace(RhoXCut[0, 0], 0.0, 100)
            ticks = np.linspace(RhoXCut[0, 0], 0.0, 4, endpoint=True)
        else:
            levels = np.linspace(0.0, 1.0, 100)
            ticks = np.linspace(0.0, 1.0, 4, endpoint=True)
    plt.contourf(Y.transpose(), Z.transpose(), RhoXCut, extend='both', levels=levels, cmap=plt.get_cmap("inferno"))
    plt.colorbar(ticks=ticks, format='%.2e')
    plt.savefig(saveDir + "\\" + title + ".png")

    print("Plotting Y Cut")
    X, Z = np.meshgrid(XPts, ZPts)
    levels = np.linspace(0.0, np.max(RhoYCut), 100)
    ticks = np.linspace(0.0, np.max(RhoYCut), 4, endpoint=True)
    title = "Rho"+str(number)+"_Y_Cut_"+str(iteration)
    plt.figure(title)
    plt.xlabel(r"x" + " (" + axisUnits[0] + ")")
    plt.ylabel(r"z" + " (" + axisUnits[2] + ")")
    if not(levels[1] > levels[0]):
        if (RhoYCut[0][0] > 0.0):
            levels = np.linspace(0.0, RhoYCut[0, 0], 100)
            ticks = np.linspace(0.0, RhoYCut[0, 0], 4, endpoint=True)
        elif (RhoYCut[0][0] < 0.0):
            levels = np.linspace(RhoYCut[0, 0], 0.0, 100)
            ticks = np.linspace(RhoYCut[0, 0], 0.0, 4, endpoint=True)
        else:
            levels = np.linspace(0.0, 1.0, 100)
            ticks = np.linspace(0.0, 1.0, 4, endpoint=True)
    plt.contourf(X.transpose(), Z.transpose(), RhoYCut, extend='both', levels=levels, cmap=plt.get_cmap("inferno"))
    plt.colorbar(ticks=ticks, format='%.2e')
    plt.savefig(saveDir + "\\" + title + ".png")

    print("Plotting Z Cut")
    X, Y = np.meshgrid(XPts, YPts)
    levels = np.linspace(0.0, np.max(RhoZCut), 100)
    ticks = np.linspace(0.0, np.max(RhoZCut), 4, endpoint=True)
    title = "Rho"+str(number)+"_Z_Cut_"+str(iteration)
    plt.figure(title)
    plt.xlabel(r"x" + " (" + axisUnits[0] + ")")
    plt.ylabel(r"y" + " (" + axisUnits[1] + ")")
    if not(levels[1] > levels[0]):
        if (RhoZCut[0][0] > 0.0):
            levels = np.linspace(0.0, RhoZCut[0, 0], 100)
            ticks = np.linspace(0.0, RhoZCut[0, 0], 4, endpoint=True)
        elif (RhoZCut[0][0] < 0.0):
            levels = np.linspace(RhoZCut[0, 0], 0.0, 100)
            ticks = np.linspace(RhoZCut[0, 0], 0.0, 4, endpoint=True)
        else:
            levels = np.linspace(0.0, 1.0, 100)
            ticks = np.linspace(0.0, 1.0, 4, endpoint=True)
    plt.contourf(X.transpose(), Y.transpose(), RhoZCut, extend='both', levels=levels, cmap=plt.get_cmap("inferno"))
    plt.colorbar(ticks=ticks, format='%.2e')
    plt.savefig(saveDir + "\\" + title + ".png")

def saveElectricFieldToVTK(resultsDir, iteration, saveDir):
    # Load simulation properties
    print("Getting files")
    NumberOfPoints, BoxLims, SSteps, TStep = FO.readMeshFile(resultsDir)

    print("Reading Array")
    E = getElectricFieldArray(resultsDir, iteration)

    # Create grids for '1D' plots
    print("Creating meshgrids")
    X = SSteps[0] * np.arange(0, NumberOfPoints[0])
    Y = SSteps[1] * np.arange(0, NumberOfPoints[1])
    Z = SSteps[2] * np.arange(0, NumberOfPoints[2])
    X, Y, Z = np.meshgrid(X, Y, Z)
    X = X.transpose((1, 0, 2))
    Y = Y.transpose((1, 0, 2))
    Z = Z.transpose((1, 0, 2))
    gridPoints = np.array([X.ravel(), Y.ravel(), Z.ravel()], order='f').transpose()

    print("Saving VTK files")
    Ex = E[:, :, :, 0]
    Ey = E[:, :, :, 1]
    Ez = E[:, :, :, 2]
    vectors = np.array([Ex.ravel(), Ey.ravel(), Ez.ravel()], order='f').transpose()
    scalars = np.sqrt(np.power(Ex, 2) + np.power(Ey, 2) + np.power(Ez, 2)).ravel()
    sg = tvtk.StructuredGrid(dimensions=X.shape, points=gridPoints)
    sg.point_data.vectors = vectors
    sg.point_data.scalars = scalars
    sg.point_data.vectors.name = 'E'
    sg.point_data.scalars.name = 'EMag'
    write_data(sg, saveDir + '\\E_'+str(iteration)+'.vtk')

def savePolarizationToVTK(resultsDir, iteration, saveDir):
    # Load simulation properties
    print("Getting files")
    NumberOfPoints, BoxLims, SSteps, TStep = FO.readMeshFile(resultsDir)

    print("Reading Array")
    P = getPolarizationArray(resultsDir, iteration)

    # Create grids for '1D' plots
    print("Creating meshgrids")
    X = SSteps[0] * np.arange(0, NumberOfPoints[0])
    Y = SSteps[1] * np.arange(0, NumberOfPoints[1])
    Z = SSteps[2] * np.arange(0, NumberOfPoints[2])
    X, Y, Z = np.meshgrid(X, Y, Z)
    X = X.transpose((1, 0, 2))
    Y = Y.transpose((1, 0, 2))
    Z = Z.transpose((1, 0, 2))
    gridPoints = np.array([X.ravel(), Y.ravel(), Z.ravel()], order='f').transpose()

    print("Saving VTK files")
    Px = P[:, :, :, 0]
    Py = P[:, :, :, 1]
    Pz = P[:, :, :, 2]
    vectors = np.array([Px.ravel(), Py.ravel(), Pz.ravel()], order='f').transpose()
    scalars = np.sqrt(np.power(Px, 2) + np.power(Py, 2) + np.power(Pz, 2)).ravel()
    sg = tvtk.StructuredGrid(dimensions=X.shape, points=gridPoints)
    sg.point_data.vectors = vectors
    sg.point_data.scalars = scalars
    sg.point_data.vectors.name = 'P'
    sg.point_data.scalars.name = 'PMag'
    write_data(sg, saveDir + '\\Pol_'+str(iteration)+'.vtk')

def saveRhoToVTK(resultsDir, number, part, key, iteration, saveDir):

    # Load simulation properties
    print("Getting files")
    NumberOfPoints, BoxLims, SSteps, TStep = FO.readMeshFile(resultsDir)

    print("Reading Array")
    Rho = getRhoArray(resultsDir, number, part, key, iteration)

    # Create grids for '1D' plots
    print("Creating meshgrids")
    X = SSteps[0] * np.arange(0, NumberOfPoints[0])
    Y = SSteps[1] * np.arange(0, NumberOfPoints[1])
    Z = SSteps[2] * np.arange(0, NumberOfPoints[2])
    X, Y, Z = np.meshgrid(X, Y, Z)
    X = X.transpose((1, 0, 2))
    Y = Y.transpose((1, 0, 2))
    Z = Z.transpose((1, 0, 2))
    gridPoints = np.array([X.ravel(), Y.ravel(), Z.ravel()], order='f').transpose()

    print("Saving VTK files")
    sg = tvtk.StructuredGrid(dimensions=X.shape, points=gridPoints)
    sg.point_data.scalars = Rho.ravel()
    sg.point_data.scalars.name = 'Rho'
    write_data(sg, saveDir + '\\Rho'+str(number)+'_'+str(iteration)+'.vtk')

