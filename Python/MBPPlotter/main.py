import sys

import FileOps as FO
import MaxwellBloch as MB
import numpy as np

if __name__ == '__main__':
    # Flag for debug mode
    debug = True

    # Get command line arguments for load path
    loadDir = ""
    if not debug:
        if len(sys.argv) < 2:
            sys.exit("Invalid number of arguments")
        else:
            loadDir = sys.argv[1]
    else:
        # loadDir = ".\\Results"
        loadDir = "E:\\Joao Paulo\\Results\\Maxwell-Bloch-3D-Thin2"

    # MB.plotDensityOperator(loadDir, 0, "real", "rho0")
    # MB.plotDensityOperator1Atom(loadDir, 3, "real", "rho3")
    # MB.plotDensityOperatorABS(loadDir, 2, "rho2")
    # MB.plotDensityOperatorABS1Atom(loadDir, 2, "rho2")
    # MB.plotDensityOperatorFFT1Atom(loadDir, 0, "real", "rho0")
    # MB.plotElectricField(loadDir)
    # MB.electricField2D(loadDir, "C:\\Users\\jpcc0\\Google Drive\\Faculdade\\5o Ano\\Tese\\Resultados\\AOP17\\reflection\\new")
    # MB.electricField2DCutLine(loadDir, "C:\\Users\\jpcc0\\Google Drive\\Faculdade\\5o Ano\\Tese\\Resultados\\AOP17\\reflection\\cut")
    # MB.plotPolarization(loadDir)
    # MB.plotOnBlochSphere(loadDir)
    # MB.saveElectricFieldToVTK(loadDir, 0, "E:\\Joao Paulo\\Results")
    # MB.savePolarizationToVTK(loadDir, 0, "E:\\Joao Paulo\\Results")
    # MB.saveRhoToVTK(loadDir, 0, "real", "rho0", 0, "E:\\Joao Paulo\\Results\\Maxwell-Bloch-VTK")
    MB.plot3DElectricField(loadDir, 0, "E:\\Joao Paulo\\Results\\Maxwell-Bloch-3D-Thin2-PNG", (1.0e9, 1.0e6, 1.0e9), ("nm", "$\mu m$", "nm"))
    MB.plot3DPolarization(loadDir, 0, "E:\\Joao Paulo\\Results\\Maxwell-Bloch-3D-Thin2-PNG", (1.0e9, 1.0e6, 1.0e9), ("nm", "$\mu m$", "nm"))
    MB.plot3DRho(loadDir, 0, "real", "rho0", 0, "E:\\Joao Paulo\\Results\\Maxwell-Bloch-3D-Thin2-PNG", (1.0e9, 1.0e6, 1.0e9), ("nm", "$\mu m$", "nm"))


