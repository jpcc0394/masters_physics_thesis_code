import glob
import sys
import numpy as np
import arrayfire as af

def getTextFiles(loadDir, filter = None):
    files = []
    if filter is None:
        files = glob.glob(loadDir + "\\*.txt")
    elif type(filter) is list:
        for f in filter:
            files += glob.glob(loadDir + "\\" + f + "*.txt")
    else:
        files = glob.glob(loadDir + "\\" + filter + "*.txt")
    return files

def getAFFiles(loadDir, filter = None):
    files = []
    if filter is None:
        files = glob.glob(loadDir + "\\*.af")
    elif type(filter) is list:
        for f in filter:
            files += glob.glob(loadDir + "\\" + f + "*.af")
    else:
        files = glob.glob(loadDir + "\\" + filter + "*.af")
    return files

def getTextFilesOrderedByIter(loadDir, filter):
    fileList = getTextFiles(loadDir, filter)
    return sorted(fileList, key=getTxtFileIter)

def getAFFilesOrderedByIter(loadDir, filter):
    fileList = getAFFiles(loadDir, filter)
    return sorted(fileList, key=getAFFileIter)

def getTxtFileIter(filePath):
    return np.int32(filePath.split('_')[-1].split(".txt")[0])

def getAFFileIter(filePath):
    return np.int32(filePath.split('_')[-1].split(".af")[0])

def getIterations(fileList, ext):
    if not(type(fileList) is list):
        sys.exit("Must provide a list")
    else:
        iterations = []
        for fileName in fileList:
            if(ext == "txt"):
                iterations.append(getTxtFileIter(fileName))
            if (ext == "af"):
                iterations.append(getAFFileIter(fileName))
        return np.array(list(map(np.int32, iterations)))

def readMeshFile(resultsPath):

    # Load simulation Mesh properties file
    meshFilePath = getTextFiles(resultsPath, "Mesh")[0]
    meshFileLines = readTxtFileLines(meshFilePath)

    # Read lines with properties
    numberOfPoints3D = np.array(list(map(np.int32, meshFileLines[0].split(" "))))
    simulationBoxLims = np.array(list(map(np.float64, meshFileLines[1].split(" "))))
    spatialSteps = np.array(list(map(np.float64, meshFileLines[2].split(" "))))
    timeStep = np.float64(meshFileLines[3])

    # Return tuple with values
    return (numberOfPoints3D, simulationBoxLims, spatialSteps, timeStep)

def readRhoProperties(resultsPath):
    # Load simulation rho properties file
    propertiesFilePath = getTextFiles(resultsPath, "rho_properties")[0]
    propertiesFileLines = readTxtFileLines(propertiesFilePath)

    # Variables to store properties
    numberOfLevels = 0
    muMatrix = []
    omegaMatrix = []
    gammaMatrix = []

    # Read file into arrays
    auxCounter = 0
    for line in propertiesFileLines:
        # Ignore empty lines
        if line != "\n":
            if auxCounter == 0:
                # Reading number of levels
                numberOfLevels = np.int32(line)
            elif auxCounter == 1:
                # Reading mu matrix
                muMatrix.append(np.array(list(map(np.float64, line.split(" ")))))
            elif auxCounter == 2:
                # Reading omega matrix
                omegaMatrix.append(np.array(list(map(np.float64, line.split(" ")))))
            elif auxCounter == 3:
                # Reading gamma matrix
                gammaMatrix.append(np.array(list(map(np.float64, line.split(" ")))))
        else:
            auxCounter += 1

    # Recast into arrays
    muMatrix = np.array(muMatrix)
    omegaMatrix = np.array(omegaMatrix)
    gammaMatrix = np.array(gammaMatrix)

    # Return tuple with results
    return (numberOfLevels, muMatrix, omegaMatrix, gammaMatrix)

def readTxtFileLines(fileDir):
    fileLines = []
    with open(fileDir) as F:
        for line in F:
            fileLines.append(line)
        F.close()
    return fileLines

def readTxtFileToArray(fileDir):
    arrayValues = []
    arrayShape = []
    with open(fileDir) as F:
        firstLine = True
        for line in F:
            if firstLine == True:
                arrayShape = list(map(np.int32, line.split(" ")))
                firstLine = False
            else:
                arrayValues.append(line)
        F.close()
    return np.array(list(map(np.float32, arrayValues))).reshape(arrayShape, order='F')

def readAFFileToArray(fileDir, key):
    return np.array(af.array.read_array(fileDir, key=key), order='F')

def readTxtFileListToArray(filesDir):
    array = []
    for file in filesDir:
        array.append(readTxtFileToArray(file))
    return np.array(array)

def readAFFileListToArray(filesDir, key):
    array = []
    for file in filesDir:
        array.append(readAFFileToArray(file, key))
    return np.array(array)
