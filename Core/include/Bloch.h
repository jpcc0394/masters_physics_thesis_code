#pragma once

#include <arrayfire.h>
#include <vector>
#include <cmath>
#include <fstream>

#include <TensorField.h>
#include <Mesh.h>
#include <units.h>
#include <Matrix.h>
#include <Misc.h>

class Bloch;

enum class DipoleDir
{
	X,
	Y,
	Z
};

class Bloch {

public:

	int levels;
	int dim;
	double eta;
	YeeMesh& mesh;
	TensorField Rho;
	Field dipoleVersor;

private:
	double alpha;
	double beta;
	int renormalizeInterval;
	int internalCounter;
	Matrix muMatrix;
	Matrix gammaMatrix;
	Matrix omegaMatrix;

public:

	/*
	Bloch class constructor. Access "rhoSchrodingerPicture" to get current density operator status and "polarization" to get the current polarization
	Inside the class all values are converted to natural units

	@param YeeMesh& mesh								Simulation's Yee Mesh
	@param int levels									Number of atomic levels
	@param double eta									Atomic density
	@param vector<vector<double>> mu					Matrix with the transition dipole moments. Must be square matrix
	@param vector<vector<double>> gamma					Matrix with the spontaneous decay frequencies. Must be square matrix
	@param vector<vector<double>> omega					Matrix with the transition frequencies. Must be square matrix
	@param vector<vector<double>> baseChangeOmega		Array with picture change frequencies
	@param vector<vector<double>> polTerms				Array with the interaction terms for the calculation of the polarization
	*/
	Bloch(int levels, 
		double eta,
		double polAlpha,
		double polBeta,
		YeeMesh& mesh,
		std::vector<std::vector<double>> mu,
		std::vector<std::vector<double>> gamma, 
		std::vector<std::vector<double>> omega,
		int renormalizeInterval = 1);

	/*
	Bloch instance initialization. Assign the initial values for each level
	Initializes the interaction and schrodinger pictures and also the polarization

	@param vector<vector<double>> initValues		Matrix with the initialization values. initValues[i][0] for the real part and initValues[i][1] for the imaginary part
	*/
	void Initialize(const std::vector< std::vector<double> > initValues, const DipoleDir initialPolDir );

	/*
	Method that evolves the density operator.

	@param Field& EField	Current electric field in natural units
	@param double t			Current time instant
	*/
	void push(Field& EFieldPrev, Field& EField, const bool RK4 = false, const bool interpolation = false);

	void push(Field& EFieldPrev, Field& EField, Field& VelocityPrev, Field& Velocity, const bool RK4 = false, const bool interpolation = false);

	/*
	Methods for updating the polarization
	*/

	Field calculatePolarization(std::vector<double> polTerms);

	Field calculatePolarization(const Field& AtomicDensity, std::vector<double> polTerms);

	af::array dipolePotential(Field& EField, std::vector<double> dipoleForceTerms);

	Field dipoleForce(Field& EField, std::vector<double> dipoleForceTerms, FieldType fieldType = FieldType::FACE);

	void saveProperties(const std::string resultsDir, const bool SI = false);

	void saveToAF(const std::string resultsDir, std::vector<bool> saveReal, const std::vector<bool> saveImag, const af::dtype precision = f32);

	void savePolToAF(const std::string resultsDir, std::vector<double> polTerms, const bool SI = false, const af::dtype precision = f32);

	void saveState(std::string saveDir);

	void loadState(std::string loadDir);

private:

	/*
	Method for calculating the dynamic matrix in the master equation

	@param af::array EField		Auxiliary eletric field values
	@param double t				Current time instant
	*/
	TensorField updateDynamicMatrix(const Field& EField, const Field& dipoleVersor);

	TensorField dislocationEffect(const Field& Velocity);

	/*
	Auxiliary method that evaluates the equation system that describes the Bloch equation
	It resets the dynamicMatrix and calculates returns a TensorField with the result of
	dot{rho} = dynamicMatrix*rho + gammaMAtrix*rho
	
	@param TensorField rho		TensorField for the calculation
	@param Field EField			Electric field to calculate the dynamicMatrix
	@param double t				Temporal instant
	*/
	TensorField rhoEquationSystem(const TensorField& rho, const Field& dipoleVersor, const Field& EField);

	TensorField rhoEquationSystem(const TensorField& rho, const Field& dipoleVersor, const Field& EField, const Field& Velocity);

	Field dipoleVersorEquationSystem(const Field& versor, const Field& EField);

	void RK2(Field& EFieldPRev, Field& EFieldNExt, const bool interpolation = false);

	void RK2(Field& EFieldPRev, Field& EFieldNExt, Field& VelocityPrev, Field& VelocityNext, const bool interpolation = false);

	void RK4(Field& EFieldPRev, Field& EFieldNExt, const bool interpolation = false);

	void RK4(Field& EFieldPRev, Field& EFieldNExt, Field& VelocityPrev, Field& VelocityNext, const bool interpolation = false);

	void IntegrationStep(Field& EFieldPrev, Field& EField, const bool RK4, const bool interpolation);

	void IntegrationStep(Field& EFieldPrev, Field& EField, Field& VelocityPrev, Field& Velocity, const bool RK4, const bool interpolation);
};