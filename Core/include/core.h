#pragma once

#include <arrayfire.h>
#include <iostream>
#include <cmath>
#include <Windows.h>
#include <string.h>
#include <fstream>
#include <functional>

#include <Mesh.h>
#include <PIC.h>
#include <EMF.h>
#include <units.h>
#include <Bloch.h>
#include <Points.h>
#include <Matrix.h>
#include <TensorField.h>
#include <Misc.h>
#include <CudaMisc.h>