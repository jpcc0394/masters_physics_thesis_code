#pragma once

#include <units.h>
#include <definitions.h>


class YeeMesh;
class Field;
class Boundary;


enum class Grid {
	CENTER,
	FACE_X, 
	FACE_Y,
	FACE_Z,
	EDGE_X,
	EDGE_Y,
	EDGE_Z
};

enum class BoundaryAxis {
	X_MIN,
	Y_MIN,
	Z_MIN,
	X_MAX,
	Y_MAX,
	Z_MAX
};

class YeeMesh {

public:
	Units units;
	const int Nx;
	const int Ny;
	const int Nz;
	const double Lx;
	const double Ly;
	const double Lz;
	const double dx;
	const double dy;
	const double dz;
	const double dt;
	af::dtype afType;

	/*
	Yee mesh class constructor

	@param Nx Number of points in X direction
	@param Ny Number of points in Y direction
	@param Nz Number of points in Z direction
	@param dx Point spacing in X direction
	@param dy Point spacing in Y direction
	@param dz Point spacing in Z direction
	@param dt Temporal integration step
	@return Null
	*/
	YeeMesh(Units units, int Nx, int Ny, int Nz, double dx, double dy, double dz, double dt, af::dtype afType = f64);

	YeeMesh(int Nx, int Ny, int Nz, double dx, double dy, double dz, double dt, af::dtype afType = f64);

	bool operator==(YeeMesh& mesh2);

	af::array x(Grid fieldType);

	af::array y(Grid fieldType);

	af::array z(Grid fieldType);

	void save(const std::string resultsDir, const bool SI = false);

	inline af::dtype dtype() {
		return this->afType;
	}
};

////////////////////////// Class definitions //////////////////////////

enum class FieldType {
	FACE,
	EDGE,
	CENTER,
	CENTER_VECTOR
};

enum class FieldDir {
	X_P,
	X_N,
	Y_P,
	Y_N,
	Z_P,
	Z_N
};

class Field {

public:
	FieldType type;
	af::array values;
	YeeMesh& mesh;

	/*
	Field class constructor

	@param type		Field type - FACE | EDGE | CENTER | CENTER_VECTOR
	@param values	ArrayFirre array for field initialization
	@param mesh		Pointer to YeeMesh object containing simulation scenario
	*/
	Field(FieldType type, af::array values, YeeMesh& mesh);

	/*
	Force af::array evaluation
	*/
	inline void eval() {
		this->values.eval();
	}

	inline bool isempty() {
		return this->values.isempty();
	}

	inline af::dtype dtype() {
		return this->values.type();
	}

	/*
	Function for quick casting / interpolating Fields
	
	@param FieldType type	Desired output type
	*/
	Field as(const FieldType type) const;

	/*
	Function that returns the shifted version of Field
	on the desired direction

	@param FieldDir shiftDir	Direction of the shift
	*/
	Field shift(const FieldDir shiftDir) const;

	/*
	Function that returns the shifted version of Field

	@param FieldDir shiftDir	Direction of the shift
	*/
	Field allShift(const int direction) const;

	/*
	Function to clear the field values
	*/
	void clear();

	void operator=(Field& rhs);

};

////////////////////////// Field operator overloads //////////////////////////
Field operator+(Field& lhs, Field& rhs);

Field operator+(Field& lhs, double rhs);

Field operator+(double lhs, Field& rhs);

void operator+=(Field& lhs, Field& rhs);

void operator+=(Field& lhs, double rhs);

Field operator-(Field& lhs, Field& rhs);

Field operator-(Field& lhs, double rhs);

Field operator-(double lhs, Field& rhs);

void operator-=(Field& lhs, Field& rhs);

void operator-=(Field& lhs, double rhs);

Field operator*(Field& lhs, Field& rhs);

Field operator*(Field& lhs, double rhs);

Field operator*(double lhs, Field& rhs);

void operator*=(Field& lhs, Field& rhs);

void operator*=(Field& lhs, double rhs);

Field operator/(Field& lhs, Field& rhs);

Field operator/(Field& lhs, double rhs);

Field operator/(double lhs, Field& rhs);

void operator/=(Field& lhs, Field& rhs);

void operator/=(Field& lhs, double rhs);

enum class BoundaryType
{
	PERIODIC,
	ABC,
	REFLECTIVE,
	UPML
};

typedef struct ABCBoundaries {
	double L, C;
	int POW;
} ABCBoundary;

typedef struct UPMLBoundaries {
	double L, Sigma, K;
	int POW;
} UPMLBoundary;

union BoundaryUnion {
	char Periodic;
	ABCBoundary ABC;
	char Reflective;
	UPMLBoundary UPML;
};

class Boundary {

public:
	Boundary();
	Boundary(BoundaryType Type, BoundaryAxis Axis, BoundaryUnion BoundaryU);
	BoundaryType Type();
	BoundaryUnion Properties();
	BoundaryAxis Axis();
	af::array Values(YeeMesh &Mesh, Grid GridPos);
	af::array ABCFMin(af::array AxisPoints, double AxisL);
	af::array ABCFMax(af::array AxisPoints, double AxisL);
	af::array UPMLKMax(af::array AxisPoints, double AxisL);
	af::array UPMLSigmaMax(af::array AxisPoints, double AxisL);
	af::array UPMLKMin(af::array AxisPoints, double AxisL);
	af::array UPMLSigmaMin(af::array AxisPoints, double AxisL);

private:
	BoundaryType M_Type;
	BoundaryUnion M_BoundaryU;
	BoundaryAxis M_BoundaryAxis;
};