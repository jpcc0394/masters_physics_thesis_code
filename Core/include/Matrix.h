#pragma once

#include <TensorField.h>
#include <Mesh.h>
#include <vector>

class Matrix;

class Matrix {

private:
	class Proxy {
	public:
		Proxy(std::vector<double> column);
		double operator[](int index);
	private:
		std::vector<double> column;
	};

public:

	int rows;
	int cols;
	std::vector<std::vector<double>> values;

	/*
	Matrix object constructor

	@param std::vector<std::vector<double>> values	Initialization values. Must be two dimensional
	*/
	Matrix(const std::vector<std::vector<double>> values);

	/*
	Function to clear the matrix values
	*/
	void clear();

	// Operators overload

	Proxy operator[] (int index) const;

	void operator= (const Matrix& rhs);

	Matrix operator+ (const Matrix& rhs) const;
	Matrix operator+ (const double rhs) const;

	Matrix operator- (const Matrix& rhs) const;
	Matrix operator- (const double rhs) const;

	Matrix operator* (const Matrix& rhs) const;
	Matrix operator* (const double rhs) const;
	TensorField operator* (const TensorField& rhs) const;

	Matrix operator/ (const double rhs) const;

	void operator+= (const Matrix& rhs);
	void operator+= (const double rhs);

	void operator-= (const Matrix& rhs);
	void operator-= (const double rhs);

	void operator*= (const double rhs);

	void operator/= (const double rhs);

};

Matrix operator+ (const double lhs, const Matrix& rhs);

Matrix operator- (const double lhs, const Matrix& rhs);

Matrix operator* (const double lhs, const Matrix& rhs);

Matrix operator/ (const double lhs, const Matrix& rhs);