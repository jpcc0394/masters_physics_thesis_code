#pragma once

#include <arrayfire.h>
#include <iostream>
#include <functional>
#include <Mesh.h>
#include <units.h>
#include <Misc.h>


class EMF;
class LinePlotParam;
class PlanePlotParam;
class FieldProperties;


enum class PropagationDir
{
	X_P,
	X_N,
	Y_P,
	Y_N,
	Z_P,
	Z_N
};

enum class EFieldPol
{
	X,
	Y,
	Z
};

class LinePlotParam {
public:
	af::Window& window;
	int direction;
	int idx1;
	int idx2; 
	int component; 
	int skipRepr;

	LinePlotParam(af::Window& window, int direction, int idx1, int idx2, int component, int skipRepr) : window(window), direction(direction), idx1(idx1), idx2(idx2), component(component), skipRepr(skipRepr) {}
};

class PlanePlotParam {
public:
	af::Window& window;
	int perpendicularDirection;
	int idx;
	int component;
	int skipRepr;


	PlanePlotParam(af::Window& window, int perpendicularDirection, int idx, int component, int skipRepr) : window(window), perpendicularDirection(perpendicularDirection), idx(idx), component(component), skipRepr(skipRepr) {}
};

class FieldProperties {
public:
	int EDim, BDim;
	int ESign, BSign;
	Grid EGrid, BGrid;
	af::array(YeeMesh::*MeshFunc)(Grid); // Pointer for the function to call


	/*
	This class calculates the properties on the electro-magnetic field
	in order to have propaggtion along the given axis and with the desired
	electric field polarization

	@param PropagationDir	direction	Axis of electro-magnetic field propagation
	@param EFIeldPol		EPol		Axis of electric field polarization
	*/
	FieldProperties(const PropagationDir direction, const EFieldPol EPol);
};

class EMF {

public:
	YeeMesh& mesh;
	bool savePrevE;
	bool savePrevB;
	bool savePrevD;
	bool savePrevH;
	int internalCounter;
	std::vector<LinePlotParam> linePlots;
	std::vector<PlanePlotParam> planePlots;
	bool boundariesLocked;
	bool externalLocked;
	bool hasUPML;
	bool hasMultiplicative;

private:
	Field m_Efield;
	Field m_EfieldPrev;
	Field m_Bfield;
	Field m_BfieldPrev;
	Field m_Dfield;
	Field m_DfieldPrev;
	Field m_Hfield;
	Field m_HfieldPrev;

	Boundary XAxisMinBoundary;
	Boundary XAxisMaxBoundary;
	Boundary YAxisMinBoundary;
	Boundary YAxisMaxBoundary;
	Boundary ZAxisMinBoundary;
	Boundary ZAxisMaxBoundary;
	af::array MultiplicativeBoundaryFace;
	af::array MultiplicativeBoundaryEdge;
	/// NEVER EVER CHANGE THESE NAMES
	std::vector< std::vector< fieldFunctionTimeDep > > m_ExternalCurrent;
	std::vector< std::vector< fieldFunctionTimeDep > > m_ExternalEfield;
	std::vector< std::vector< fieldFunctionTimeDep > > m_ExternalBfield;

public:
	EMF(YeeMesh& mesh, bool savePrevE = false, bool savePrevB = false, bool savePrevD = false, bool savePrevH = false);

	void addGaussianPulsePlaneWave(PropagationDir direction, EFieldPol EPol, double r0, double waveNumber, double phase, double FWHM, double intensity);

	void addPlaneWave(PropagationDir direction, EFieldPol EPol, double waveNumber, double phase, double intensity);

	void push();

	void push(Field& Jfield, Field& Pfield, Field& Mfield);

	void borisPush(Field& Jfield, Field& Pfield, Field& Mfield, Field& Charge);

	void initialize();

	void initialize(Field& chargeDist, Field& Jfield, Field& Pfield, Field& Mfield);

	void addLinePlot(af::Window& window, int direction, int idx1, int idx2, int component, int skipRepr);

	void addPlanePlot(af::Window& window, int perpendicularDirection, int idx, int component, int skipRepr);

	void plotLine(LinePlotParam& params);

	void plotPlane(PlanePlotParam& params);

	void plotAll();

	void saveToTxt(const std::string saveDir, const bool SI = false);

	void saveToAF(const std::string saveDir, const std::vector<bool> fieldSelector, const bool SI = false, const af::dtype precision = f32, FieldType fieldType = FieldType::CENTER_VECTOR);

	void saveState(const std::string saveDir);

	void loadState(const std::string loadDir);

	void addBoundaryCondition(Boundary Bound);

	double gaussLawElectricVerify(Field chargeDensity);

	double gaussLawMagneticVerify();

	double chargeContinuityVerify(Field chargeDensity, Field chargeDensityPrev, Field currentDensity);

	Field currentSources(double t = NAN);

	/// NEVER EVER CHANGE THESE NAMES
	void addExternalCurrent(fieldFunctionTimeDep Fx, fieldFunctionTimeDep Fy, fieldFunctionTimeDep Fz);

	void addExternalEfield(fieldFunctionTimeDep Fx, fieldFunctionTimeDep Fy, fieldFunctionTimeDep Fz);

	void addExternalBfield(fieldFunctionTimeDep Fx, fieldFunctionTimeDep Fy, fieldFunctionTimeDep Fz);
	/// YOU CAN CHANGE NOW

	Field Efield(bool numeric = false);

	Field EfieldPrev(bool numeric = false);

	Field Bfield(bool numeric = false);

	Field BfieldPrev(bool numeric = false);

	Field Dfield(bool numeric = false);

	Field DfieldPrev(bool numeric = false);

	Field Hfield(bool numeric = false);

	Field HfieldPrev(bool numeric = false);

private:
	void pushElectric(Field& Jfield, Field& Pfield);
	void pushMagnetic(Field& Mfield, double dtFactor = 1.0);
	void getMultiplicativeBoundaryValues();
	void applyMultiplicativeBoundaries();
	af::array KX(Grid GridPos);
	af::array SigmaX(Grid GridPos);
	af::array KY(Grid GridPos);
	af::array SigmaY(Grid GridPos);
	af::array KZ(Grid GridPos);
	af::array SigmaZ(Grid GridPos);
};