#pragma once

#include <Mesh.h>
#include <arrayfire.h>

class TensorField;

enum class TensorFieldType {
	CENTER_MATRIX
};

class TensorField {

private:
	class Proxy {
	public:
		Proxy(const std::vector<af::array> column);
		af::array operator[](const int index) const;
	private:
		std::vector<af::array> column;
	};

public:
	TensorFieldType type;
	int rows;
	int cols;
	YeeMesh& mesh;
	std::vector<std::vector<af::array>> values;

	/*
	TensorField class constructor

	@param type		Tensor field type CENTER_MATRIX
	@param rows		Number of matrix rows
	@param cols		Number of matrix columms
	@param mesh		Pointer to YeeMesh object containing simulation scenario
	*/
	TensorField(const TensorFieldType type, const int rows, const int cols, YeeMesh& mesh);

	/*
	Function that assigns an array fire array to a matrix position

	@param row		Row index
	@param col		Columm index
	@param v		Array fire array
	*/
	void setValue(const int row, const int col, const af::array v);

	/*
	Function that forces the arithemetic evalution of the tensor field values
	*/
	void eval() const ;

	/*
	Function to clear the field values
	*/
	void clear();

	// Overloads declaration

	Proxy operator[] (const int index) const;
	
	void operator= (const TensorField& rhs);

	TensorField operator+ (const TensorField& rhs) const;
	TensorField operator+ (const double rhs) const;
	TensorField operator+ (const Field& rhs) const;

	TensorField operator- (const TensorField& rhs) const;
	TensorField operator- (const double rhs) const;
	TensorField operator- (const Field& rhs) const;

	TensorField operator* (const TensorField& rhs) const;
	TensorField operator* (const double rhs) const;
	TensorField operator* (const Field& rhs) const;

	TensorField operator/ (const double rhs) const;
	TensorField operator/ (const Field& rhs) const;

	void operator+= (const TensorField& rhs);
	void operator+= (const double rhs);
	void operator+= (const Field& rhs);

	void operator-= (const TensorField& rhs);
	void operator-= (const double rhs);
	void operator-= (const Field& rhs);

	void operator*= (const double rhs);
	void operator*= (const Field& rhs);

	void operator/= (const double rhs);
	void operator/= (const Field& rhs);
};

// Overload when other type perform arithemetics with TensorField

TensorField operator+ (const double lhs, const TensorField& rhs);
TensorField operator+ (const Field& lhs, const TensorField& rhs);

TensorField operator- (const double lhs, const TensorField& rhs);
TensorField operator- (const Field& lhs, const TensorField& rhs);

TensorField operator* (const double lhs, const TensorField& rhs);
TensorField operator* (const Field& lhs, const TensorField& rhs);

TensorField operator/ (const double lhs, const TensorField& rhs);
TensorField operator/ (const Field& lhs, const TensorField& rhs);