#pragma once

#include <arrayfire.h>
#include <Mesh.h>
#include <PIC.h>
#include <Points.h>
#include <string.h>
#include <ctime>
#include <fstream>
#include <omp.h>
#include <limits>
#include <chrono>
#include <random>
#include <math.h>
#include <omp.h>


af::array fftfreq(int n, double d = 1.0, af::dtype type = f64);

af::array meshgrid(af::array x, af::array y, af::array z);

int pythonLikeMod(int lhs, int rhs);

//af::array pythonLikeMod(af::array lhs, int rhs);

af::array pythonLikeMod(af::array lhs, double rhs);

void print3D(af::array in, char *arrayName = "");

Field zeros(YeeMesh& mesh, FieldType type, af::dtype dtype);

af::array mulByComplexExp(af::array values, double arg);

Field div(Field& source, FieldType outputType);

Field grad(Field& in, FieldType outputType);

/*
Function that calculates the curl of mesh fields

@param source Field class object field to be interpolated
@param outputType Field Type of the output
@return Field class object of the given type containing the calculated curl
*/
Field curl(Field& source, FieldType output_type);

/*
Function that interpolates mesh fields

@param source Field class object field to be interpolated
@param outputType Output Field type
@return Output Field object
*/
Field interpolate(const Field& source, const FieldType outputType);

//PointsQuantities interpolate(Field& source, PointsPositions& positions);

af::array poissonSolverAux(af::array in, double dx, double dy, double dz);

Field poissonSolver(Field& in);

#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif

Field gaussLawSolver(Field& in, FieldType returnType, double stopingParameter = 1e-14, double timeLimitSecs = std::numeric_limits<double>::max());

/*
Calculates the finite diffrences of center fields along the given dimension

@param source Field class object to be differentiated
@param output
*/
void diff_center(Field& source, Field& output, int axis);

Field calculateFieldVersor(const Field& inputField);

af::array normalizeForImage(af::array in);

af::array compress(af::array in, int ratio0, int ratio1, int ratio2, int ratio3);

Field cross(const Field& lhs, const Field& rhs, FieldType outputType = FieldType::CENTER_VECTOR);

Field magnitude(Field& in);

/*
Function to calculate the current date and time

@param bool	useLocalTime	Choose to use local time or UTC
*/
std::string getCurrentDateTime(bool useLocalTime);

/*
Function to generate a directory path to save results

@param std::string	preamble	Preamble for the generated file path
*/
std::string generateDirectoryPath(std::string preamble = "", bool temp = false);

/*
Function to generate create a directory

@param std::string	directoryPath	Path to desired directory
*/
bool MKDirectory(std::string directoryPath);

bool saveAFValuesOld(const std::string filePath, const af::array values, const int precision = -1);

af::array normalizeVector(const af::array in);

af::array relativeError(af::array exactVals, af::array calcVals);

af::dtype afTypeSelector(af::dtype lhs, af::dtype rhs);

double integrate(Field& field);

double min(Field& field);

double max(Field& field);

af::array distributionSampling(Field& field, int n, af::dtype dtype = f64);

bool saveAFValuesToFile(std::string path, af::array in, const int precision = -1);

void atomicIndexAdd(af::array& out, af::array values, af::array i, af::array j = af::array(), af::array k = af::array(), af::array l = af::array());

bool saveAFValuesWithAF(const std::string filePath, const std::string key, af::array values);

Boundary makeABCBoundary(BoundaryAxis Axis, double L, double C, int POW = 2);

Boundary makeUPMLBoundary(BoundaryAxis Axis, double L, double Sigma, double KMax, int Pow);

double maxwellJuttnerDistributionGen(double T, double k, double m, double c);

af::array maxwellJuttnerDistributionGen(af::array T, double k, double m, double c);

fieldFunction fieldFunctionFromField(Field& field);