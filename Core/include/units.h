#pragma once

#include <definitions.h>
#include <vector>
#include <cmath>


enum class UnitsNames {
	LENGTH,
	MASS,
	TIME,
	TEMPERATURE,
	CHARGE
};


enum class UnitSystems {
	SI,
	PLANCK,
	STONEY,
	HARTREE,
	RYDBERG,
	NATURAL_LH,
	NATURAL_GAUSS,
};


class Units {
private:
	double m_pi;
	double m_c;
	double m_hbar;
	double m_e;
	double m_G;
	double m_kb;
	double m_me;
	double m_eps0;
	double m_mu0;
	
	double CL;
	double CM;
	double CT;
	double CK;
	double CC;

public:
	Units(UnitSystems unitSystem);

	Units(double lenghtScale, double massScale, double timeScale, double temperatureScale, double chargeScale);

	inline double pi() { return this->m_pi; }
	inline double c() { return this->m_c; }
	inline double hbar() { return this->m_hbar; }
	inline double e() { return this->m_e; }
	inline double G() { return this->m_G; }
	inline double kb() { return this->m_kb; }
	inline double me() { return this->m_me; }
	inline double eps0() { return this->m_eps0; }
	inline double mu0() { return this->m_mu0; }

	static double si_c();
	static double si_hbar();
	static double si_e();
	static double si_G();
	static double si_kb();
	static double si_me();
	static double si_eps0();
	static double si_mu0();

	double scalingFactorToNatural(std::vector<UnitsNames> unitNames, std::vector<int> powers);

	double scalingFactorToSI(std::vector<UnitsNames> unitNames, std::vector<int> powers);

	template<typename T> T convertToNatural(T value, std::vector<UnitsNames> unitNames, std::vector<int> powers) {
		return value * scalingFactorToNatural(unitNames, powers);
	}

	template<typename T> T convertToSI(T value, std::vector<UnitsNames> unitNames, std::vector<int> powers) {
		return value * scalingFactorToSI(unitNames, powers);
	}
};