#include <units.h>
#include <cmath>


#define ALPHA 0.007297352566417
#define ALPHA_G 1.7516876e-45
#define SI_HBAR 1.05457180013e-34
#define SI_C 299792458.0
#define SI_G 6.6740831e-11
#define SI_KB 1.3806485279e-23
#define SI_EPS0 8.8541878176e-12
#define SI_MU0 1.256637061435917295385e-6
#define SI_E 1.602176620898e-19
#define SI_ME 9.1093835611e-31

Units::Units(UnitSystems unitSystem) {
	this->m_pi = PI;

	switch (unitSystem) {

	case UnitSystems::SI:
		this->m_c = SI_C;
		this->m_hbar = SI_HBAR;
		this->m_e = SI_E;
		this->m_G = SI_G;
		this->m_kb = SI_KB;
		this->m_me = SI_ME;
		this->m_eps0 = SI_EPS0;
		this->m_mu0 = SI_MU0;
		this->CL = 1.0;
		this->CM = 1.0;
		this->CT = 1.0;
		this->CK = 1.0;
		this->CC = 1.0;
		break;

	case UnitSystems::NATURAL_LH:
		this->m_c = 1.0;
		this->m_hbar = 1.0;
		this->m_e = std::sqrt(4.0*PI*ALPHA);
		this->m_G = ALPHA_G/(SI_ME*SI_ME);
		this->m_kb = 1.0;
		this->m_me = 510998.946113;
		this->m_eps0 = 1.0;
		this->m_mu0 = 1.0;
		this->CL = SI_E/(SI_HBAR*SI_C);
		this->CM = SI_C*SI_C/SI_E;
		this->CT = SI_E/SI_HBAR;
		this->CK = SI_KB/SI_E;
		this->CC = std::sqrt(4.0*PI*ALPHA)/SI_E;
		break;

	case UnitSystems::HARTREE:
		this->m_c = 1.0/ALPHA;
		this->m_hbar = 1.0;
		this->m_e = 1.0;
		this->m_G = ALPHA_G/ALPHA;
		this->m_kb = 1.0;
		this->m_me = 1.0;
		this->m_eps0 = 1.0;
		this->m_mu0 = 1.0;
		#define SI_HBAR2 (SI_HBAR*SI_HBAR)
		#define SI_E2 (SI_E*SI_E)
		#define	SI_E4 (SI_E2*SI_E2)
		#define SI_4PIEPS0 (4.0*PI*SI_EPS0)
		this->CL = SI_ME*SI_E2/(SI_HBAR2*SI_4PIEPS0);
		this->CM = 1.0/SI_ME;
		this->CT = SI_ME*SI_E4/(SI_HBAR2*SI_HBAR*SI_4PIEPS0*SI_4PIEPS0);
		this->CK = SI_HBAR2*SI_4PIEPS0*SI_4PIEPS0*SI_KB/(SI_ME*SI_E4);
		this->CC = 1.0/SI_E;
		#undef SI_HBAR2		
		#undef SI_E2
		#undef SI_E4
		#undef SI_4PIEPS0
		break;

	default:
		throw std::invalid_argument("Units class initialization  ->  Not implemented for that unit system.\n");
		break;
	}
}

Units::Units(double lenghtScale, double massScale, double timeScale, double temperatureScale, double chargeScale) {
	double factors[] = { lenghtScale, massScale, timeScale, temperatureScale, chargeScale };

	for (int i = 0; i < 5; i++)
		if (factors[i] <= 0)
			throw std::invalid_argument("Units::Units(double lenghtFactor, double massFactor, double timeFactor, double temperatureFactor, double chargeFactor)  ->  All factors must be strictly positive numbers.\n");

	this->CL = 1.0/lenghtScale;
	this->CM = 1.0/massScale;
	this->CT = 1.0/timeScale;
	this->CK = 1.0/temperatureScale;
	this->CC = 1.0/chargeScale;

	this->m_pi   = PI;
	this->m_c    = SI_C    * this->CL/this->CT;
	this->m_hbar = SI_HBAR * this->CL*this->CL*this->CM/this->CT;
	this->m_e    = SI_E    * this->CC;
	this->m_G    = SI_G    * this->CL*this->CL*this->CL/(this->CM*this->CT*this->CT);
	this->m_kb   = SI_KB   * this->CL*this->CL*this->CM/(this->CT*this->CT*this->CK);
	this->m_me   = SI_ME   * this->CM;
	this->m_eps0 = SI_EPS0 * this->CC*this->CL*this->CT*this->CT/(this->CM*this->CL*this->CL*this->CL);
	this->m_mu0  = SI_MU0  * this->CM*this->CL/(this->CC*this->CC);
}

double Units::si_c()    { return SI_C;    }
double Units::si_hbar() { return SI_HBAR; }
double Units::si_e()    { return SI_E;    }
double Units::si_G()    { return SI_G;    }
double Units::si_kb()   { return SI_KB;   }
double Units::si_me()   { return SI_ME;   }
double Units::si_eps0() { return SI_EPS0; }
double Units::si_mu0()  { return SI_MU0;  }

#undef ALPHA
#undef ALPHA_G
#undef SI_HBAR
#undef SI_C
#undef G_CONST
#undef SI_KB
#undef SI_EPS0
#undef SI_MU0
#undef SI_E
#undef SI_ME

double Units::scalingFactorToNatural(std::vector<UnitsNames> unitNames, std::vector<int> powers) {
	if (unitNames.size() != powers.size())
		throw std::invalid_argument("double Units::scalingFactorToNatural(std::vector<UnitsNames> unitNames, std::vector<int> powers)  ->  Vector sizes don't match.\n");
	else {

		double factor = 1.0;

		for (int i = 0; i < unitNames.size(); i++) {

			switch (unitNames[i]) {
			case UnitsNames::LENGTH:
				factor *= std::pow(this->CL, powers[i]);
				break;
			case UnitsNames::MASS:
				factor *= std::pow(this->CM, powers[i]);
				break;
			case UnitsNames::TIME:
				factor *= std::pow(this->CT, powers[i]);
				break;
			case UnitsNames::TEMPERATURE:
				factor *= std::pow(this->CK, powers[i]);
				break;
			case UnitsNames::CHARGE:
				factor *= std::pow(this->CC, powers[i]);
				break;
			default:
				break;
				throw std::invalid_argument("Units.convertValue - That unit aint cannon bruh!");
			}

		}
		return factor;
	}
}

double Units::scalingFactorToSI(std::vector<UnitsNames> unitNames, std::vector<int> powers) {
	return 1.0/scalingFactorToNatural(unitNames, powers);
}
