#include <Misc.h>


void print3D(af::array in, char *arrayName) {
	std::cout << std::string(arrayName) << std::endl;
	for (int i = 0; i < in.dims(0); i++) {
		for (int j = 0; j < in.dims(1); j++) {
			for (int k = 0; k < in.dims(2); k++) {
				if (in.type() == c32)
					std::cout << i << ", " << j << ", " << k << " -> " << af::real(in(i, j, k)).scalar<float>() << " + " << af::imag(in(i, j, k)).scalar<float>() << "i" << std::endl;
				else if (in.type() == c64)
					std::cout << i << ", " << j << ", " << k << " -> " << af::real(in(i, j, k)).scalar<double>() << " + " << af::imag(in(i, j, k)).scalar<double>() << "i" << std::endl;
				else if (in.type() == f32)
					std::cout << i << ", " << j << ", " << k << " -> " << in(i, j, k).scalar<float>() << std::endl;
				else if (in.type() == f64)
					std::cout << i << ", " << j << ", " << k << " -> " << in(i, j, k).scalar<double>() << std::endl;
			}
		}
	}
	std::cout << std::endl;
}

Field zeros(YeeMesh& mesh, FieldType type, af::dtype afType) {
	switch (type) {

	case FieldType::CENTER:
		return Field(type, af::constant(0.0, mesh.Nx, mesh.Ny, mesh.Nz, 1, afType), mesh);
		break;

	case FieldType::CENTER_VECTOR:
	case FieldType::FACE:
	case FieldType::EDGE:
		return Field(type, af::constant(0.0, mesh.Nx, mesh.Ny, mesh.Nz, 3, afType), mesh);
		break;

	default:
		throw std::invalid_argument("zeros - Wtf son?");
		break;
	}
}

int pythonLikeMod(int lhs, int rhs) {
	return (lhs % rhs + rhs) % rhs;
}

//af::array pythonLikeMod(af::array lhs, int rhs) {
//	return (lhs % rhs + rhs) % rhs;
//}

af::array pythonLikeMod(af::array lhs, double rhs) {
	return (lhs % rhs + rhs) % rhs;
}

af::array trilinearInterpolation(af::array in, af::array x, af::array y, af::array z, int SFO) {
	int Nx = int(in.dims(0));
	int Ny = int(in.dims(1));
	int Nz = int(in.dims(2));

	af::array X = pythonLikeMod(x, Nx);
	af::array Y = pythonLikeMod(y, Ny);
	af::array Z = pythonLikeMod(z, Nz);

	af::array idx1 = af::floor(X);
	af::array idx2 = pythonLikeMod(af::floor(X) + 1.0, Nx);
	af::array idy1 = af::floor(Y);
	af::array idy2 = pythonLikeMod(af::floor(Y) + 1.0, Ny);
	af::array idz1 = af::floor(Z);
	af::array idz2 = pythonLikeMod(af::floor(Z) + 1.0, Nz);

	af::array f111 = in(idx1 + Nx*(idy1 + Ny*idz1));
	af::array f211 = in(idx2 + Nx*(idy1 + Ny*idz1));
	af::array f121 = in(idx1 + Nx*(idy2 + Ny*idz1));
	af::array f112 = in(idx1 + Nx*(idy1 + Ny*idz2));
	af::array f221 = in(idx2 + Nx*(idy2 + Ny*idz1));
	af::array f212 = in(idx2 + Nx*(idy1 + Ny*idz2));
	af::array f122 = in(idx1 + Nx*(idy2 + Ny*idz2));
	af::array f222 = in(idx2 + Nx*(idy2 + Ny*idz2));

	idx2 = af::floor(X) + 1.0;
	idy2 = af::floor(Y) + 1.0;
	idz2 = af::floor(Z) + 1.0;

	idx1 = X - idx1;
	idx2 = idx2 - X;
	idy1 = Y - idy1;
	idy2 = idy2 - Y;
	idz1 = Z - idz1;
	idz2 = idz2 - Z;

	return f111 * idx2 * idy2 * idz2 +
		   f211 * idx1 * idy2 * idz2 +
		   f121 * idx2 * idy1 * idz2 +
		   f112 * idx2 * idy2 * idz1 +
		   f221 * idx1 * idy1 * idz2 +
		   f212 * idx1 * idy2 * idz1 +
		   f122 * idx2 * idy1 * idz1 +
		   f222 * idx1 * idy1 * idz1;
}

af::array mulByComplexExp(af::array values, double arg) {
	if (values.iscomplex()) {
		return af::complex(std::cos(arg) * af::real(values) - std::sin(arg) * af::imag(values),
			std::cos(arg) * af::imag(values) + std::sin(arg) * af::real(values)).as(c64);
	}
	else {
		return af::complex(std::cos(arg) * values, std::sin(arg) * values).as(c64);
	}
}

void diff_center(Field& source, Field& output, int axis) {
	if (axis == 0) {
		output.values += (0.5 / source.mesh.dx)*(af::shift(source.values, -1, 0, 0) - af::shift(source.values, 1, 0, 0));
		output.values.eval();
	}
	else if (axis == 1) {
		output.values += (0.5 / source.mesh.dy)*(af::shift(source.values, 0, -1, 0) - af::shift(source.values, 0, 1, 0));
		output.values.eval();
	}
	else if (axis == 2) {
		output.values += (0.5 / source.mesh.dz)*(af::shift(source.values, 0, 0, -1) - af::shift(source.values, 0, 0, 1));
		output.values.eval();
	}
	else
		throw std::invalid_argument("YeeMesh::diff_center - Unknown axis type.");
}

Field grad(Field& in, FieldType outputType) {
	if (in.type == FieldType::CENTER) {
		if (outputType == FieldType::FACE) {
			return Field(FieldType::FACE, af::join(3, (in.values - af::shift(in.values, 1, 0, 0)) / in.mesh.dx,
													  (in.values - af::shift(in.values, 0, 1, 0)) / in.mesh.dy,
													  (in.values - af::shift(in.values, 0, 0, 1)) / in.mesh.dz),
						 in.mesh);
		}
		if (outputType == FieldType::CENTER_VECTOR) {
			return Field(FieldType::CENTER_VECTOR, af::join(3, (af::shift(in.values, -1,  0,  0) - af::shift(in.values, 1, 0, 0)) / (2.0*in.mesh.dx),
															   (af::shift(in.values,  0, -1,  0) - af::shift(in.values, 0, 1, 0)) / (2.0*in.mesh.dy),
															   (af::shift(in.values,  0,  0, -1) - af::shift(in.values, 0, 0, 1)) / (2.0*in.mesh.dz)),
						in.mesh);
		}
		else
			throw std::invalid_argument("grad(Field& in)  ->  Method only available for 'outputType' FACE or CENTER_VECTOR.");
	}
	else
		throw std::invalid_argument("grad(Field& in)  ->  Method only available for input CENTER type fields.");
}

// Div methods
Field div(Field& in, FieldType outputType) {
	if (in.type == FieldType::FACE && outputType == FieldType::CENTER) {
		af::array out = af::array(in.mesh.Nx, in.mesh.Ny, in.mesh.Nz, in.values.type());
		out  = (af::shift(in.values(SPAN3, 0), -1, 0, 0) - in.values(SPAN3, 0)) / in.mesh.dx;
		out += (af::shift(in.values(SPAN3, 1), 0, -1, 0) - in.values(SPAN3, 1)) / in.mesh.dy;
		out += (af::shift(in.values(SPAN3, 2), 0, 0, -1) - in.values(SPAN3, 2)) / in.mesh.dz;
		out.eval();
		return Field(FieldType::CENTER, out, in.mesh);
	}
	else
		throw std::invalid_argument("Field div(Field& in, FieldType outputType)  ->  Method not implemented or unsupported.\n");
}

// Curl methods
Field curl(Field& source, FieldType output_type) {
	if (source.type == FieldType::FACE && output_type == FieldType::EDGE) {
		Field output = Field(FieldType::EDGE, af::constant(0.0, source.values.dims(), source.values.type()), source.mesh);
		output.values(SPAN3, 0) += (source.values(SPAN3, 2) - af::shift(source.values(SPAN3, 2), 0, 1, 0)) / source.mesh.dy + (af::shift(source.values(SPAN3, 1), 0, 0, 1) - source.values(SPAN3, 1)) / source.mesh.dz;
		output.values(SPAN3, 1) += (source.values(SPAN3, 0) - af::shift(source.values(SPAN3, 0), 0, 0, 1)) / source.mesh.dz + (af::shift(source.values(SPAN3, 2), 1, 0, 0) - source.values(SPAN3, 2)) / source.mesh.dx;
		output.values(SPAN3, 2) += (source.values(SPAN3, 1) - af::shift(source.values(SPAN3, 1), 1, 0, 0)) / source.mesh.dx + (af::shift(source.values(SPAN3, 0), 0, 1, 0) - source.values(SPAN3, 0)) / source.mesh.dy;
		output.values.eval();
		return output;
	}
	else if (source.type == FieldType::EDGE && output_type == FieldType::FACE) {
		Field output = Field(FieldType::FACE, af::constant(0.0, source.values.dims(), source.values.type()), source.mesh);
		output.values(SPAN3, 0) += (af::shift(source.values(SPAN3, 2), 0, -1, 0) - source.values(SPAN3, 2)) / source.mesh.dy + (source.values(SPAN3, 1) - af::shift(source.values(SPAN3, 1), 0, 0, -1)) / source.mesh.dy;
		output.values(SPAN3, 1) += (af::shift(source.values(SPAN3, 0), 0, 0, -1) - source.values(SPAN3, 0)) / source.mesh.dz + (source.values(SPAN3, 2) - af::shift(source.values(SPAN3, 2), -1, 0, 0)) / source.mesh.dz;
		output.values(SPAN3, 2) += (af::shift(source.values(SPAN3, 1), -1, 0, 0) - source.values(SPAN3, 1)) / source.mesh.dx + (source.values(SPAN3, 0) - af::shift(source.values(SPAN3, 0), 0, -1, 0)) / source.mesh.dx;
		output.values.eval();
		return output;
	}
	else
		throw std::invalid_argument("YeeMesh::curl - Method not implemented or unsupported.");
}

// Interpolation methods
Field interpolate(const Field& source, const FieldType outputType) {
	if (source.type == outputType)
		return source;
	else if (source.type == FieldType::FACE && outputType == FieldType::CENTER_VECTOR) {
		Field output = zeros(source.mesh, outputType, source.values.type());
		output.values(SPAN3, 0) += 0.5*(source.values(SPAN3, 0) + af::shift(source.values(SPAN3, 0), -1, 0, 0));
		output.values(SPAN3, 1) += 0.5*(source.values(SPAN3, 1) + af::shift(source.values(SPAN3, 1), 0, -1, 0));
		output.values(SPAN3, 2) += 0.5*(source.values(SPAN3, 2) + af::shift(source.values(SPAN3, 2), 0, 0, -1));
		output.values.eval();
		return output;
	}
	else if (source.type == FieldType::CENTER_VECTOR && outputType == FieldType::FACE) {
		Field output = zeros(source.mesh, outputType, source.values.type());
		output.values(SPAN3, 0) += 0.5*(source.values(SPAN3, 0) + af::shift(source.values(SPAN3, 0), 1, 0, 0));
		output.values(SPAN3, 1) += 0.5*(source.values(SPAN3, 1) + af::shift(source.values(SPAN3, 1), 0, 1, 0));
		output.values(SPAN3, 2) += 0.5*(source.values(SPAN3, 2) + af::shift(source.values(SPAN3, 2), 0, 0, 1));
		output.values.eval();
		return output;
	}
	else if (source.type == FieldType::EDGE && outputType == FieldType::CENTER_VECTOR) {
		Field output = zeros(source.mesh, outputType, source.values.type());
		output.values(SPAN3, 0) += 0.25*(source.values(SPAN3, 0) + af::shift(source.values(SPAN3, 0), 0, 1, 0) + af::shift(source.values(SPAN3, 0), 0, 0, 1) + af::shift(source.values(SPAN3, 0), 0, 1, 1));
		output.values(SPAN3, 1) += 0.25*(source.values(SPAN3, 1) + af::shift(source.values(SPAN3, 1), 1, 0, 0) + af::shift(source.values(SPAN3, 1), 0, 0, 1) + af::shift(source.values(SPAN3, 1), 1, 0, 1));
		output.values(SPAN3, 2) += 0.25*(source.values(SPAN3, 2) + af::shift(source.values(SPAN3, 2), 1, 0, 0) + af::shift(source.values(SPAN3, 2), 0, 1, 0) + af::shift(source.values(SPAN3, 2), 1, 1, 0));
		output.values.eval();
		return output;
	}
	else {
		throw std::invalid_argument("Method not implemented or unsupported");
	}
}

// General interpolation methods
/*PointsQuantities interpolate(Field& source, PointsPositions& positions) {
	//af_print(source.values);
	if (source.type == FieldType::FACE) {
		af::array aux = af::constant(0.0, positions.positions.dims(0), 3, f64);
		aux(SPAN, 0) = trilinearInterpolation(source.values(SPAN3, 0), positions.positions(SPAN, 0) / source.mesh.dx, positions.positions(SPAN, 1) / source.mesh.dy - 0.5, positions.positions(SPAN, 2) / source.mesh.dz - 0.5);
		aux(SPAN, 1) = trilinearInterpolation(source.values(SPAN3, 1), positions.positions(SPAN, 0) / source.mesh.dx - 0.5, positions.positions(SPAN, 1) / source.mesh.dy, positions.positions(SPAN, 2) / source.mesh.dz - 0.5);
		aux(SPAN, 2) = trilinearInterpolation(source.values(SPAN3, 2), positions.positions(SPAN, 0) / source.mesh.dx - 0.5, positions.positions(SPAN, 1) / source.mesh.dy - 0.5, positions.positions(SPAN, 2) / source.mesh.dz);
		aux.eval();
		return PointsQuantities(PointsType::VECTOR, positions, aux);
	}
	if (source.type == FieldType::EDGE) {
		af::array aux = af::constant(0.0, positions.positions.dims(0), 3, f64);
		aux(SPAN, 0) = trilinearInterpolation(source.values(SPAN3, 0), positions.positions(SPAN, 0) / source.mesh.dx - 0.5, positions.positions(SPAN, 1) / source.mesh.dy, positions.positions(SPAN, 2) / source.mesh.dz);
		aux(SPAN, 1) = trilinearInterpolation(source.values(SPAN3, 1), positions.positions(SPAN, 0) / source.mesh.dx, positions.positions(SPAN, 1) / source.mesh.dy - 0.5, positions.positions(SPAN, 2) / source.mesh.dz);
		aux(SPAN, 2) = trilinearInterpolation(source.values(SPAN3, 2), positions.positions(SPAN, 0) / source.mesh.dx, positions.positions(SPAN, 1) / source.mesh.dy, positions.positions(SPAN, 2) / source.mesh.dz - 0.5);
		aux.eval();
		return PointsQuantities(PointsType::VECTOR, positions, aux);
	}
	if (source.type == FieldType::CENTER) {
		return PointsQuantities(PointsType::SCALAR, positions, trilinearInterpolation(source.values, positions.positions(SPAN, 0) / source.mesh.dx - 0.5, positions.positions(SPAN, 1) / source.mesh.dy - 0.5, positions.positions(SPAN, 2) / source.mesh.dz - 0.5));
	}
	if (source.type == FieldType::CENTER_VECTOR) {
		af::array aux = af::constant(0.0, positions.positions.dims(0), 3, f64);
		aux(SPAN, 0) = trilinearInterpolation(source.values(SPAN3, 0), positions.positions(af::span, 0) / source.mesh.dx + 0.5, positions.positions(af::span, 1) / source.mesh.dy + 0.5, positions.positions(af::span, 2) / source.mesh.dz + 0.5);
		aux(SPAN, 1) = trilinearInterpolation(source.values(SPAN3, 1), positions.positions(af::span, 0) / source.mesh.dx + 0.5, positions.positions(af::span, 1) / source.mesh.dy + 0.5, positions.positions(af::span, 2) / source.mesh.dz + 0.5);
		aux(SPAN, 2) = trilinearInterpolation(source.values(SPAN3, 2), positions.positions(af::span, 0) / source.mesh.dx + 0.5, positions.positions(af::span, 1) / source.mesh.dy + 0.5, positions.positions(af::span, 2) / source.mesh.dz + 0.5);
		aux.eval();
		return PointsQuantities(PointsType::VECTOR, positions, aux);
	}
}*/

// Poisson methods
af::array fftfreq(int n, double d, af::dtype type) {
	if (n % 2 == 0)
		return af::join(0, af::range(n / 2, 1, 1, 1, -1, type), -1.0*(af::flip(af::range(n / 2, 1, 1, 1, -1, type), 0) + 1.0)) / (n*d);
	else
		return af::join(0, af::range((n - 1) / 2 + 1, 1, 1, 1, -1, type), -1.0*(af::flip(af::range((n - 1) / 2, 1, 1, 1, -1, type), 0) + 1.0)) / (n*d);
}

af::array meshgrid(af::array x, af::array y, af::array z) {
	return af::join(3, af::tile(x, 1, (unsigned int)y.dims(0), (unsigned int)z.dims(0)),
					   af::tile(af::moddims(y, 1, y.dims(0)), x.dims(0), 1, z.dims(0)),
					   af::tile(af::moddims(z, 1, 1, z.dims(0)), x.dims(0), y.dims(0), 1));
}

af::array poissonSolverAux(af::array in, double dx, double dy, double dz) {
	if (in.dims(3) == 1) {
		af::array k2 = meshgrid(2*PI*fftfreq(in.dims(0), dx), 
								2*PI*fftfreq(in.dims(1), dy), 
								2*PI*fftfreq(in.dims(2), dz));

		k2 = -af::sum(af::pow(k2, 2.0), 3);
		k2(0, 0, 0) = 1.0;

		af::array out = af::dft(in, 1.0, in.dims()) / k2;

		out(0, 0, 0) = 0.0;
		out = af::real(af::idft(out, 1.0/(out.dims(0)*out.dims(1)*out.dims(2)), out.dims()));
		out.eval();
		return out;
	}
	else
		throw std::invalid_argument("af::array poissonSolverAux(af::array in, double dx, double dy, double dz)  ->  This method is only available for 3D arrays.\n");
}

Field poissonSolver(Field& in) {
	if (in.type == FieldType::CENTER)
		return Field(FieldType::CENTER, poissonSolverAux(in.values, in.mesh.dx, in.mesh.dy, in.mesh.dz), in.mesh);

	else if (in.type == FieldType::FACE) {
		af::array out = af::array(in.values.dims(), in.values.type());
		out(SPAN3, 0) = poissonSolverAux(in.values(SPAN3, 0), in.mesh.dx, in.mesh.dy, in.mesh.dz);
		out(SPAN3, 1) = poissonSolverAux(in.values(SPAN3, 1), in.mesh.dx, in.mesh.dy, in.mesh.dz);
		out(SPAN3, 2) = poissonSolverAux(in.values(SPAN3, 2), in.mesh.dx, in.mesh.dy, in.mesh.dz);
		out.eval();
		return Field(FieldType::FACE, out, in.mesh);
	}
	else
		throw std::invalid_argument("poissonSolver(Field& in)  ->  Method not implemented for that field type.");
}

Field gaussLawSolver(Field& in, FieldType returnType, double stopingParameter, double timeLimitSecs) {
	if (in.type == FieldType::CENTER) {
		if (returnType == FieldType::FACE) {
			af::array out = grad(poissonSolver(in), FieldType::FACE).values;
			int mulFactorPow = 1;
			double inMax = af::max<double>(af::abs(in.values.as(f64)));
			auto start_time = std::chrono::steady_clock::now();
			int i = 0;
			
			while (true) {
				if (std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - start_time).count() > timeLimitSecs) {
					std::cout << "Maximum aloted time has passed" << std::endl;
					break; // Maximum aloted time has passed
				}

				af::array aux = in.mesh.dy*in.mesh.dz*(af::shift(out(SPAN3, 0), -1, 0, 0) - out(SPAN3, 0)) +
								in.mesh.dx*in.mesh.dz*(af::shift(out(SPAN3, 1), 0, -1, 0) - out(SPAN3, 1)) +
								in.mesh.dx*in.mesh.dy*(af::shift(out(SPAN3, 2), 0, 0, -1) - out(SPAN3, 2)) - in.values;

				double stpAux = std::fmax(inMax, af::max<double>(af::abs(out.as(f64))));

				//if (af::sum<int>((af::abs(out) <= stopingParameter*stpAux).as(s32))) {
				//	std::cout << "Gauss Law is obeyed to within machine epsilon" << std::endl;
				//	break; // Gauss Law is obeyed to within machine epsilon
				//}

				aux *= 1.0/6.0;
				out(SPAN3, 0) += aux - af::shift(aux, 1, 0, 0);
				out(SPAN3, 1) += aux - af::shift(aux, 0, 1, 0);
				out(SPAN3, 2) += aux - af::shift(aux, 0, 0, 1);

				i++;
			}
			std::cout << i << std::endl << std::endl;

			return Field(returnType, out, in.mesh);
		}
		else
			throw std::invalid_argument("Field gaussLawSolver(Field& in, FieldType returnType)  ->  Method not implemented for that type of return field.\n");
	}
	else
		throw std::invalid_argument("Field gaussLawSolver(Field& in, FieldType returnType)  ->  Input field must be CENTER.\n");
}

Field calculateFieldVersor(const Field& inputField) {

	/// Calculate field magnitude
	af::array inputFieldMagnitude = af::sqrt(af::sum(af::pow(inputField.values, 2.0), 3));
	const double maxMagnitude = af::max<double>(inputFieldMagnitude);

	/// TODO: Change hardcoded threshold
	double threshold = 1.0e-13;

	/// 0 where field < threshold
	af::array auxBool = inputFieldMagnitude >= threshold;

	/// Create a Field with the versor
	Field outputField = zeros(inputField.mesh, inputField.type, f64);
	outputField.values(SPAN3, 0) = auxBool * (inputField.values(SPAN3, 0) / inputFieldMagnitude);
	outputField.values(SPAN3, 1) = auxBool * (inputField.values(SPAN3, 1) / inputFieldMagnitude);
	outputField.values(SPAN3, 2) = auxBool * (inputField.values(SPAN3, 2) / inputFieldMagnitude);

	outputField.values.eval();

	/// Return the Field of versors
	return outputField;
}

af::array normalizeForImage(af::array in) {
	float MIN = af::min<float>(in);
	float MAX = af::max<float>(in);
	return af::transpose((in.as(f32) - MIN) / (MAX - MIN));
}

af::array compress(af::array in, int ratio0, int ratio1, int ratio2, int ratio3) {
	af::array out = af::array(in.dims(0)/ratio0, in.dims(1)/ratio1/*, in.dims(2)/ratio2, in.dims(3)/ratio3*/, in.type());
	int i, j, k, l;
	for (i = 0; i < ratio0; i++) {
		for (j = 0; j < ratio1; j++) {
			for (k = 0; k < ratio2; k++) {
				for (l = 0; l < ratio3; l++) {
					//std::cout << in(af::seq(i, af::end, ratio0), af::seq(j, af::end, ratio1)).dims() << std::endl;
					out += in(af::seq(i, af::end, ratio0),
							  af::seq(j, af::end, ratio1)/*,
							  af::seq(k, af::end - ratio2 + k, ratio2),
							  af::seq(l, af::end - ratio3 + l, ratio3)*/);
				}
			}
		}
	}
	return out / (ratio0*ratio1*ratio2*ratio3);
}

Field cross(const Field& lhs, const Field& rhs, FieldType outputType) {
	if (lhs.mesh == rhs.mesh) {
		if (outputType == FieldType::CENTER_VECTOR) {
			af::array output(lhs.mesh.Nx, lhs.mesh.Ny, lhs.mesh.Nz, 3, lhs.values.type());
			af::array lhs_aux = interpolate(lhs, FieldType::CENTER_VECTOR).values;
			af::array rhs_aux = interpolate(rhs, FieldType::CENTER_VECTOR).values;
			output(SPAN3, 0) = lhs_aux(SPAN3, 1)*rhs_aux(SPAN3, 2) - lhs_aux(SPAN3, 2)*rhs_aux(SPAN3, 1);
			output(SPAN3, 1) = lhs_aux(SPAN3, 2)*rhs_aux(SPAN3, 0) - lhs_aux(SPAN3, 0)*rhs_aux(SPAN3, 2);
			output(SPAN3, 2) = lhs_aux(SPAN3, 0)*rhs_aux(SPAN3, 1) - lhs_aux(SPAN3, 1)*rhs_aux(SPAN3, 0);
			return Field(FieldType::CENTER_VECTOR, output, lhs.mesh);
		}
		else
			throw std::invalid_argument("cross(Field& lhs, Field& rhs, FieldType outputType)  ->  Method currently only available for 'outpuType' CENTER_VECTOR.");
	}
	else
		throw std::invalid_argument("cross(Field& lhs, Field& rhs, FieldType outputType)  ->  Both 'Field' instances must have the same mesh.");
}

Field magnitude(Field& in) {
	if (in.type == FieldType::CENTER_VECTOR) {
		af::array out = af::sqrt(in.values(SPAN3, 0)*in.values(SPAN3, 0) + in.values(SPAN3, 1)*in.values(SPAN3, 1) + in.values(SPAN3, 2)*in.values(SPAN3, 2));
		out.eval();
		return Field(FieldType::CENTER, out, in.mesh);
	}
	else
		throw std::invalid_argument("magnitude(Field& in)  ->  'in' argument must have type CENTER_VECTOR.");
}

std::string getCurrentDateTime(bool useLocalTime) {
	std::stringstream currentDateTime;
	// current date/time based on current system
	time_t ttNow = time(0);
	tm * ptmNow;

	if (useLocalTime)
		ptmNow = localtime(&ttNow);
	else
		ptmNow = gmtime(&ttNow);

	currentDateTime << 1900 + ptmNow->tm_year;

	//month
	if (ptmNow->tm_mon < 9)
		//Fill in the leading 0 if less than 10
		currentDateTime << "0" << 1 + ptmNow->tm_mon;
	else
		currentDateTime << (1 + ptmNow->tm_mon);

	//day
	if (ptmNow->tm_mday < 10)
		currentDateTime << "0" << ptmNow->tm_mday << " ";
	else
		currentDateTime << ptmNow->tm_mday << "_";

	//hour
	if (ptmNow->tm_hour < 10)
		currentDateTime << "0" << ptmNow->tm_hour;
	else
		currentDateTime << ptmNow->tm_hour;

	//min
	if (ptmNow->tm_min < 10)
		currentDateTime << "0" << ptmNow->tm_min;
	else
		currentDateTime << ptmNow->tm_min;

	//sec
	if (ptmNow->tm_sec < 10)
		currentDateTime << "0" << ptmNow->tm_sec;
	else
		currentDateTime << ptmNow->tm_sec;


	return currentDateTime.str();
}

std::string generateDirectoryPath(std::string preamble, bool temp) {
	std::string directoryPath;
	if (temp) {
		/// Standard results path is located under $(SolutionDir)/Results
		directoryPath = "..\\Temp\\";
	}
	else {
		directoryPath = "..\\Results\\";
	}
	if (preamble == "")
		directoryPath += getCurrentDateTime(true);
	else
		directoryPath += preamble + "_" + getCurrentDateTime(true);

	return directoryPath;
}

bool MKDirectory(std::string directoryPath) {
	/// Use windows native function to attempt dir creation
	bool success = CreateDirectory(directoryPath.c_str(), NULL);

	/// Throw error if dir creation was unsuccessful
	if (!success)
		throw std::invalid_argument("MKDirectory - Error creating folder. Confirm that it does not exist");

	return success;
}

bool saveAFValuesOld(const std::string filePath, const af::array values, const int precision) {

	/// Open file
	std::ofstream output(filePath);
	if (!output) {
		throw std::invalid_argument("SaveAFValues - Error opening file");
		return false;
	}

	/// Set saving precision, if given
	if (precision != -1)
		output.precision(precision);

	/// Get array dimensions and save them to file
	af::dim4 arrayShape = values.dims();
	output << arrayShape[0] << " " << arrayShape[1] << " " << arrayShape[2] << " " << arrayShape[3] << std::endl;

	/// Count number of elements
	int numberOfElements = arrayShape[0] * arrayShape[1] * arrayShape[2] * arrayShape[3];

	/// Copy array to host
	float *hostValues = values.as(f32).host<float>();

	/// Save the values 
	for (int i = 0; i < numberOfElements; i++) {
		output << hostValues[i] << std::endl;
	}

	/// Close the file and free the host array
	af::freeHost(hostValues);
	output.close();

	return true;
}

af::array normalizeVector(const af::array in) {
	/// Calculate input magnitude
	af::array magnitude = af::sqrt( af::sum( af::pow(in, 2), 3) );

	/// Avoid NaNs
	af::array auxBool = magnitude > 1.0e-15;

	/// Create output
	af::array out = af::constant(0.0, in.dims(), in.type());
	out(SPAN3, 0) = auxBool * (in(SPAN3, 0) / magnitude);
	out(SPAN3, 1) = auxBool * (in(SPAN3, 1) / magnitude);
	out(SPAN3, 2) = auxBool * (in(SPAN3, 2) / magnitude);

	return out;
}

af::array relativeError(af::array exactVals, af::array calcVals) {
	af::array den = 2.0*af::abs(exactVals + calcVals);
	af::array num = af::abs(exactVals - calcVals);
	af::array zero = (den == 0);
	return (num/(den + zeros))*(1 - zero);
}

af::dtype afTypeSelector(af::dtype lhs, af::dtype rhs) {
	af::dtype table[12][12] = { { f32, c32, f64, c64, f32, f32, f32, f32, f32, f32, f32, f32 },
								{ c32, c32, c64, c64, c32, c32, c32, c32, c32, c32, c32, c32 },
								{ f64, c64, f64, c64, f64, f64, f64, f64, f64, f64, f64, f64 },
								{ c64, c64, c64, c64, c64, c64, c64, c64, c64, c64, c64, c64 },
								{ f32, c32, f64, c64, s32, s32, u32, s32, s64, u64, s32, s32 },
								{ f32, c32, f64, c64, s32, s32, u32, s32, s64, u64, s32, s32 },
								{ f32, c32, f64, c64, u32, u32, u32, u32, s64, u64, u32, u32 },
								{ f32, c32, f64, c64, s32, s32, u32, s32, s64, u64, s32, s32 },
								{ f32, c32, f64, c64, s64, s64, s64, s64, s64, u64, s64, s64 },
								{ f32, c32, f64, c64, u64, u64, u64, u64, u64, u64, u64, u64 },
								{ f32, c32, f64, c64, s32, s32, u32, s32, s64, u64, s32, s32 },
								{ f32, c32, f64, c64, s32, s32, u32, s32, s64, u64, s32, s32 } };

	#ifdef ENABLE_TESTS
	if (table[lhs][rhs] != (af::constant(0.0, 1, lhs) + af::constant(0.0, 1, rhs)).type())
		throw std::invalid_argument("af::dtype afTypeSelector(af::dtype lhs, af::dtype rhs)  ->  The recorded table of selection does not match the obtained type from ArrayFire.\n");
	#endif

	return table[lhs][rhs];
}

double integrate(Field& field) {
	#ifdef ENABLE_TESTS
	if (field.type != FieldType::CENTER)
		throw std::invalid_argument("double integrate(Field& field)  ->  This method only works for CENTER fields.\n");
	#endif

	double I;
	if (field.dtype() == f32)
		I = (double)(af::sum<float>(field.values));
	else
		I = af::sum<double>(field.values);

	return field.mesh.dx*field.mesh.dy*field.mesh.dz*I;
}

double min(Field& field) {
	#ifdef ENABLE_TESTS
	if (field.type != FieldType::CENTER)
		throw std::invalid_argument("double min(Field& field)  ->  This method only works for CENTER fields.\n");
	#endif

	double I;
	if (field.dtype() == f32)
		I = (double)(af::min<float>(field.values));
	else
		I = af::min<double>(field.values);

	return I;
}

double max(Field& field) {
	#ifdef ENABLE_TESTS
	if (field.type != FieldType::CENTER)
		throw std::invalid_argument("double min(Field& field)  ->  This method only works for CENTER fields.\n");
	#endif

	double I;
	if (field.dtype() == f32)
		I = (double)(af::max<float>(field.values));
	else
		I = af::max<double>(field.values);

	return I;
}

af::array distributionSampling(Field& field, int n, af::dtype dtype) {
	#ifdef ENABLE_TESTS
	if (field.type != FieldType::CENTER)
		throw std::invalid_argument("af::array distributionSampling(Field& field, int n)  ->  This method only works for CENTER fields.\n");
	if (min(field) < 0.0)
		throw std::invalid_argument("af::array distributionSampling(Field& field, int n)  ->  Distribution can not contain negative values.\n");
	#endif

	double norm = max(field);
	double Ls[3] = { field.mesh.Lx, field.mesh.Ly, field.mesh.Lz };
	af::array out = af::constant(0.0, 1, 3, dtype);
	
	int j = 0;
	while (out.dims(0) < n) {
		std::cout << j << std::endl;

		int n_aux = n - out.dims(0);

		af::array outX = field.mesh.Lx*af::randu(n_aux, dtype);
		af::array outY = field.mesh.Ly*af::randu(n_aux, dtype);
		af::array outZ = field.mesh.Lz*af::randu(n_aux, dtype);

		af::array aux = af::randu(n_aux) <= interpolation(field, af::join(1, outX, outY, outZ))/norm;
		if (af::sum<int>(aux) != 0) {
			af::array indices = af::where(aux);

			af::array auxaux = af::join(1, outX(indices), outY(indices), outZ(indices));
			return auxaux;
			out = af::join(0, out, auxaux);

			out.eval();
		}

		j++;
	}

	out = out(af::seq(1, n));
	out.eval();

	return out;
}

bool saveAFValuesToFile(const std::string filePath, const af::array values, const int precision) {

	/// Open file
	std::ofstream output(filePath);
	if (!output) {
		throw std::invalid_argument("SaveAFValues - Error opening file");
		return false;
	}

	/// Set saving precision, if given
	if (precision != -1)
		output.precision(precision);

	/// Get array dimensions and save them to file
	std::stringstream ss;
	ss.precision(precision);
	af::dim4 arrayShape = values.dims();
	ss << arrayShape[0] << " " << arrayShape[1] << " " << arrayShape[2] << " " << arrayShape[3] << std::endl;

	/// Count number of elements
	int numberOfElements = arrayShape[0] * arrayShape[1] * arrayShape[2] * arrayShape[3];

	/// Copy array to host
	float *hostValues = values.as(f32).host<float>();

	/// Save the values 
	for (int i = 0; i < numberOfElements; i++) {
		ss << hostValues[i] << std::endl;
	}

	/// Close the file and free the host array
	af::freeHost(hostValues);
	output << ss.str();
	output.close();

	return true;
}

void atomicIndexAdd(af::array& out, af::array values, af::array i, af::array j, af::array k, af::array l) {
	if (j.isempty()) j = af::constant(0, i.dims(0), i.type());
	if (k.isempty()) k = af::constant(0, i.dims(0), i.type());
	if (l.isempty()) l = af::constant(0, i.dims(0), i.type());

	if (values.dims(0) != i.dims(0) || i.dims(0) != j.dims(0) || k.dims(0) != l.dims(0) || j.dims(0) != k.dims(0) ||
		values.dims(1) != 1 || values.dims(2) != 1 || values.dims(3) != 1 ||
		i.dims(1) != 1 || i.dims(2) != 1 || i.dims(3) != 1 ||
		j.dims(1) != 1 || j.dims(2) != 1 || j.dims(3) != 1 ||
		k.dims(1) != 1 || k.dims(2) != 1 || k.dims(3) != 1 ||
		l.dims(1) != 1 || l.dims(2) != 1 || l.dims(3) != 1)
		throw std::invalid_argument("void atomicIndexAdd(af::array& out, af::array& values, af::array& i, af::array& j, af::array& k, af::array& l)  ->  Invalid dimensions: the input and index arrays must be all of 1 dimensional and of equal lenght.\n");

	af::array indices = (i + out.dims(0)*(j + out.dims(1)*(k + l*out.dims(2)))).as(s32);
	af::array keys_s, vals_s;
	af::sort(keys_s, vals_s, indices, values);
	af::array sbk = af::scanByKey(keys_s, vals_s);
	af::array idx = af::where((shift(keys_s, -1) - keys_s) != 0);
	if (idx.isempty()) {
		af::array aux = keys_s(af::end);
		out(aux) += sbk(af::end);
		out.eval();
	}
	else {
		af::array aux = keys_s(idx);
		out(aux) += sbk(idx);
		out.eval();
	}
}

bool saveAFValuesWithAF(const std::string filePath, const std::string key, af::array values) {

	/// Save to path using ArrayFire method
	int saveKey = af::saveArray(key.c_str(), values, filePath.c_str());

	return (saveKey >= 0);
}

Boundary makeABCBoundary(BoundaryAxis Axis, double L, double C, int POW) {
	if (C <= 1.0) {
		/// Create Union object
		BoundaryUnion ABCUnion;
		ABCUnion.ABC.L = L;
		ABCUnion.ABC.POW = POW;
		ABCUnion.ABC.C = C;

		/// Create Boundary object
		return Boundary(BoundaryType::ABC, Axis, ABCUnion);
	}
	else {
		throw(std::invalid_argument("makeABCBoundary - C bigger than 1"));
	}
}

Boundary makeUPMLBoundary(BoundaryAxis Axis, double L, double Sigma, double K, int POW) {
	BoundaryUnion UPMLUnion;
	UPMLUnion.UPML.L = L;
	UPMLUnion.UPML.Sigma = Sigma;
	UPMLUnion.UPML.K = K;
	UPMLUnion.UPML.POW = POW;
	return Boundary(BoundaryType::UPML, Axis, UPMLUnion);
}

double maxwellJuttnerDistributionGen(double T, double k, double m, double c) {
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<> dis(0, 1);

	double X4, X5, X6, X7, u;
	T *= 1.0;

	if (T == 0.0)
		return 0.0;

	while (true) {
		//Xs = af::randu(4, f64).host<double>();
		//X4 = Xs[0];
		//X5 = Xs[1];
		//X6 = Xs[2];
		//X7 = Xs[3];
		X4 = dis(gen);
		X5 = dis(gen);
		X6 = dis(gen);
		X7 = dis(gen);

		if (X4 == 0.0 || X5 == 0.0 || X6 == 0.0 || X7 == 0.0)
			continue;

		u = -T*std::log(X4*X5*X6);

		if (std::sqrt(1.0 + u*u) < (u - T*std::log(X7)))
			return u/std::sqrt(1.0 + u*u);
	}
}

af::array maxwellJuttnerDistributionGen(af::array T, double k, double m, double c) {
	if (T.dims(1) != 1 || T.dims(2) != 1 || T.dims(3) != 1)
		throw std::invalid_argument("af::array maxwellJuttnerDistributionGen(af::array T, double k, double m, double c)  ->  Temperature array must be one dimensional.\n");

	int N = T.dims(0);
	double *temp = T.as(f64).host<double>();

	omp_set_dynamic(0);
	#pragma omp parallel for num_threads(4)
	for (int i = 0; i < N; i++) {
		temp[i] = maxwellJuttnerDistributionGen(temp[i], k, m, c);
	}

	af::array v = af::array(N, temp).as(T.type());

	af::array theta = 2.0*PI*af::randu(N, T.type());
	af::array phi = af::acos(2.0*af::randu(N, T.type()) - 1.0);
	af::array x = af::cos(theta) * af::sin(phi);
	af::array y = af::sin(theta) * af::sin(phi);
	af::array z = af::cos(phi);
	return af::join(1, v*x, v*y, v*z);
}

fieldFunction fieldFunctionFromField(Field& field) {

	fieldFunction aux = [&](af::array x, af::array y, af::array z) {
		return generalShapeInterpolation(field.values, x, y, z, 1);
	};

	return aux;
}