#include <Bloch.h>

Bloch::Bloch(int levels,
	double eta,
	double alpha,
	double beta,
	YeeMesh& mesh,
	std::vector<std::vector<double>> mu,
	std::vector<std::vector<double>> gamma,
	std::vector<std::vector<double>> omega,
	int renormalizeInterval) :	levels(levels),
												dim(levels*levels),
												eta(eta),
												alpha(alpha),
												beta(beta),
												mesh(mesh),
												muMatrix(mu),
												gammaMatrix(gamma),
												omegaMatrix(omega),
												renormalizeInterval(renormalizeInterval),
												internalCounter(0),
												Rho(TensorFieldType::CENTER_MATRIX, this->dim, 1, mesh),
												dipoleVersor(zeros(this->mesh, FieldType::CENTER_VECTOR, mesh.dtype())) {

	if (!(this->omegaMatrix.rows == this->omegaMatrix.cols
		&& this->muMatrix.rows == this->muMatrix.cols
		&& this->gammaMatrix.rows == this->gammaMatrix.cols
		&& this->omegaMatrix.rows == this->dim
		&& this->muMatrix.rows == this->dim
		&& this->omegaMatrix.rows == this->dim)) {

		throw std::invalid_argument("Bloch.Bloch - Arguments size do not match");

	}
		
}

void Bloch::Initialize(std::vector<std::vector<double>> initValues, const DipoleDir initialPolDir) {
	if (initValues.size() == this->dim && initValues[0].size() == 2) {
		/// The values are initialized in the Schrodinger Picture
		for (int i = 0; i < this->dim; i++) {
			this->Rho.setValue(i, 0, af::complex(af::constant(initValues[i][0], this->mesh.Nx, this->mesh.Ny, this->mesh.Nz), af::constant(initValues[i][1], this->mesh.Nx, this->mesh.Ny, this->mesh.Nz)));
		}

		/// Initialize the polarization
		switch (initialPolDir)
		{
		case DipoleDir::X:
			this->dipoleVersor.values(SPAN3, 0) = af::constant(1.0, this->mesh.Nx, this->mesh.Ny, this->mesh.Nz);
			break;
		case DipoleDir::Y:
			this->dipoleVersor.values(SPAN3, 1) = af::constant(1.0, this->mesh.Nx, this->mesh.Ny, this->mesh.Nz);
			break;
		case DipoleDir::Z:
			this->dipoleVersor.values(SPAN3, 2) = af::constant(1.0, this->mesh.Nx, this->mesh.Ny, this->mesh.Nz);
			break;
		default:
			throw(std::invalid_argument("Bloch.Initialize - Invalid polarization direction"));
			break;
		}
	}
	else {
		throw std::invalid_argument("Bloch.Initialize - Initialization values vector inconsistent with number of levels");
	}
}

TensorField Bloch::updateDynamicMatrix(const Field& EField, const Field& dipoleVersor) {
	af::array aux;
	TensorField dynamicMatrix = TensorField(TensorFieldType::CENTER_MATRIX, this->dim, this->dim, this->mesh);
	for (int i = 0; i < this->dim; i++) 
		for (int j = 0; j < this->dim; j++) {
			if (this->muMatrix[i][j] != 0.0 || this->omegaMatrix[i][j] != 0.0) {
				/// RabiFreq = i*dot(mu,E)/hbar
				aux = (af::sum((this->muMatrix[i][j]*dipoleVersor.values) * EField.values, 3) + this->omegaMatrix[i][j]) / this->mesh.units.hbar();
				dynamicMatrix.setValue(i, j, af::complex(0.0, aux));
			}
		}

	return dynamicMatrix;
}

TensorField Bloch::dislocationEffect(const Field& Velocity) {
/// v dot grad(rho_{ij})
	TensorField result = TensorField(TensorFieldType::CENTER_MATRIX, this->dim, 1, this->mesh);
	for (int i = 0; i < this->dim; i++) {
		result.setValue(i,
						0,
						af::sum(Velocity.values * grad(Field(FieldType::CENTER, this->Rho[i][0], this->mesh), FieldType::CENTER_VECTOR).values, 3));
	}
	return result;
}

TensorField Bloch::rhoEquationSystem(const TensorField& rho, const Field& dipoleVersor, const Field& EField) {
	return this->updateDynamicMatrix(EField, dipoleVersor) * rho + this->gammaMatrix * rho;
}

TensorField Bloch::rhoEquationSystem(const TensorField& rho, const Field& dipoleVersor, const Field& EField, const Field& Velocity) {
	return this->updateDynamicMatrix(EField, dipoleVersor) * rho + this->gammaMatrix * rho - this->dislocationEffect(Velocity);
}

Field Bloch::dipoleVersorEquationSystem(const Field& versor, const Field& EField) {
	Field aux = cross(versor, EField);
	return this->alpha * aux - this->beta * cross(versor, aux);
}

void Bloch::RK2(Field& EPrev, Field& ENext, const bool interpolation) {
	if (!interpolation) {
		// First RK2 step
		TensorField RhoAux = this->Rho + 0.5 * this->mesh.dt * this->rhoEquationSystem(this->Rho, this->dipoleVersor, EPrev);
		Field dipoleVersorAux = this->dipoleVersor + 0.5 * this->mesh.dt * this->dipoleVersorEquationSystem(this->dipoleVersor, EPrev);

		// Sencond RK2 step
		this->Rho += this->mesh.dt * this->rhoEquationSystem(RhoAux, dipoleVersorAux, EPrev);
		this->dipoleVersor += this->mesh.dt * this->dipoleVersorEquationSystem(dipoleVersorAux, EPrev);
	}
	else {
		// First RK2 step
		TensorField RhoAux = this->Rho + 0.5 * this->mesh.dt * this->rhoEquationSystem(this->Rho, this->dipoleVersor, EPrev);
		Field dipoleVersorAux = this->dipoleVersor + 0.5 * this->mesh.dt * this->dipoleVersorEquationSystem(this->dipoleVersor, EPrev);

		// Sencond RK2 step
		this->Rho += this->mesh.dt * this->rhoEquationSystem(RhoAux, dipoleVersorAux, 0.5 * (EPrev + ENext));
		this->dipoleVersor += this->mesh.dt * this->dipoleVersorEquationSystem(dipoleVersorAux, 0.5 * (EPrev + ENext));

	}
}

void Bloch::RK2(Field& EPrev, Field& ENext, Field& VPrev, Field& VMiddle, const bool interpolation) {
	if (!interpolation) {
		// First RK2 step
		TensorField RhoAux = this->Rho + 0.5 * this->mesh.dt * this->rhoEquationSystem(this->Rho, this->dipoleVersor, EPrev, VPrev);
		Field dipoleVersorAux = this->dipoleVersor + 0.5 * this->mesh.dt * this->dipoleVersorEquationSystem(this->dipoleVersor, EPrev);

		// Sencond RK2 step
		this->Rho += this->mesh.dt * this->rhoEquationSystem(RhoAux, dipoleVersorAux, EPrev, VPrev);
		this->dipoleVersor += this->mesh.dt * this->dipoleVersorEquationSystem(dipoleVersorAux, EPrev);
	}
	else {
		// First RK2 step
		TensorField RhoAux = this->Rho + 0.5 * this->mesh.dt * this->rhoEquationSystem(this->Rho, this->dipoleVersor, EPrev, 0.5 * (VPrev + VMiddle));
		Field dipoleVersorAux = this->dipoleVersor + 0.5 * this->mesh.dt * this->dipoleVersorEquationSystem(this->dipoleVersor, EPrev);

		// Sencond RK2 step
		this->Rho += this->mesh.dt * this->rhoEquationSystem(RhoAux, dipoleVersorAux, 0.5 * (EPrev + ENext), VMiddle);
		this->dipoleVersor += this->mesh.dt * this->dipoleVersorEquationSystem(dipoleVersorAux, 0.5 * (EPrev + ENext));

	}
}

void Bloch::RK4(Field& EPrev, Field& ENext, const bool interpolation) {
	// RK4 uses two auxiliary variables. K is the evaluiton o the equation system and rhoSchrodingerPicture is the accumulator
	if (!interpolation) {
		// First RK4 step
		TensorField KRho = this->rhoEquationSystem(this->Rho, this->dipoleVersor, EPrev);
		Field KDipoleVersor = this->dipoleVersorEquationSystem(this->dipoleVersor, EPrev);
		TensorField RhoAux = (this->mesh.dt / 6.0) * KRho;
		Field dipoleVersorAux = (this->mesh.dt / 6.0) * KDipoleVersor;

		// Second RK4 step
		KRho = this->rhoEquationSystem(this->Rho + (0.5 * this->mesh.dt) * KRho, this->dipoleVersor + (0.5 * this->mesh.dt) * KDipoleVersor, EPrev);
		KDipoleVersor = this->dipoleVersorEquationSystem(this->dipoleVersor + (0.5 * this->mesh.dt) * KDipoleVersor, EPrev);
		RhoAux += (this->mesh.dt / 3.0) * KRho;
		dipoleVersorAux = (this->mesh.dt / 3.0) * KDipoleVersor;

		// Third RK4 step
		KRho = this->rhoEquationSystem(this->Rho + (0.5 * this->mesh.dt) * KRho, this->dipoleVersor + (0.5 * this->mesh.dt) * KDipoleVersor, EPrev);
		KDipoleVersor = this->dipoleVersorEquationSystem(this->dipoleVersor + (0.5 * this->mesh.dt) * KDipoleVersor, EPrev);
		RhoAux += (this->mesh.dt / 3.0) * KRho;
		dipoleVersorAux = (this->mesh.dt / 3.0) * KDipoleVersor;

		// Fourth RK4 step
		KRho = this->rhoEquationSystem(this->Rho + this->mesh.dt * KRho, this->dipoleVersor + this->mesh.dt * KDipoleVersor, EPrev);
		KDipoleVersor = this->dipoleVersorEquationSystem(this->dipoleVersor + this->mesh.dt * KDipoleVersor, EPrev);
		RhoAux += (this->mesh.dt / 6.0) * KRho;
		dipoleVersorAux = (this->mesh.dt / 6.0) * KDipoleVersor;

		// Update rhoInteractionPicture
		this->Rho += RhoAux;
		this->dipoleVersor += dipoleVersorAux;
	}
	else {
		Field EMiddle = 0.5 * (EPrev + ENext);

		// First RK4 step
		TensorField KRho = this->rhoEquationSystem(this->Rho, this->dipoleVersor, EPrev);
		Field KDipoleVersor = this->dipoleVersorEquationSystem(this->dipoleVersor, EPrev);
		TensorField RhoAux = (this->mesh.dt / 6.0) * KRho;
		Field dipoleVersorAux = (this->mesh.dt / 6.0) * KDipoleVersor;

		// Second RK4 step
		KRho = this->rhoEquationSystem(this->Rho + (0.5 * this->mesh.dt) * KRho, this->dipoleVersor + (0.5 * this->mesh.dt) * KDipoleVersor, EMiddle);
		KDipoleVersor = this->dipoleVersorEquationSystem(this->dipoleVersor + (0.5 * this->mesh.dt) * KDipoleVersor, EMiddle);
		RhoAux += (this->mesh.dt / 3.0) * KRho;
		dipoleVersorAux = (this->mesh.dt / 3.0) * KDipoleVersor;

		// Third RK4 step
		KRho = this->rhoEquationSystem(this->Rho + (0.5 * this->mesh.dt) * KRho, this->dipoleVersor + (0.5 * this->mesh.dt) * KDipoleVersor, EMiddle);
		KDipoleVersor = this->dipoleVersorEquationSystem(this->dipoleVersor + (0.5 * this->mesh.dt) * KDipoleVersor, EMiddle);
		RhoAux += (this->mesh.dt / 3.0) * KRho;
		dipoleVersorAux = (this->mesh.dt / 3.0) * KDipoleVersor;

		// Fourth RK4 step
		KRho = this->rhoEquationSystem(this->Rho + this->mesh.dt * KRho, this->dipoleVersor + this->mesh.dt * KDipoleVersor, ENext);
		KDipoleVersor = this->dipoleVersorEquationSystem(this->dipoleVersor + this->mesh.dt * KDipoleVersor, ENext);
		RhoAux += (this->mesh.dt / 6.0) * KRho;
		dipoleVersorAux = (this->mesh.dt / 6.0) * KDipoleVersor;

		// Update rhoInteractionPicture
		this->Rho += RhoAux;
		this->dipoleVersor += dipoleVersorAux;
	}
}

void Bloch::RK4(Field& EPrev, Field& ENext, Field& VPrev, Field& VMiddle, const bool interpolation) {
	// RK4 uses two auxiliary variables. K is the evaluiton o the equation system and rhoSchrodingerPicture is the accumulator
	if (!interpolation) {
		// First RK4 step
		TensorField KRho = this->rhoEquationSystem(this->Rho, this->dipoleVersor, EPrev, VPrev);
		Field KDipoleVersor = this->dipoleVersorEquationSystem(this->dipoleVersor, EPrev);
		TensorField RhoAux = (this->mesh.dt / 6.0) * KRho;
		Field dipoleVersorAux = (this->mesh.dt / 6.0) * KDipoleVersor;

		// Second RK4 step
		KRho = this->rhoEquationSystem(this->Rho + (0.5 * this->mesh.dt) * KRho, this->dipoleVersor + (0.5 * this->mesh.dt) * KDipoleVersor, EPrev, VPrev);
		KDipoleVersor = this->dipoleVersorEquationSystem(this->dipoleVersor + (0.5 * this->mesh.dt) * KDipoleVersor, EPrev);
		RhoAux += (this->mesh.dt / 3.0) * KRho;
		dipoleVersorAux = (this->mesh.dt / 3.0) * KDipoleVersor;

		// Third RK4 step
		KRho = this->rhoEquationSystem(this->Rho + (0.5 * this->mesh.dt) * KRho, this->dipoleVersor + (0.5 * this->mesh.dt) * KDipoleVersor, EPrev, VPrev);
		KDipoleVersor = this->dipoleVersorEquationSystem(this->dipoleVersor + (0.5 * this->mesh.dt) * KDipoleVersor, EPrev);
		RhoAux += (this->mesh.dt / 3.0) * KRho;
		dipoleVersorAux = (this->mesh.dt / 3.0) * KDipoleVersor;

		// Fourth RK4 step
		KRho = this->rhoEquationSystem(this->Rho + this->mesh.dt * KRho, this->dipoleVersor + this->mesh.dt * KDipoleVersor, EPrev, VPrev);
		KDipoleVersor = this->dipoleVersorEquationSystem(this->dipoleVersor + this->mesh.dt * KDipoleVersor, EPrev);
		RhoAux += (this->mesh.dt / 6.0) * KRho;
		dipoleVersorAux = (this->mesh.dt / 6.0) * KDipoleVersor;

		// Update rhoInteractionPicture
		this->Rho += RhoAux;
		this->dipoleVersor += dipoleVersorAux;
	}
	else {
		Field EMiddle = 0.5 * (EPrev + ENext);
		Field V = 0.5 * (VPrev + VMiddle);
		Field VNext = 0.5 * this->mesh.dt * ((VMiddle - VPrev) / this->mesh.dt) + VMiddle;

		// First RK4 step
		TensorField KRho = this->rhoEquationSystem(this->Rho, this->dipoleVersor, EPrev, V);
		Field KDipoleVersor = this->dipoleVersorEquationSystem(this->dipoleVersor, EPrev);
		TensorField RhoAux = (this->mesh.dt / 6.0) * KRho;
		Field dipoleVersorAux = (this->mesh.dt / 6.0) * KDipoleVersor;

		// Second RK4 step
		KRho = this->rhoEquationSystem(this->Rho + (0.5 * this->mesh.dt) * KRho, this->dipoleVersor + (0.5 * this->mesh.dt) * KDipoleVersor, EMiddle, VMiddle);
		KDipoleVersor = this->dipoleVersorEquationSystem(this->dipoleVersor + (0.5 * this->mesh.dt) * KDipoleVersor, EPrev);
		RhoAux += (this->mesh.dt / 3.0) * KRho;
		dipoleVersorAux = (this->mesh.dt / 3.0) * KDipoleVersor;

		// Third RK4 step
		KRho = this->rhoEquationSystem(this->Rho + (0.5 * this->mesh.dt) * KRho, this->dipoleVersor + (0.5 * this->mesh.dt) * KDipoleVersor, EMiddle, VMiddle);
		KDipoleVersor = this->dipoleVersorEquationSystem(this->dipoleVersor + (0.5 * this->mesh.dt) * KDipoleVersor, EPrev);
		RhoAux += (this->mesh.dt / 3.0) * KRho;
		dipoleVersorAux = (this->mesh.dt / 3.0) * KDipoleVersor;

		// Fourth RK4 step
		KRho = this->rhoEquationSystem(this->Rho + this->mesh.dt * KRho, this->dipoleVersor + this->mesh.dt * KDipoleVersor, ENext, VNext);
		KDipoleVersor = this->dipoleVersorEquationSystem(this->dipoleVersor + this->mesh.dt * KDipoleVersor, EPrev);
		RhoAux += (this->mesh.dt / 6.0) * KRho;
		dipoleVersorAux = (this->mesh.dt / 6.0) * KDipoleVersor;

		// Update rhoInteractionPicture
		this->Rho += RhoAux;
		this->dipoleVersor += dipoleVersorAux;
	}
}

void Bloch::IntegrationStep(Field& EFieldPrev, Field& EField, const bool RK4, const bool interpolation) {
	if (!RK4) {
		this->RK2(EFieldPrev, EField, interpolation);
	}
	else {
		this->RK4(EFieldPrev, EField, interpolation);
	}
}

void Bloch::IntegrationStep(Field& EFieldPrev, Field& EField, Field& VelocityPrev, Field& Velocity, const bool RK4, const bool interpolation) {
	if (!RK4) {
		this->RK2(EFieldPrev, EField, VelocityPrev, Velocity, interpolation);
	}
	else {
		this->RK4(EFieldPrev, EField, VelocityPrev, Velocity, interpolation);
	}
}

void Bloch::push(Field& EFieldPrev, Field& EField, const bool RK4, const bool interpolation) {
	/// Call the integrator
	this->IntegrationStep(EFieldPrev.as(this->dipoleVersor.type), EField.as(this->dipoleVersor.type), RK4, interpolation);

	/// Renormalize versor 
	if (this->internalCounter % this->renormalizeInterval == 0)
		this->dipoleVersor.values = normalizeVector(this->dipoleVersor.values);

	/// Update the internal counter
	this->internalCounter++;
}

void Bloch::push(Field& EFieldPrev, Field& EField, Field& VelocityPrev, Field& Velocity, const bool RK4, const bool interpolation) {
	/// Call the integrator
	this->IntegrationStep(EFieldPrev.as(this->dipoleVersor.type), 
						  EField.as(this->dipoleVersor.type), 
						  VelocityPrev.as(this->dipoleVersor.type), 
						  Velocity.as(this->dipoleVersor.type), 
						  RK4, 
						  interpolation);

	/// Renormalize versor 
	if (this->internalCounter % this->renormalizeInterval == 0)
		this->dipoleVersor.values = normalizeVector(this->dipoleVersor.values);

	/// Update the internal counter
	this->internalCounter++;
}

Field Bloch::calculatePolarization(std::vector<double> polTerms) {
	return this->calculatePolarization(Field(FieldType::CENTER, af::constant(this->eta, this->mesh.Nx, this->mesh.Ny, this->mesh.Nz), this->mesh),
									   polTerms);
}

Field Bloch::calculatePolarization(const Field& ADensity, std::vector<double> polTerms) {
	/// Interpolate AtomicDensity to center
	Field AtomicDensity = ADensity.as(FieldType::CENTER);

	/// Calculate the polarization amplitude
	af::array polarizationAmplitude = af::constant(0.0, this->mesh.Nx, this->mesh.Ny, this->mesh.Nz, this->mesh.dtype());
	for (int i = 0; i < this->dim; i++) {
		if (polTerms[i] != 0.0) {
			/// Already accounting for the complex conj
			polarizationAmplitude += polTerms[i] * AtomicDensity.values * this->Rho[i][0];
		}
	}
	polarizationAmplitude = af::real(polarizationAmplitude);
	//polarizationAmplitude.eval();

	/// Calculate polarization vector
	Field Polarization = zeros(this->mesh, this->dipoleVersor.type, this->mesh.dtype());
	gfor(af::seq i, 3)
		Polarization.values(SPAN3, i) = polarizationAmplitude * this->dipoleVersor.values(SPAN3, i);
	//Polarization.values.eval();

	return Polarization;
}

af::array Bloch::dipolePotential(Field& EField, std::vector<double> dipoleForceTerms) {
	af::array potential = af::constant(0.0, this->mesh.Nx, this->mesh.Ny, this->mesh.Nz, this->mesh.dtype());
	for (int i = 0; i < this->dim; i++) {
		if (dipoleForceTerms[i] != 0.0)
			potential += af::sum((dipoleForceTerms[i] * dipoleVersor.values) * EField.values, 3) * this->Rho[i][0];
	}
	return af::real(potential);
}

Field Bloch::dipoleForce(Field& EField, std::vector<double> dipoleForceTerms, FieldType fieldType) {
	/// Calculate the dipole potential
	Field DipolePotential = Field(FieldType::CENTER, this->dipolePotential(EField.as(this->dipoleVersor.type), dipoleForceTerms), this->mesh);
	
	/// F = grad(U)
	return -1.0*grad(DipolePotential, fieldType);
}

void Bloch::saveProperties(const std::string resultsDir, const bool SI) {
	/// Aux variables for storage
	std::string filePath;
	std::ofstream output;

	/// Aux variables for SI conversion
	double muScaling = 1.0;
	double omegaScaling = 1.0;
	double gammaScaling = 1.0;
	if (SI) {
		std::vector<UnitsNames> U;
		std::vector<int> P;

		/// Mu scaling
		U = { UnitsNames::CHARGE, UnitsNames::LENGTH };
		P = { 1, 1 };
		muScaling = this->mesh.units.scalingFactorToSI(U, P);

		/// Omega Scalling
		U = { UnitsNames::TIME};
		P = { -1 };
		omegaScaling = this->mesh.units.scalingFactorToSI(U, P);

		/// Gamma scalling
		gammaScaling = omegaScaling;
	}

	/// Create file output path
	filePath = resultsDir + "\\rho_properties.txt";

	/// Create file and open
	output.open(filePath);

	/// Save number of levels
	output << this->levels << std::endl;

	output << std::endl;

	/// Save mu to file
	for (int i = 0; i < this->dim; i++) {
		for (int j = 0; j < this->dim - 1; j++) {
			output << muScaling * this->muMatrix[i][j]<< " ";
		}
		output << muScaling * this->muMatrix[i][this->dim-1]<< std::endl;
	}

	output << std::endl;

	/// Save omega to file
	for (int i = 0; i < this->dim; i++) {
		for (int j = 0; j < this->dim - 1; j++) {
			output << omegaScaling * this->omegaMatrix[i][j] << " ";
		}
		output << omegaScaling * this->omegaMatrix[i][this->dim - 1] << std::endl;
	}

	output << std::endl;

	/// Save gamma to file
	for (int i = 0; i < this->dim; i++) {
		for (int j = 0; j < this->dim - 1; j++) {
			output << gammaScaling * this->gammaMatrix[i][j] << " ";
		}
		output << gammaScaling * this->gammaMatrix[i][this->dim - 1] << std::endl;
	}

	/// Close file
	output.close();
}

void Bloch::saveToAF(const std::string resultsDir, const std::vector<bool> saveReal, const std::vector<bool> saveImag, const af::dtype precision) {

	/// Check if the given array is valid
	if (saveReal.size() != this->dim || saveImag.size() != this->dim) {
		throw std::invalid_argument("Bloch.save - Invalid size for save options");
		return;
	}

	/// Create the new file dir and save
	std::string filePath;
	std::string key;
	for (int i = 0; i < this->dim; i++) {
		if (saveReal[i]) {
			filePath = resultsDir + "\\rho" + std::to_string(i) + "real_" + std::to_string(this->internalCounter) + ".af";
			key = "rho" + std::to_string(i);
			saveAFValuesWithAF(filePath, key, af::real(this->Rho[i][0]).as(precision));
		}
		if (saveImag[i]) {
			filePath = resultsDir + "\\rho" + std::to_string(i) + "imag_" + std::to_string(this->internalCounter) + ".af";
			key = "rho" + std::to_string(i);
			saveAFValuesWithAF(filePath, key, af::imag(this->Rho[i][0]).as(precision));
		}
	}

	return;
}

void Bloch::savePolToAF(const std::string resultsDir, std::vector<double> polTerms, const bool SI, const af::dtype precision) {
	/// Create the new file dir and save
	std::string filePath;
	std::string key;

	filePath = resultsDir + "\\pol" + "_" + std::to_string(this->internalCounter) + ".af";
	key = "pol";
	double polScaling = 1.0;
	if (SI) {
		std::vector<UnitsNames> U = { UnitsNames::CHARGE, UnitsNames::LENGTH };
		std::vector<int> P = { 1, -2 };
		polScaling = this->mesh.units.scalingFactorToSI(U, P);
	}
	saveAFValuesWithAF(filePath, key, (polScaling * this->calculatePolarization(polTerms).values).as(precision));

	return;
}


void Bloch::saveState(std::string saveDir) {
	/// Save simulation properties
	this->saveProperties(saveDir, false);

	/// Save Bloch equatiosn arrays
	std::string filePath;
	for (int i = 0; i < this->dim; i++) {
		filePath = saveDir + "\\rho" + std::to_string(i) + ".af";
		af::saveArray(std::to_string(i).c_str(), this->Rho[i][0], filePath.c_str(), false);
	}
}

void Bloch::loadState(std::string loadDir) {
	/// Load Bloch equatiosn arrays
	std::string filePath;
	for (int i = 0; i < this->dim; i++) {
		filePath = loadDir + "\\rho" + std::to_string(i) + ".af";
		this->Rho[i][0] = af::readArray(filePath.c_str(), std::to_string(i).c_str());
	}
}