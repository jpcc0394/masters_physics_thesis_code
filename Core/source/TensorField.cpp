#include <TensorField.h>
#include <Mesh.h>
#include <arrayfire.h>

///////////////////////////// 10 YEARS OF BAD LUCK IF CHANGED //////////////////////////////
TensorField::Proxy::Proxy(const std::vector<af::array> column) : column(column) {};
af::array TensorField::Proxy::operator[] (const int index) const { return this->column[index]; }
///////////////////////////////////////////////////////////////////////////////////////////

//////////////////////// TensorField class definitions ////////////////////////
TensorField::TensorField(const TensorFieldType type, const int rows, const int cols, YeeMesh& mesh) : mesh(mesh) {
	if (type == TensorFieldType::CENTER_MATRIX) {
		this->type = type;
		this->rows = rows;
		this->cols = cols;
		for (int i = 0; i < this->rows; i++) {
			std::vector<af::array> V;
			this->values.push_back(V);
			for (int j = 0; j < this->cols; j++) {
				af::array C;
				this->values[i].push_back(C);
			}
		}
	}
	else 
		throw std::invalid_argument("TensorField.TensorField - Only CENTER_MATRIX type is implemented");
}

void TensorField::setValue(const int row, int col, const af::array v) {
	if (row <= this->rows - 1 || col <= this->cols - 1) {
		if (v.dims(0) == this->mesh.Nx && v.dims(1) == this->mesh.Ny && v.dims(2) == this->mesh.Nz) {
			this->values[row][col] = v;
			this->values[row][col].eval();	
		}
		else 
			throw std::invalid_argument("TensorField.setValue - Array does not have the dimensions of the current mesh");
	}
	else 
		throw std::invalid_argument("TensorField.setValue - Invalid indexes");
}

void TensorField::eval() const  {
	for (int i = 0; i < this->rows; i++)
		for (int j = 0; j < this->cols; j++)
			this->values[i][j].eval();
}

void TensorField::clear() {
	if (this->type == TensorFieldType::CENTER_MATRIX) {
		for (int i = 0; i < this->rows; i++)
			for (int j = 0; j < this->cols; j++) 
				if (! this->values[i][j].isempty()) {
					this->values[i][j](SPAN3) = 0.0;
					this->values[i][j].eval();
				}
	}
	else
		throw std::invalid_argument("TensorField.clear - Not implemented yet for this type");
}

TensorField::Proxy TensorField::operator[] (int index) const {
	return TensorField::Proxy(this->values[index]);
}

void TensorField::operator= (const TensorField& rhs) {
	if (this->mesh == rhs.mesh && this->type == rhs.type && this->values.size() == rhs.values.size() && this->values[0].size() == rhs.values[0].size()) {
		for (int i = 0; i < this->rows; i++) 
			for (int j = 0; j < this->cols; j++) {
				this->values[i][j] = rhs.values[i][j].copy();
				this->values[i][j].eval();
			}
	}
	else 
		throw std::invalid_argument("TensorField.operator= - Tensor fields structure does not match");
}

TensorField TensorField::operator+ (const TensorField& rhs) const {
	if (this->mesh == rhs.mesh && this->type == rhs.type && this->values.size() == rhs.values.size() && this->values[0].size() == rhs.values[0].size()) {
		TensorField output = TensorField(this->type, this->rows, this->cols, this->mesh);
		for (int i = 0; i < this->rows; i++) 
			for (int j = 0; j < this->cols; j++) {
				if (!this->values[i][j].isempty() && !rhs.values[i][j].isempty()) 
					output.setValue(i, j, this->values[i][j] + rhs.values[i][j]);
				else if (this->values[i][j].isempty() && !rhs.values[i][j].isempty())
						output.setValue(i, j, rhs.values[i][j]);
				else if (rhs.values[i][j].isempty() && !this->values[i][j].isempty())
					output.setValue(i, j, this->values[i][j]);
			}
		return output;
	}
	else 
		throw std::invalid_argument("TensorField.operator+ - Tensor fields structure does not match");
}

TensorField TensorField::operator+ (const double rhs) const {
	TensorField output = TensorField(this->type, this->rows, this->cols, this->mesh);
	for (int i = 0; i < this->rows; i++) 
		for (int j = 0; j < this->cols; j++) 
			if (!this->values[i][j].isempty())
				output.setValue(i, j, this->values[i][j] + rhs);
	return output;
}

TensorField operator+ (const double lhs, const TensorField& rhs) {
	TensorField output = TensorField(rhs.type, rhs.rows, rhs.cols, rhs.mesh);
	for (int i = 0; i < output.rows; i++)
		for (int j = 0; j < output.cols; j++)
			if (!rhs.values[i][j].isempty())
				output.setValue(i, j, lhs + rhs.values[i][j]);
	return output;
}

TensorField TensorField::operator+ (const Field& rhs) const {
	if (this->mesh == rhs.mesh && this->values[0][0].dims() == rhs.values.dims()) {
		TensorField output = TensorField(this->type, this->rows, this->cols, this->mesh);
		for (int i = 0; i < this->rows; i++) 
			for (int j = 0; j < this->cols; j++) {
				if (!this->values[i][j].isempty() && !rhs.values.isempty())
					output.setValue(i, j, this->values[i][j] + rhs.values);
				else if (rhs.values.isempty() && !this->values[i][j].isempty())
					output.setValue(i, j, this->values[i][j]);
			}
				
		return output;
	}
	else 
		throw std::invalid_argument("TensorField.operator+ - Tensor field and field structures do not match");
}

TensorField operator+ (const Field& lhs, const TensorField& rhs) {
	if (lhs.mesh == rhs.mesh && rhs.values[0][0].dims() == lhs.values.dims()) {
		TensorField output = TensorField(rhs.type, rhs.rows, rhs.cols, rhs.mesh);
		for (int i = 0; i < rhs.rows; i++)
			for (int j = 0; j < rhs.cols; j++) {
				if (!rhs.values[i][j].isempty() && !lhs.values.isempty())
					output.setValue(i, j, rhs.values[i][j] + lhs.values);
				else if (lhs.values.isempty() && !rhs.values[i][j].isempty())
					output.setValue(i, j, rhs.values[i][j]);
			}
		return output;
	}
	else
		throw std::invalid_argument("TensorField.operator+ - Field and Tensor field structures do not match");
}

TensorField TensorField::operator- (const TensorField& rhs) const {
	if (this->mesh == rhs.mesh && this->type == rhs.type && this->values.size() == rhs.values.size() && this->values[0].size() == rhs.values[0].size()) {
		TensorField output = TensorField(this->type, this->rows, this->cols, this->mesh);
		for (int i = 0; i < this->rows; i++)
			for (int j = 0; j < this->cols; j++) {
				if (!this->values[i][j].isempty() && !rhs.values[i][j].isempty()) 
					output.setValue(i, j, this->values[i][j] - rhs.values[i][j]);
				else if (this->values[i][j].isempty() && !rhs.values[i][j].isempty())
					output.setValue(i, j, -rhs.values[i][j]);
				else if (rhs.values[i][j].isempty() && !this->values[i][j].isempty())
					output.setValue(i, j, -this->values[i][j]);
			}
		return output;
	}
	else 
		throw std::invalid_argument("TensorField.operator- - Tensor fields structure does not match");
}

TensorField TensorField::operator- (const double rhs) const {
	TensorField output = TensorField(this->type, this->rows, this->cols, this->mesh);
	for (int i = 0; i < this->rows; i++)
		for (int j = 0; j < this->cols; j++)
			if (!this->values[i][j].isempty())
				output.setValue(i, j, this->values[i][j] - rhs);
	return output;
}

TensorField operator- (const double lhs, const TensorField& rhs) {
	TensorField output = TensorField(rhs.type, rhs.rows, rhs.cols, rhs.mesh);
	for (int i = 0; i < output.rows; i++)
		for (int j = 0; j < output.cols; j++)
			if (!rhs.values[i][j].isempty())
				output.setValue(i, j, lhs - rhs.values[i][j]);
	return output;
}

TensorField TensorField::operator- (const Field& rhs) const {
	if (this->mesh == rhs.mesh && this->values[0][0].dims() == rhs.values.dims()) {
		TensorField output = TensorField(this->type, this->rows, this->cols, this->mesh);
		for (int i = 0; i < this->rows; i++)
			for (int j = 0; j < this->cols; j++) {
				if (!this->values[i][j].isempty() && !rhs.values.isempty()) {
					output.setValue(i, j, this->values[i][j] - rhs.values);
				}
				else if (rhs.values.isempty() && !this->values[i][j].isempty())
					output.setValue(i, j, this->values[i][j]);
			}

		return output;
	}
	else 
		throw std::invalid_argument("TensorField.operator- - Tensor field and field structures do not match");
}

TensorField operator- (const Field& lhs, const TensorField& rhs) {
	if (lhs.mesh == rhs.mesh && rhs.values[0][0].dims() == lhs.values.dims()) {
		TensorField output = TensorField(rhs.type, rhs.rows, rhs.cols, rhs.mesh);
		for (int i = 0; i < rhs.rows; i++)
			for (int j = 0; j < rhs.cols; j++) {
				if (!rhs.values[i][j].isempty() && !lhs.values.isempty()) {
					output.setValue(i, j, rhs.values[i][j] - lhs.values);
				}
				else if (lhs.values.isempty() && !rhs.values[i][j].isempty())
					output.setValue(i, j, -rhs.values[i][j]);
			}
		return output;
	}
	else
		throw std::invalid_argument("TensorField.operator- - Field and Tensor field structures do not match");
}

TensorField TensorField::operator* (const TensorField& rhs) const {
	if (this->mesh == rhs.mesh && this->type == rhs.type && this->cols == rhs.rows) {
		TensorField output = TensorField(this->type, this->rows, rhs.cols, this->mesh);
		for (int i = 0; i < output.rows; i++)
			for (int j = 0; j < output.cols; j++) {
				for (int k = 0; k < rhs.rows; k++) {
					if (!this->values[i][k].isempty() && !rhs.values[k][j].isempty()) {
						if (output.values[i][j].isempty())
							output.values[i][j] = this->values[i][k] * rhs.values[k][j];
						else
							output.values[i][j] += this->values[i][k] * rhs.values[k][j];
					}
				}
				output.values[i][j].eval();
			}
				
		return output;
	}
	else 
		throw std::invalid_argument("TensorField.operator* - Tensor fields structure does not match");
}

TensorField TensorField::operator* (const double rhs) const {
	TensorField output = TensorField(this->type, this->rows, this->cols, this->mesh);
	for (int i = 0; i < this->rows; i++) 
		for (int j = 0; j < this->cols; j++) 
			if (!this->values[i][j].isempty())
				output.setValue(i, j, this->values[i][j] * rhs);
	return output;
}

TensorField operator* (const double lhs, const TensorField& rhs) {
	TensorField output = TensorField(rhs.type, rhs.rows, rhs.cols, rhs.mesh);
	for (int i = 0; i < output.rows; i++)
		for (int j = 0; j < output.cols; j++)
			if (!rhs.values[i][j].isempty())
				output.setValue(i, j, lhs * rhs.values[i][j]);
	return output;
}

TensorField TensorField::operator* (const Field& rhs) const {
	if (this->mesh == rhs.mesh && this->values[0][0].dims() == rhs.values.dims()) {
		TensorField output = TensorField(this->type, this->rows, this->cols, this->mesh);
		for (int i = 0; i < this->rows; i++)
			for (int j = 0; j < this->cols; j++) {
				if (!this->values[i][j].isempty() && !rhs.values.isempty()) {
					output.setValue(i, j, this->values[i][j] * rhs.values);
				}
				else if (rhs.values.isempty() && !this->values[i][j].isempty())
					output.setValue(i, j, this->values[i][j]);
			}

		return output;
	}
	else 
		throw std::invalid_argument("TensorField.operator* - Tensor field and field structures do not match");
}

TensorField operator* (const Field& lhs, const TensorField& rhs) {
	if (lhs.mesh == rhs.mesh && rhs.values[0][0].dims() == lhs.values.dims()) {
		TensorField output = TensorField(rhs.type, rhs.rows, rhs.cols, rhs.mesh);
		for (int i = 0; i < rhs.rows; i++)
			for (int j = 0; j < rhs.cols; j++) 
				if (!rhs.values[i][j].isempty() && !lhs.values.isempty()) 
					output.setValue(i, j, rhs.values[i][j] * lhs.values);
		return output;
	}
	else
		throw std::invalid_argument("TensorField.operator* - Field and Tensor field structures do not match");
}

TensorField TensorField::operator/ (const double rhs) const {
	TensorField output = TensorField(this->type, this->rows, this->cols, this->mesh);
	for (int i = 0; i < this->rows; i++)
		for (int j = 0; j < this->cols; j++)
			if (!this->values[i][j].isempty())
				output.setValue(i, j, this->values[i][j] / rhs);
	return output;
}

TensorField operator/ (const double lhs, const TensorField& rhs) {
	TensorField output = TensorField(rhs.type, rhs.rows, rhs.cols, rhs.mesh);
	for (int i = 0; i < output.rows; i++)
		for (int j = 0; j < output.cols; j++)
			if (!rhs.values[i][j].isempty())
				output.setValue(i, j, lhs / rhs.values[i][j]);
	return output;
}

TensorField TensorField::operator/ (const Field& rhs) const {
	if (this->mesh == rhs.mesh && this->values[0][0].dims() == rhs.values.dims()) {
		TensorField output = TensorField(this->type, this->rows, this->cols, this->mesh);
		for (int i = 0; i < this->rows; i++)
			for (int j = 0; j < this->cols; j++) {
				if (!this->values[i][j].isempty() && !rhs.values.isempty()) {
					output.setValue(i, j, this->values[i][j] / rhs.values);
				}
				else if (rhs.values.isempty() && !this->values[i][j].isempty())
					output.setValue(i, j, this->values[i][j]);
			}

		return output;
	}
	else 
		throw std::invalid_argument("TensorField.operator/ - Tensor field and field structures do not match");
}

TensorField operator/ (const Field& lhs, const TensorField& rhs) {
	if (lhs.mesh == rhs.mesh && rhs.values[0][0].dims() == lhs.values.dims()) {
		TensorField output = TensorField(rhs.type, rhs.rows, rhs.cols, rhs.mesh);
		for (int i = 0; i < rhs.rows; i++)
			for (int j = 0; j < rhs.cols; j++)
				if (!rhs.values[i][j].isempty() && !lhs.values.isempty())
					output.setValue(i, j, rhs.values[i][j] * lhs.values);
		return output;
	}
	else
		throw std::invalid_argument("TensorField.operator* - Field and Tensor field structures do not match");
}

void TensorField::operator+= (const TensorField& rhs) {
	if (this->mesh == rhs.mesh && this->type == rhs.type && this->values.size() == rhs.values.size() && this->values[0].size() == rhs.values[0].size()) {
		for (int i = 0; i < this->rows; i++) 
			for (int j = 0; j < this->cols; j++) {
				if (! this->values[i][j].isempty() && ! rhs.values[i][j].isempty()) {
					this->values[i][j] += rhs.values[i][j];
					this->values[i][j].eval();
				}
				else if (this->values[i][j].isempty() && !rhs.values[i][j].isempty())
						this->setValue(i, j, rhs.values[i][j]);
			}
	}
	else 
		throw std::invalid_argument("TensorField.operator+= - Tensor fields structure does not match");
}

void TensorField::operator+= (const double rhs) {
	for (int i = 0; i < this->rows; i++) 
		for (int j = 0; j < this->cols; j++) 
			if (!this->values[i][j].isempty()) {
				this->values[i][j] += rhs;
				this->values[i][j].eval();
			}
}

void TensorField::operator+= (const Field& rhs) {
	if (this->mesh == rhs.mesh && this->values[0][0].dims() == rhs.values.dims()) {
		for (int i = 0; i < this->rows; i++)
			for (int j = 0; j < this->cols; j++)
				if (!this->values[i][j].isempty() && !rhs.values.isempty()) {
					this->values[i][j] += rhs.values;
					this->values[i][j].eval();
				}
	}
	else 
		throw std::invalid_argument("TensorField.operator+= - Tensor field and field structures do not match");
}

void TensorField::operator-= (const TensorField& rhs) {
	if (this->mesh == rhs.mesh && this->type == rhs.type && this->values.size() == rhs.values.size() && this->values[0].size() == rhs.values[0].size()) {
		for (int i = 0; i < this->rows; i++)
			for (int j = 0; j < this->cols; j++) {
				if (!this->values[i][j].isempty() && rhs.values[i][j].isempty()) {
					this->values[i][j] -= rhs.values[i][j];
					this->values[i][j].eval();
				}
				else if (this->values[i][j].isempty() && !rhs.values[i][j].isempty())
					this->setValue(i, j, -rhs.values[i][j]);
			}
	}
	else 
		throw std::invalid_argument("TensorField.operator-= - Tensor fields structure does not match");
}

void TensorField::operator-= (const double rhs) {
	for (int i = 0; i < this->rows; i++)
		for (int j = 0; j < this->cols; j++)
			if (!this->values[i][j].isempty()) {
				this->values[i][j] -= rhs;
				this->values[i][j].eval();
			}
}

void TensorField::operator-= (const Field& rhs) {
	if (this->mesh == rhs.mesh && this->values[0][0].dims() == rhs.values.dims()) {
		for (int i = 0; i < this->rows; i++)
			for (int j = 0; j < this->cols; j++)
				if (!this->values[i][j].isempty() && !rhs.values.isempty()) {
					this->values[i][j] -= rhs.values;
					this->values[i][j].eval();
				}
	}
	else 
		throw std::invalid_argument("TensorField.operator-= - Tensor field and field structures do not match");
}

void TensorField::operator*= (const double rhs) {
	for (int i = 0; i < this->rows; i++)
		for (int j = 0; j < this->cols; j++)
			if (!this->values[i][j].isempty()) {
				this->values[i][j] *= rhs;
				this->values[i][j].eval();
			}
}

void TensorField::operator*= (const Field& rhs) {
	if (this->mesh == rhs.mesh && this->values[0][0].dims() == rhs.values.dims()) {
		for (int i = 0; i < this->rows; i++)
			for (int j = 0; j < this->cols; j++)
				if (!this->values[i][j].isempty() && !rhs.values.isempty()) {
					this->values[i][j] *= rhs.values;
					this->values[i][j].eval();
				}
	}
	else 
		throw std::invalid_argument("TensorField.operator*= - Tensor field and field structures do not match");
}

void TensorField::operator/= (const double rhs) {
	for (int i = 0; i < this->rows; i++)
		for (int j = 0; j < this->cols; j++)
			if (!this->values[i][j].isempty()) {
				this->values[i][j] /= rhs;
				this->values[i][j].eval();
			}
}

void TensorField::operator/= (const Field& rhs) {
	if (this->mesh == rhs.mesh && this->values[0][0].dims() == rhs.values.dims()) {
		for (int i = 0; i < this->rows; i++)
			for (int j = 0; j < this->cols; j++)
				if (!this->values[i][j].isempty() && !rhs.values.isempty()) {
					this->values[i][j] /= rhs.values;
					this->values[i][j].eval();
				}
	}
	else 
		throw std::invalid_argument("TensorField.operator/= - Tensor field and field structures do not match");
}