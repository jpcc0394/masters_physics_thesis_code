#include <Mesh.h>
#include <Misc.h>

//////////////////////// YeeMesh class definitions ////////////////////////
YeeMesh::YeeMesh(Units units, int Nx, int Ny, int Nz, double dx, double dy, double dz, double dt, af::dtype afType) : units(units),
																													   Nx(Nx), Ny(Ny), Nz(Nz),
																													   dx(dx), dy(dy), dz(dz), dt(dt),
																													   Lx(Nx*dx), Ly(Ny*dy), Lz(Nz*dz),
																													   afType(afType) {}

YeeMesh::YeeMesh(int Nx, int Ny, int Nz, double dx, double dy, double dz, double dt, af::dtype afType) : units(Units(UnitSystems::NATURAL_LH)),
																										 Nx(Nx), Ny(Ny), Nz(Nz),
																										 dx(dx), dy(dy), dz(dz), dt(dt),
																										 Lx(Nx*dx), Ly(Ny*dy), Lz(Nz*dz),
																										 afType(afType) {}

bool YeeMesh::operator==(YeeMesh& mesh2) {
	return this == &mesh2;
}

af::array YeeMesh::x(Grid gridType) {
	double aux = 0.0;
	if (gridType == Grid::CENTER ||
		gridType == Grid::FACE_Y ||
		gridType == Grid::FACE_Z ||
		gridType == Grid::EDGE_X)
		aux = 0.5 * this->dx;

	return aux + this->dx*af::range(this->Nx, this->Ny, this->Nz, 1, 0, this->afType);
}

af::array YeeMesh::y(Grid gridType) {
	double aux = 0.0;
	if (gridType == Grid::CENTER ||
		gridType == Grid::FACE_X ||
		gridType == Grid::FACE_Z ||
		gridType == Grid::EDGE_Y)
		aux = 0.5*this->dy;
	
	return aux + this->dy*af::range(this->Nx, this->Ny, this->Nz, 1, 1, this->afType);
}

af::array YeeMesh::z(Grid gridType) {
	double aux = 0.0;
	if (gridType == Grid::CENTER ||
		gridType == Grid::FACE_X ||
		gridType == Grid::FACE_Y ||
		gridType == Grid::EDGE_Z)
		aux = 0.5*this->dz;

	return aux + this->dz*af::range(this->Nx, this->Ny, this->Nz, 1, 2, this->afType);
}

void YeeMesh::save(const std::string resultsDir, const bool SI) {

	/// Aux variables for SI conversion
	double LScaling = 1.0;
	double TScaling = 1.0;
	if (SI) {
		std::vector<UnitsNames> U;
		std::vector<int> P;

		/// Spatial grid Scaling
		U = { UnitsNames::LENGTH};
		P = { 1 };
		LScaling = this->units.scalingFactorToSI(U, P);

		/// Time scaling
		U = { UnitsNames::TIME };
		P = { 1 };
		TScaling = this->units.scalingFactorToSI(U, P);
	}

	/// Create file output path
	std::string filePath = resultsDir + "\\Mesh.txt";

	/// Create file and open
	std::ofstream output(filePath);

	/// Write information to file
	output << this->Nx << " " << this->Ny << " " << this->Nz << std::endl;
	output << LScaling * this->Lx << " " << LScaling * this->Ly << " " << LScaling * this->Lz << std::endl;
	output << LScaling * this->dx << " " << LScaling * this->dy << " " << LScaling * this->dz << std::endl;
	output << TScaling * this->dt;

	output.close();
}

//////////////////////// Field class definitions ////////////////////////
Field::Field(FieldType type, af::array values, YeeMesh& mesh) : type(type), values(values), mesh(mesh) {
	if (values.isempty());
	else if (mesh.Nx == values.dims(0) && mesh.Ny == values.dims(1) && mesh.Nz == values.dims(2)) {
		switch (type) {
		case FieldType::CENTER:
			if (values.dims(3) != 1)
				throw std::invalid_argument("Field::Field(FieldType type, af::array values, YeeMesh& mesh)  ->  For CENTER fields the fourth dimension of the array must be 1.");
			break;
		case FieldType::CENTER_VECTOR:
		case FieldType::FACE:
		case FieldType::EDGE:
			if (values.dims(3) != 3)
				throw std::invalid_argument("Field::Field(FieldType type, af::array values, YeeMesh& mesh)  ->  For CENTER_VECTOR, FACE and EDGE fields the fourth dimension of the array must be 3.");
			break;
		}
		this->values.eval();
	}
	else
		throw std::invalid_argument("Unmatched dimensions between mesh and array.");
}

Field Field::as(const FieldType type) const {
	if (this->type == type) {
		return *this;
	}
	else {
		return interpolate(*this, type);
	}
}

Field Field::shift(const FieldDir shiftDir) const {
	Field output = zeros(this->mesh, this->type, f64);
	switch (shiftDir)
	{
	case FieldDir::X_P:
		output.values = af::shift(this->values(SPAN3, 0), -1, 0, 0);
		break;
	case FieldDir::X_N:
		output.values = af::shift(this->values(SPAN3, 0), 1, 0, 0);
		break;
	case FieldDir::Y_P:
		output.values = af::shift(this->values(SPAN3, 1), 0, -1, 0);
		break;
	case FieldDir::Y_N:
		output.values = af::shift(this->values(SPAN3, 1), 0, 1, 0);
		break;
	case FieldDir::Z_P:
		output.values = af::shift(this->values(SPAN3, 2), 0, 0, -1);
		break;
	case FieldDir::Z_N:
		output.values = af::shift(this->values(SPAN3, 2), 0, 0, 1);
		break;
	default:
		throw std::invalid_argument("Field.shift - Invalid shift direction");
		break;
	}
	return output;
}

Field Field::allShift(const int direction) const {
	return Field(this->type, af::shift(this-values, direction, direction, direction), this->mesh);
}

void Field::clear() {
	if (!this->values.isempty()) {
		if (this->type == FieldType::CENTER)
			this->values(SPAN3) = 0.0;
		else if (this->type == FieldType::EDGE || this->type == FieldType::FACE || this->type == FieldType::CENTER_VECTOR)
			this->values(SPAN4) = 0.0;
		this->values.eval();
	}
	else
		throw std::invalid_argument("Field.clear - Can't clear an <empty> field");
}

void Field::operator=(Field& rhs) {
	if (this->mesh == rhs.mesh) {
		if (this->type == rhs.type) {
			this->values = rhs.values.copy();
			this->values.eval();
		}
		else
			throw std::invalid_argument("Field.operator=  ->  Unmatched Field.type.");
	}
	else
		throw std::invalid_argument("Field.operator=  ->  Unmatched YeeMesh.");
}

inline bool binFieldFieldCheck(Field& lhs, Field& rhs) {
	return lhs.mesh == rhs.mesh  &&  lhs.type == rhs.type  &&  !lhs.isempty()  &&  !rhs.isempty();
}

void binFieldFieldThrow(Field& lhs, Field& rhs, std::string& functionIdetifier) {
	std::string errorMsg;
	errorMsg += functionIdetifier + std::string("  ->  ");
	if (!(lhs.mesh == rhs.mesh))
		errorMsg += std::string("Unmatched 'YeeMesh' instance Field::mesh.\n");
	else if (!(lhs.type == rhs.type))
		errorMsg += std::string("Unmatched Field.type.");
	else if (lhs.isempty())
		errorMsg += std::string("Field.values of left hand side is empty.\n");
	else if (rhs.isempty())
		errorMsg += std::string("Field.values of right hand side is empty.\n");
	throw std::invalid_argument(errorMsg);
}

Field operator+(Field& lhs, Field& rhs) {
	if (binFieldFieldCheck(lhs, rhs))
		return Field(lhs.type, lhs.values + rhs.values, lhs.mesh);
	else
		binFieldFieldThrow(lhs, rhs, std::string("Field operator+(Field& lhs, Field& rhs)"));
}

Field operator+(Field& lhs, double rhs) {
	if (!lhs.isempty())
		return Field(lhs.type, lhs.values + rhs, lhs.mesh);
	else
		throw std::invalid_argument("Field operator+(Field& lhs, double rhs)  ->  Can't perform operation with an empy Field.");
}

Field operator+(double lhs, Field& rhs) {
	if (!rhs.isempty())
		return Field(rhs.type, lhs + rhs.values, rhs.mesh);
	else
		throw std::invalid_argument("Field operator+(double lhs, Field& rhs)  ->  Can't perform operation with an empy Field.");
}

void operator+=(Field& lhs, Field& rhs) {
	if (binFieldFieldCheck(lhs, rhs)) {
		lhs.values += rhs.values;
		lhs.values.eval();
	}
	else
		binFieldFieldThrow(lhs, rhs, std::string("void operator+=(Field& lhs, Field& rhs)"));
}

void operator+=(Field& lhs, double rhs) {
	if (!lhs.isempty()) {
		lhs.values += rhs;
		lhs.values.eval();
	}
	else
		throw std::invalid_argument("Field operator+=(Field& lhs, double rhs)  ->  Can't perform operation with an empy Field.");
}

Field operator-(Field& lhs, Field& rhs) {
	if (binFieldFieldCheck(lhs, rhs))
		return Field(lhs.type, lhs.values - rhs.values, lhs.mesh);
	else
		binFieldFieldThrow(lhs, rhs, std::string("Field operator-(Field& lhs, Field& rhs)"));
}

Field operator-(Field& lhs, double rhs) {
	if (!lhs.isempty())
		return Field(lhs.type, lhs.values - rhs, lhs.mesh);
	else
		throw std::invalid_argument("Field operator-(Field& lhs, double rhs)  ->  Can't perform operation with an empy Field.");
}

Field operator-(double lhs, Field& rhs) {
	if (!rhs.isempty())
		return Field(rhs.type, lhs - rhs.values, rhs.mesh);
	else
		throw std::invalid_argument("Field operator-(double lhs, Field& rhs)  ->  Can't perform operation with an empy Field.");
}

void operator-=(Field& lhs, Field& rhs) {
	if (binFieldFieldCheck(lhs, rhs)) {
		lhs.values -= rhs.values;
		lhs.values.eval();
	}
	else
		binFieldFieldThrow(lhs, rhs, std::string("void operator-=(Field& lhs, Field& rhs)"));
}

void operator-=(Field& lhs, double rhs) {
	if (!lhs.isempty()) {
		lhs.values -= rhs;
		lhs.values.eval();
	}
	else
		throw std::invalid_argument("Field operator-=(Field& lhs, double rhs)  ->  Can't perform operation with an empy Field.");
}

Field operator*(Field& lhs, Field& rhs) {
	if (binFieldFieldCheck(lhs, rhs))
		return Field(lhs.type, lhs.values * rhs.values, lhs.mesh);
	else
		binFieldFieldThrow(lhs, rhs, std::string("Field operator*(Field& lhs, Field& rhs)"));
}

Field operator*(Field& lhs, double rhs) {
	if (!lhs.isempty())
		return Field(lhs.type, lhs.values*rhs, lhs.mesh);
	else
		throw std::invalid_argument("Field operator*(Field& lhs, double rhs)  ->  Can't perform operation with an empy Field.");
}

Field operator*(double lhs, Field& rhs) {
	if (!rhs.isempty())
		return Field(rhs.type, lhs*rhs.values, rhs.mesh);
	else
		throw std::invalid_argument("Field operator*(double lhs, Field& rhs)  ->  Can't perform operation with an empy Field.");
}

void operator*=(Field& lhs, Field& rhs) {
	if (binFieldFieldCheck(lhs, rhs)) {
		lhs.values *= rhs.values;
		lhs.values.eval();
	}
	else
		binFieldFieldThrow(lhs, rhs, std::string("void operator*=(Field& lhs, Field& rhs)"));
}

void operator*=(Field& lhs, double rhs) {
	if (!lhs.isempty()) {
		lhs.values *= rhs;
		lhs.values.eval();
	}
	else
		throw std::invalid_argument("Field operator*=(Field& lhs, double rhs)  ->  Can't perform operation with an empy Field.");
}

Field operator/(Field& lhs, Field& rhs) {
	if (binFieldFieldCheck(lhs, rhs))
		return Field(lhs.type, lhs.values / rhs.values, lhs.mesh);
	else
		binFieldFieldThrow(lhs, rhs, std::string("Field operator/(Field& lhs, Field& rhs)"));
}

Field operator/(Field& lhs, double rhs) {
	if (rhs == 1.0)
		return lhs;
	else if (rhs == 0.0)
		throw std::invalid_argument("operator/(Field& lhs, double rhs)  ->  rhs cannot be zero.");
	else {
		if (!lhs.isempty())
			return Field(lhs.type, lhs.values / rhs, lhs.mesh);
		else
			throw std::invalid_argument("Field operator/(Field& lhs, double rhs)  ->  Can't perform operation with an empy Field.");
	}
}

Field operator/(double lhs, Field& rhs) {
	if (!rhs.isempty())
		return Field(rhs.type, lhs / rhs.values, rhs.mesh);
	else
		throw std::invalid_argument("Field operator/(double lhs, Field& rhs)  ->  Can't perform operation with an empy Field.");
}

void operator/=(Field& lhs, Field& rhs) {
	if (binFieldFieldCheck(lhs, rhs)) {
		lhs.values /= rhs.values;
		lhs.values.eval();
	}
	else
		binFieldFieldThrow(lhs, rhs, std::string("void operator/=(Field& lhs, Field& rhs)"));
}

void operator/=(Field& lhs, double rhs) {
	if (!lhs.isempty()) {
		lhs.values /= rhs;
		lhs.values.eval();
	}
	else
		throw std::invalid_argument("Field operator/=(Field& lhs, double rhs)  ->  Can't perform operation with an empy Field.");
}

Boundary::Boundary() : M_Type(BoundaryType::PERIODIC), M_BoundaryU(BoundaryUnion()), M_BoundaryAxis(BoundaryAxis::X_MAX) {
	this->M_BoundaryU.Periodic = '1';
};

Boundary::Boundary(BoundaryType Type, BoundaryAxis Axis, BoundaryUnion BoundaryU) : M_Type(Type), M_BoundaryU(BoundaryU), M_BoundaryAxis(Axis) {};

BoundaryType Boundary::Type() {
	return this->M_Type;
}

BoundaryUnion Boundary::Properties() {
	return this->M_BoundaryU;
}

BoundaryAxis Boundary::Axis() {
	return this->M_BoundaryAxis;
}

af::array Boundary::ABCFMin(af::array AxisPoints, double AxisL) {
	return -this->M_BoundaryU.ABC.C * af::pow((this->M_BoundaryU.ABC.L - AxisPoints) / this->M_BoundaryU.ABC.L, this->M_BoundaryU.ABC.POW) * (AxisPoints <= this->M_BoundaryU.ABC.L);
}

af::array Boundary::ABCFMax(af::array AxisPoints, double AxisL) {
	return -this->M_BoundaryU.ABC.C * af::pow((this->M_BoundaryU.ABC.L - AxisL + AxisPoints) / this->M_BoundaryU.ABC.L, this->M_BoundaryU.ABC.POW) * (AxisPoints >= AxisL - this->M_BoundaryU.ABC.L);
}

af::array Boundary::Values(YeeMesh &Mesh, Grid GridPos) {
	if (this->M_Type == BoundaryType::ABC) {
		af::array BoundValues = af::constant(1.0, Mesh.Nx, Mesh.Ny, Mesh.Nz, Mesh.afType);
		switch (this->M_BoundaryAxis)
		{
		case BoundaryAxis::X_MIN:
			BoundValues += this->ABCFMin(Mesh.x(GridPos), Mesh.Lx);
			break;
		case BoundaryAxis::Y_MIN:
			BoundValues += this->ABCFMin(Mesh.y(GridPos), Mesh.Ly);
			break;
		case BoundaryAxis::Z_MIN:
			BoundValues += this->ABCFMin(Mesh.z(GridPos), Mesh.Lz);
			break;
		case BoundaryAxis::X_MAX:
			BoundValues += this->ABCFMax(Mesh.x(GridPos), Mesh.Lx);
			break;
		case BoundaryAxis::Y_MAX:
			BoundValues += this->ABCFMax(Mesh.y(GridPos), Mesh.Ly);
			break;
		case BoundaryAxis::Z_MAX:
			BoundValues += this->ABCFMax(Mesh.z(GridPos), Mesh.Lz);
			break;
		default:
			throw(std::invalid_argument("Boundary.Values - How did you reach this case is beyond us"));
			break;
		}
		return BoundValues;
	}
	else if (this->M_Type == BoundaryType::PERIODIC || this->M_Type == BoundaryType::UPML) {
		return af::constant(1.0, Mesh.Nx, Mesh.Ny, Mesh.Nz, Mesh.afType);
	}
	else if (this->M_Type == BoundaryType::REFLECTIVE) {
		throw(std::invalid_argument("Boundary.Values - Reflective boundaries not implemented yet"));
	}
}

af::array Boundary::UPMLKMax(af::array AxisPoints, double AxisL) {
	return ((this->M_BoundaryU.UPML.K - 1.0)
		* af::pow((AxisPoints - AxisL + this->M_BoundaryU.UPML.L) / this->M_BoundaryU.UPML.L, this->M_BoundaryU.UPML.POW)
		* (AxisPoints >= AxisL - this->M_BoundaryU.UPML.L));
}

af::array Boundary::UPMLSigmaMax(af::array AxisPoints, double AxisL) {
	return (this->M_BoundaryU.UPML.Sigma
		* af::pow((AxisPoints - AxisL + this->M_BoundaryU.UPML.L) / this->M_BoundaryU.UPML.L, this->M_BoundaryU.UPML.POW)
		* (AxisPoints >= AxisL - this->M_BoundaryU.UPML.L));
}

af::array Boundary::UPMLKMin(af::array AxisPoints, double AxisL) {
	return ((this->M_BoundaryU.UPML.K - 1.0)
		* af::pow((this->M_BoundaryU.UPML.L - AxisPoints) / this->M_BoundaryU.UPML.L, this->M_BoundaryU.UPML.POW)
		* (AxisPoints <= this->M_BoundaryU.UPML.L));
}

af::array Boundary::UPMLSigmaMin(af::array AxisPoints, double AxisL) {
	return (this->M_BoundaryU.UPML.Sigma
		* af::pow((this->M_BoundaryU.UPML.L - AxisPoints) / this->M_BoundaryU.UPML.L, this->M_BoundaryU.UPML.POW)
		* (AxisPoints <= this->M_BoundaryU.UPML.L));
}