#include <EMF.h>


EMF::EMF(YeeMesh& mesh, bool savePrevE, bool savePrevB, bool savePrevD, bool savePrevH) : mesh(mesh), 
															savePrevE(savePrevE), savePrevB(savePrevB), savePrevD(savePrevD), savePrevH(savePrevH),
															hasUPML(false), hasMultiplicative(false), boundariesLocked(false), externalLocked(false),
															m_Efield(zeros(this->mesh, FieldType::FACE, mesh.dtype())), m_EfieldPrev(FieldType::FACE, af::array(), mesh),
															m_Bfield(zeros(this->mesh, FieldType::EDGE, mesh.dtype())), m_BfieldPrev(FieldType::EDGE, af::array(), mesh),
															m_Dfield(zeros(this->mesh, FieldType::FACE, mesh.dtype())), m_DfieldPrev(FieldType::FACE, af::array(), mesh),
															m_Hfield(zeros(this->mesh, FieldType::EDGE, mesh.dtype())), m_HfieldPrev(FieldType::EDGE, af::array(), mesh) {

	if (mesh.dt*mesh.units.c()*std::sqrt(1.0/(mesh.dx*mesh.dx) + 1.0/(mesh.dy*mesh.dy) + 1.0/(mesh.dz*mesh.dz)) > 1.0)
		throw std::invalid_argument("EMF::EMF(YeeMesh& mesh, bool savePrevE, bool savePrevB)  ->  The Courant condition is not obeyed by the numerical constants in the YeeMesh provided.\n");

	if (this->savePrevE) 
		this->m_EfieldPrev = zeros(this->mesh, FieldType::FACE, mesh.dtype());
	if (this->savePrevB)
		this->m_BfieldPrev = zeros(this->mesh, FieldType::EDGE, mesh.dtype());
	if (this->savePrevD)
		this->m_DfieldPrev = zeros(this->mesh, FieldType::FACE, mesh.dtype());
	if (this->savePrevH)
		this->m_HfieldPrev = zeros(this->mesh, FieldType::EDGE, mesh.dtype());

	this->internalCounter = 0;
}

void EMF::addGaussianPulsePlaneWave(PropagationDir direction, EFieldPol EPol, double r0, double waveNumber, double phase, double FWHM, double intensity) {

	// Create aux varibles for the initialization of the Electric and Magnetic fields
	FieldProperties prop = FieldProperties(direction, EPol);

	double sigma = FWHM / (2*std::sqrt(2*std::log(2)));

	this->m_Efield.values(SPAN3, prop.EDim) += prop.ESign * intensity*af::sin(waveNumber*(this->mesh.*prop.MeshFunc)(prop.EGrid) + phase)*af::exp(-af::pow((this->mesh.*prop.MeshFunc)(prop.EGrid) - r0, 2.0) / (2.0*sigma*sigma));
	this->m_Bfield.values(SPAN3, prop.BDim) += prop.BSign * intensity/this->mesh.units.c()*af::sin(waveNumber*(this->mesh.*prop.MeshFunc)(prop.BGrid) + phase)*af::exp(-af::pow((this->mesh.*prop.MeshFunc)(prop.BGrid) - r0, 2.0) / (2.0*FWHM*FWHM));

	this->m_Efield.values.eval();
	this->m_Bfield.values.eval();
}

void EMF::addPlaneWave(PropagationDir direction, EFieldPol EPol, double waveNumber, double phase, double intensity) {
	// Create aux varibles for the initialization of the Electric and Magnetic fields
	FieldProperties prop = FieldProperties(direction, EPol);

	this->m_Efield.values(SPAN3, prop.EDim) += prop.ESign * intensity*af::sin(waveNumber*(this->mesh.*prop.MeshFunc)(prop.EGrid) + phase);
	this->m_Bfield.values(SPAN3, prop.BDim) += prop.BSign * intensity/this->mesh.units.c()*af::sin(waveNumber*(this->mesh.*prop.MeshFunc)(prop.BGrid) + phase);

	this->m_Efield.values.eval();
	this->m_Bfield.values.eval();
}

void EMF::getMultiplicativeBoundaryValues() {
	/// Basic structures
	this->MultiplicativeBoundaryFace = af::constant(1.0, this->m_Efield.values.dims(), this->m_Efield.values.type());
	this->MultiplicativeBoundaryEdge = af::constant(1.0, this->m_Efield.values.dims(), this->m_Efield.values.type());
	af::array auxBoundary = af::array(this->m_Efield.values.dims(), this->m_Efield.values.type());

	/// X Axis Face
	auxBoundary(SPAN3, 0) = this->XAxisMinBoundary.Values(this->mesh, Grid::FACE_X) * this->XAxisMaxBoundary.Values(this->mesh, Grid::FACE_X);
	auxBoundary(SPAN3, 1) = this->XAxisMinBoundary.Values(this->mesh, Grid::FACE_Y) * this->XAxisMaxBoundary.Values(this->mesh, Grid::FACE_Y);
	auxBoundary(SPAN3, 2) = this->XAxisMinBoundary.Values(this->mesh, Grid::FACE_Z) * this->XAxisMaxBoundary.Values(this->mesh, Grid::FACE_Z);
	this->MultiplicativeBoundaryFace *= auxBoundary;

	/// X Axis Edge
	auxBoundary(SPAN3, 0) = this->XAxisMinBoundary.Values(this->mesh, Grid::EDGE_X) * this->XAxisMaxBoundary.Values(this->mesh, Grid::EDGE_X);
	auxBoundary(SPAN3, 1) = this->XAxisMinBoundary.Values(this->mesh, Grid::EDGE_Y) * this->XAxisMaxBoundary.Values(this->mesh, Grid::EDGE_Y);
	auxBoundary(SPAN3, 2) = this->XAxisMinBoundary.Values(this->mesh, Grid::EDGE_Z) * this->XAxisMaxBoundary.Values(this->mesh, Grid::EDGE_Z);
	this->MultiplicativeBoundaryEdge *= auxBoundary;

	/// Y Axis Face
	auxBoundary(SPAN3, 0) = this->YAxisMinBoundary.Values(this->mesh, Grid::FACE_X) * this->YAxisMaxBoundary.Values(this->mesh, Grid::FACE_X);
	auxBoundary(SPAN3, 1) = this->YAxisMinBoundary.Values(this->mesh, Grid::FACE_Y) * this->YAxisMaxBoundary.Values(this->mesh, Grid::FACE_Y);
	auxBoundary(SPAN3, 2) = this->YAxisMinBoundary.Values(this->mesh, Grid::FACE_Z) * this->YAxisMaxBoundary.Values(this->mesh, Grid::FACE_Z);
	this->MultiplicativeBoundaryFace *= auxBoundary;

	/// Y Axis Edge
	auxBoundary(SPAN3, 0) = this->YAxisMinBoundary.Values(this->mesh, Grid::EDGE_X) * this->YAxisMaxBoundary.Values(this->mesh, Grid::EDGE_X);
	auxBoundary(SPAN3, 1) = this->YAxisMinBoundary.Values(this->mesh, Grid::EDGE_Y) * this->YAxisMaxBoundary.Values(this->mesh, Grid::EDGE_Y);
	auxBoundary(SPAN3, 2) = this->YAxisMinBoundary.Values(this->mesh, Grid::EDGE_Z) * this->YAxisMaxBoundary.Values(this->mesh, Grid::EDGE_Z);
	this->MultiplicativeBoundaryEdge *= auxBoundary;

	/// Z Axis Face
	auxBoundary(SPAN3, 0) = this->ZAxisMinBoundary.Values(this->mesh, Grid::FACE_X) * this->YAxisMaxBoundary.Values(this->mesh, Grid::FACE_X);
	auxBoundary(SPAN3, 1) = this->ZAxisMinBoundary.Values(this->mesh, Grid::FACE_Y) * this->YAxisMaxBoundary.Values(this->mesh, Grid::FACE_Y);
	auxBoundary(SPAN3, 2) = this->ZAxisMinBoundary.Values(this->mesh, Grid::FACE_Z) * this->YAxisMaxBoundary.Values(this->mesh, Grid::FACE_Z);
	this->MultiplicativeBoundaryFace *= auxBoundary;

	/// Z Axis Edge
	auxBoundary(SPAN3, 0) = this->ZAxisMinBoundary.Values(this->mesh, Grid::EDGE_X) * this->YAxisMaxBoundary.Values(this->mesh, Grid::EDGE_X);
	auxBoundary(SPAN3, 1) = this->ZAxisMinBoundary.Values(this->mesh, Grid::EDGE_Y) * this->YAxisMaxBoundary.Values(this->mesh, Grid::EDGE_Y);
	auxBoundary(SPAN3, 2) = this->ZAxisMinBoundary.Values(this->mesh, Grid::EDGE_Z) * this->YAxisMaxBoundary.Values(this->mesh, Grid::EDGE_Z);
	this->MultiplicativeBoundaryEdge *= auxBoundary;
}

void EMF::applyMultiplicativeBoundaries() {
	if (!this->MultiplicativeBoundaryFace.isempty()) {
		this->m_Dfield.values *= this->MultiplicativeBoundaryFace;
		this->m_Efield.values *= this->MultiplicativeBoundaryFace;
	}
	if (!this->MultiplicativeBoundaryEdge.isempty()) {
		this->m_Bfield.values *= this->MultiplicativeBoundaryEdge;
		this->m_Hfield.values *= this->MultiplicativeBoundaryEdge;
	}
}

#define	eps0 this->mesh.units.eps0()
#define	mu0 this->mesh.units.mu0()
#define	dt this->mesh.dt
#define Dx this->m_Dfield.values(SPAN3, 0)
#define Dy this->m_Dfield.values(SPAN3, 1)
#define Dz this->m_Dfield.values(SPAN3, 2)
#define Dx_Prev this->m_DfieldPrev.values(SPAN3, 0)
#define Dy_Prev this->m_DfieldPrev.values(SPAN3, 1)
#define Dz_Prev this->m_DfieldPrev.values(SPAN3, 2)
#define Ex this->m_Efield.values(SPAN3, 0)
#define Ey this->m_Efield.values(SPAN3, 1)
#define Ez this->m_Efield.values(SPAN3, 2)
#define Ex_Prev this->m_EfieldPrev.values(SPAN3, 0)
#define Ey_Prev this->m_EfieldPrev.values(SPAN3, 1)
#define Ez_Prev this->m_EfieldPrev.values(SPAN3, 2)
#define Bx this->m_Bfield.values(SPAN3, 0)
#define By this->m_Bfield.values(SPAN3, 1)
#define Bz this->m_Bfield.values(SPAN3, 2)
#define Bx_Prev this->m_BfieldPrev.values(SPAN3, 0)
#define By_Prev this->m_BfieldPrev.values(SPAN3, 1)
#define Bz_Prev this->m_BfieldPrev.values(SPAN3, 2)
#define Hx this->m_Hfield.values(SPAN3, 0)
#define Hy this->m_Hfield.values(SPAN3, 1)
#define Hz this->m_Hfield.values(SPAN3, 2)
#define Hx_Prev this->m_HfieldPrev.values(SPAN3, 0)
#define Hy_Prev this->m_HfieldPrev.values(SPAN3, 1)
#define Hz_Prev this->m_HfieldPrev.values(SPAN3, 2)

void EMF::pushElectric(Field& Jfield, Field& Pfield) {

	Field J = Jfield + this->currentSources();

	if (!this->hasUPML) {
		this->m_Dfield += dt*(curl(this->m_Hfield, FieldType::FACE) - J.as(this->m_Dfield.type));
		this->m_Efield = (this->m_Dfield - Pfield.as(this->m_Efield.type))/eps0;
	}
	else {
		af::array sigma;
		af::array k;

		/// D Field push
		this->m_Dfield = curl(this->m_Hfield, FieldType::FACE) - J.as(this->m_Dfield.type);
		/// Apply DX UPML
		sigma = this->SigmaY(Grid::FACE_X);
		k = this->KY(Grid::FACE_X);
		Dx *= ((2.0*eps0*dt) / (2.0*eps0*k + sigma*dt));
		Dx += ((2.0*eps0*k - sigma*dt) / (2.0*eps0*k + sigma*dt)) * Dx_Prev;
		/// Apply DY UPML
		sigma = this->SigmaZ(Grid::FACE_Y);
		k = this->KZ(Grid::FACE_Y);
		Dy *= ((2.0*eps0*dt) / (2.0*eps0*k + sigma*dt));
		Dy += ((2.0*eps0*k - sigma*dt) / (2.0*eps0*k + sigma*dt)) * Dy_Prev;
		/// Apply DZ UPML
		sigma = this->SigmaX(Grid::FACE_Z);
		k = this->KX(Grid::FACE_Z);
		Dz *= ((2.0*eps0*dt) / (2.0*eps0*k + sigma*dt));
		Dz += ((2.0*eps0*k - sigma*dt) / (2.0*eps0*k + sigma*dt)) * Dz_Prev;

		this->m_Dfield.values.eval();

		/// E Field push
		/// Apply EX UPML
		k = this->KX(Grid::FACE_X);
		sigma = this->SigmaX(Grid::FACE_X);
		Ex = ((2.0*eps0*k + sigma*dt)*Dx - (2.0*eps0*k - sigma*dt)*Dx_Prev - Pfield.as(this->m_Efield.type).values(SPAN3, 0));
		k = this->KZ(Grid::FACE_X);
		sigma = this->SigmaZ(Grid::FACE_X);
		Ex /= (2.0*eps0*k + sigma*dt)*eps0;
		Ex += ((2.0*eps0*k - sigma*dt) / (2.0*eps0*k + sigma*dt)) * Ex_Prev;
		/// Apply EY UPML
		k = this->KY(Grid::FACE_Y);
		sigma = this->SigmaY(Grid::FACE_Y);
		Ey = ((2.0*eps0*k + sigma*dt)*Dy - (2.0*eps0*k - sigma*dt)*Dy_Prev - Pfield.as(this->m_Efield.type).values(SPAN3, 1));
		k = this->KX(Grid::FACE_Y);
		sigma = this->SigmaX(Grid::FACE_Y);
		Ey /= (2.0*eps0*k + sigma*dt)*eps0;
		Ey += ((2.0*eps0*k - sigma*dt) / (2.0*eps0*k + sigma*dt)) * Ey_Prev;
		/// Apply EZ UPML
		k = this->KZ(Grid::FACE_Z);
		sigma = this->SigmaZ(Grid::FACE_Z);
		Ez = ((2.0*eps0*k + sigma*dt)*Dz - (2.0*eps0*k - sigma*dt)*Dz_Prev - Pfield.as(this->m_Efield.type).values(SPAN3, 2));
		k = this->KY(Grid::FACE_Z);
		sigma = this->SigmaY(Grid::FACE_Z);
		Ez /= (2.0*eps0*k + sigma*dt)*eps0;
		Ez += ((2.0*eps0*k - sigma*dt) / (2.0*eps0*k + sigma*dt)) * Ez_Prev;

		this->m_Efield.values.eval();
	}
}

void EMF::pushMagnetic(Field& Mfield, double dtFactor) {
	if (!this->hasUPML) {
		this->m_Bfield += -dtFactor*dt*curl(this->m_Efield, FieldType::EDGE);
		this->m_Hfield = this->m_Bfield / mu0 - Mfield.as(this->m_Hfield.type);
	}
	else {
		af::array sigma;
		af::array k;

		/// B Field push
		this->m_Bfield = curl(this->m_Efield, FieldType::EDGE);
		/// Apply BX UPML
		sigma = this->SigmaY(Grid::EDGE_X);
		k = this->KY(Grid::EDGE_X);
		Bx *= ((-2.0*eps0*dt) / (2.0*eps0*k + sigma*dt));
		Bx += ((2.0*eps0*k - sigma*dt) / (2.0*eps0*k + sigma*dt)) * Bx_Prev;
		/// Apply BY UPML
		sigma = this->SigmaZ(Grid::EDGE_Y);
		k = this->KZ(Grid::EDGE_Y);
		By *= ((-2.0*eps0*dt) / (2.0*eps0*k + sigma*dt));
		By += ((2.0*eps0*k - sigma*dt) / (2.0*eps0*k + sigma*dt)) * By_Prev;
		/// Apply BZ UPML
		sigma = this->SigmaX(Grid::EDGE_Z);
		k = this->KX(Grid::EDGE_Z);
		Bz *= ((-2.0*eps0*dt) / (2.0*eps0*k + sigma*dt));
		Bz += ((2.0*eps0*k - sigma*dt) / (2.0*eps0*k + sigma*dt)) * Bz_Prev;

		this->m_Bfield.values.eval();

		/// H Field push
		/// Apply HX UPML
		k = this->KX(Grid::FACE_X);
		sigma = this->SigmaX(Grid::FACE_X);
		Hx = ((2.0*eps0*k + sigma*dt)*Bx - (2.0*eps0*k - sigma*dt)*Bx_Prev - Mfield.as(this->m_Hfield.type).values(SPAN3, 0));
		k = this->KZ(Grid::FACE_X);
		sigma = this->SigmaZ(Grid::FACE_X);
		Hx /= (2.0*eps0*k + sigma*dt)*mu0;
		Hx += ((2.0*eps0*k - sigma*dt) / (2.0*eps0*k + sigma*dt)) * Hx_Prev;
		/// Apply HY UPML
		k = this->KY(Grid::FACE_Y);
		sigma = this->SigmaY(Grid::FACE_Y);
		Hy = ((2.0*eps0*k + sigma*dt)*By - (2.0*eps0*k - sigma*dt)*By_Prev - Mfield.as(this->m_Hfield.type).values(SPAN3, 1));
		k = this->KX(Grid::FACE_Y);
		sigma = this->SigmaX(Grid::FACE_Y);
		Hy /= (2.0*eps0*k + sigma*dt)*mu0;
		Hy += ((2.0*eps0*k - sigma*dt) / (2.0*eps0*k + sigma*dt)) * Hy_Prev;
		/// Apply HZ UPML
		k = this->KZ(Grid::FACE_Z);
		sigma = this->SigmaZ(Grid::FACE_Z);
		Hz = ((2.0*eps0*k + sigma*dt)*Bz - (2.0*eps0*k - sigma*dt)*Bz_Prev - Mfield.as(this->m_Hfield.type).values(SPAN3, 2));
		k = this->KY(Grid::FACE_Z);
		sigma = this->SigmaY(Grid::FACE_Z);
		Hz /= (2.0*eps0*k + sigma*dt)*mu0;
		Hz += ((2.0*eps0*k - sigma*dt) / (2.0*eps0*k + sigma*dt)) * Hz_Prev;

		this->m_Hfield.values.eval();
	}
}

void EMF::borisPush(Field& Jfield, Field& Pfield, Field& Mfield, Field& Charge) {
	this->plotAll();

	if (this->savePrevD)
		this->m_DfieldPrev = this->m_Dfield;

	if (this->savePrevE)
		this->m_EfieldPrev = this->m_Efield;

	if (this->savePrevB)
		this->m_BfieldPrev = this->m_Bfield;

	if (this->savePrevH)
		this->m_HfieldPrev = this->m_Hfield;

	//// Simple PIC like push
	this->m_Efield += dt*(curl(this->m_Bfield, FieldType::FACE)/(eps0*mu0) - Jfield/eps0);
	this->m_Efield -= grad(poissonSolver(div(this->m_Efield, FieldType::CENTER) - Charge/eps0), FieldType::FACE);
	this->m_Bfield -= dt*curl(this->m_Efield, FieldType::EDGE);

	//this->m_Dfield += dt*(curl(this->m_Hfield, FieldType::FACE) - Jfield.as(this->m_Dfield.type));
	//this->m_Dfield -= grad(poissonSolver(div(this->m_Dfield, FieldType::CENTER)) - Charge, FieldType::FACE);
	//this->m_Efield  = (this->m_Dfield - Pfield.as(this->m_Efield.type))/eps0;

	//this->m_Bfield += -dt*curl(this->m_Efield, FieldType::EDGE);
	//this->m_Hfield  = this->m_Bfield/mu0 - Mfield.as(this->m_Hfield.type);


	(this->internalCounter)++;
}

#undef eps0
#undef mu0
#undef dt
#undef Dx 
#undef Dy 
#undef Dz 
#undef Dx_Prev 
#undef Dy_Prev 
#undef Dz_Prev 
#undef Ex 
#undef Ey 
#undef Ez 
#undef Ex_Prev 
#undef Ey_Prev 
#undef Ez_Prev 
#undef Bx 
#undef By 
#undef Bz 
#undef Bx_Prev 
#undef By_Prev 
#undef Bz_Prev 
#undef Hx 
#undef Hy 
#undef Hz 
#undef Hx_Prev 
#undef Hy_Prev
#undef Hz_Prev

void EMF::push() {
	this->push(zeros(this->mesh, FieldType::FACE, this->m_Hfield.dtype()), zeros(this->mesh, FieldType::FACE, this->m_Hfield.dtype()), zeros(this->mesh, FieldType::EDGE, this->m_Hfield.dtype()));
}

void EMF::push(Field& Jfield, Field& Pfield, Field& Mfield) {
	this->plotAll();

	if (this->savePrevD) {
		this->m_DfieldPrev = this->m_Dfield;
		this->m_DfieldPrev.eval();
	}
	if (this->savePrevE) {
		this->m_EfieldPrev = this->m_Efield;
		this->m_EfieldPrev.eval();
	}
	if (this->savePrevB) {
		this->m_BfieldPrev = this->m_Bfield;
		this->m_BfieldPrev.eval();
	}
	if (this->savePrevH) {
		this->m_HfieldPrev = this->m_Hfield;
		this->m_HfieldPrev.eval();
	}
	//// Simple PIC like push
	//this->m_Efield += this->mesh.dt*(curl(this->m_Bfield, FieldType::FACE)/(unitCnst::eps0*unitCnst::mu0) - J/unitCnst::eps0);
	//this->m_Bfield -= this->mesh.dt*curl(this->m_Efield, FieldType::EDGE);
	
	this->pushElectric(Jfield, Pfield);
	this->pushMagnetic(Mfield);

	/// Apply Multiplicative boundary conditions
	if (this->hasMultiplicative) {
		this->applyMultiplicativeBoundaries();
	}

	this->m_Efield.eval();
	this->m_Dfield.eval();
	this->m_Bfield.eval();
	this->m_Hfield.eval();

	(this->internalCounter)++;
}

void EMF::initialize() {
	this->initialize(zeros(this->mesh, FieldType::CENTER, this->mesh.dtype()), 
					 zeros(this->mesh, FieldType::FACE  , this->mesh.dtype()),
					 zeros(this->mesh, FieldType::FACE  , this->mesh.dtype()),
					 zeros(this->mesh, FieldType::EDGE  , this->mesh.dtype()));
}

void EMF::initialize(Field& chargeDist, Field& Jfield, Field& Pfield, Field& Mfield) {

	if (this->hasUPML && (!this->savePrevE || !this->savePrevD || !this->savePrevB || !this->savePrevH)) {
		throw(std::invalid_argument("EMF.initialize - To use UPMLs you need to save previous fields"));
		return;
	}
	else {

		//this->m_Efield -= grad(poissonSolver(-1.0*chargeDist/unitCnst::eps0), FieldType::FACE);
		//this->m_Bfield -= 0.5*this->mesh.dt * curl(this->m_Efield, FieldType::EDGE);


		// E and D initialization
		this->m_Dfield += grad(poissonSolver(chargeDist), FieldType::FACE);
		//this->m_Dfield += gaussLawSolver(chargeDist, FieldType::FACE, 1e-14, 10.0);
		this->m_Efield += (this->m_Dfield - Pfield.as(this->m_Efield.type)) / this->mesh.units.eps0();
		this->m_Dfield = this->mesh.units.eps0() * this->m_Efield;

		if (this->savePrevD)
			this->m_DfieldPrev = this->m_Dfield;
		if (this->savePrevE)
			this->m_EfieldPrev = this->m_Efield;

		// B and H initialization and advancement to dt/2
		//this->m_Bfield += curl(poissonSolver(-unitCnst::mu0 * J), FieldType::EDGE); // Smilei doesn't use this (don't know why).
		this->m_Hfield += this->m_Bfield / this->mesh.units.mu0() - Mfield.as(this->m_Hfield.type);
		if (this->savePrevB)
			this->m_BfieldPrev = this->m_Bfield;
		if (this->savePrevH)
			this->m_HfieldPrev = this->m_Hfield;
		
		this->pushMagnetic(Mfield, 0.5);
		//this->m_Bfield += -0.5 * this->mesh.dt * curl(this->m_Efield, FieldType::EDGE);
		//this->m_Hfield += this->m_Bfield / this->mesh.units.mu0() - Mfield.as(this->m_Hfield.type);

		/// Obtain Boundary arrays
		if (this->hasMultiplicative)
			this->getMultiplicativeBoundaryValues();


		/// Lock addition of boundaries and current sources
		this->boundariesLocked = true;
		this->externalLocked = true;
	}
} 

void EMF::plotLine(LinePlotParam& params) {
	if (this->internalCounter % params.skipRepr == 0){
		if (params.direction == 0){
			if (params.component == 3)
				params.window.plot( this->mesh.dx * af::range(this->mesh.Nx)  ,  af::sqrt(af::pow(this->Efield().values(SPAN, params.idx1, params.idx2, 0).as(f32), 2.0) + 
																						  af::pow(this->Efield().values(SPAN, params.idx1, params.idx2, 1).as(f32), 2.0) + 
																						  af::pow(this->Efield().values(SPAN, params.idx1, params.idx2, 2).as(f32), 2.0)) );
			else
				params.window.plot( this->mesh.dx * af::range(this->mesh.Nx)  ,  (this->Efield().values(SPAN, params.idx1, params.idx2, params.component)).as(f32) );
		}
		else if (params.direction == 1)
			params.window.plot( this->mesh.dy * af::range(this->mesh.Ny)  ,  (this->Efield().values(params.idx1, SPAN, params.idx2, params.component)).as(f32) );
		else if (params.direction == 2)
			params.window.plot( this->mesh.dz * af::range(this->mesh.Nz)  ,  (this->Efield().values(params.idx1, params.idx2, SPAN, params.component)).as(f32) );
	}
}

void EMF::plotPlane(PlanePlotParam& params) {
	if (this->internalCounter % params.skipRepr == 0) {
		af::array aux;
		if (params.perpendicularDirection == 0)
			aux = this->Efield().values(params.idx, SPAN2, params.component).as(f32);
		else if (params.perpendicularDirection == 1)
			aux = this->Efield().values(SPAN, params.idx, SPAN, params.component).as(f32);
		else if (params.perpendicularDirection == 2) {
			if (params.component >= 0 && params.component <= 2)
				aux = this->Efield().values(SPAN2, params.idx, params.component).as(f32);
			else if (params.component == 3)
				aux = interpolate(this->Efield(), FieldType::CENTER_VECTOR).values;
				aux = af::sqrt(af::pow(aux(SPAN2, params.idx, 0).as(f32), 2.0) + af::pow(aux(SPAN2, params.idx, 1).as(f32), 2.0) + af::pow(aux(SPAN2, params.idx, 2).as(f32), 2.0));
		}
		else
			throw std::invalid_argument("EMF::plotPlane(PlanePlotParam& params)  ->  Invalid plotting dimension.");
		
		params.window.image(   normalizeForImage(aux)   );
	}
}

void EMF::plotAll() {
	int i;
	for (i = 0; i < this->linePlots.size(); i++)
		this->plotLine(this->linePlots[i]);
	for (i = 0; i < this->planePlots.size(); i++)
		this->plotPlane(this->planePlots[i]);
}

void EMF::addLinePlot(af::Window& window, int direction, int idx1, int idx2, int component, int skipRepr) {
	this->linePlots.push_back(LinePlotParam(window, direction, idx1, idx2, component, skipRepr));
}

void EMF::addPlanePlot(af::Window& window, int perpendicularDirection, int idx, int component, int skipRepr) {
	this->planePlots.push_back(PlanePlotParam(window, perpendicularDirection, idx, component, skipRepr));
}

FieldProperties::FieldProperties(PropagationDir direction, EFieldPol EPol) {
	// Test if desired configuration is physically possible
	if (((direction == PropagationDir::X_P || direction == PropagationDir::X_N) && EPol != EFieldPol::X) ||
		((direction == PropagationDir::Y_P || direction == PropagationDir::Y_N) && EPol != EFieldPol::Y) ||
		((direction == PropagationDir::Z_P || direction == PropagationDir::Z_N) && EPol != EFieldPol::Z)) {

		// Assign the variables according to the selected propagation direction
		switch (direction)
		{
		case PropagationDir::X_P:
			if (EPol == EFieldPol::Y) {
				this->EDim = 1;
				this->BDim = 2;
				this->EGrid = Grid::FACE_Y;
				this->BGrid = Grid::EDGE_Z;
				this->ESign = 1;
				this->BSign = 1;
			}
			else if (EPol == EFieldPol::Z) {
				this->EDim = 2;
				this->BDim = 1;
				this->EGrid = Grid::FACE_Z;
				this->BGrid = Grid::EDGE_Y;
				this->ESign = 1;
				this->BSign = -1;
			}
			else {
				throw std::invalid_argument("FieldProperties.FieldProperties - Invalid electric field polarization");
				break;
			}
			this->MeshFunc = &YeeMesh::x;
			break;
		case PropagationDir::X_N:
			if (EPol == EFieldPol::Y) {
				this->EDim = 1;
				this->BDim = 2;
				this->EGrid = Grid::FACE_Y;
				this->BGrid = Grid::EDGE_Z;
				this->ESign = 1;
				this->BSign = -1;
			}
			else if (EPol == EFieldPol::Z) {
				this->EDim = 2;
				this->BDim = 1;
				this->EGrid = Grid::FACE_Z;
				this->BGrid = Grid::EDGE_Y;
				this->ESign = 1;
				this->BSign = 1;
			}
			else {
				throw std::invalid_argument("FieldProperties.FieldProperties - Invalid electric field polarization");
				break;
			}
			this->MeshFunc = &YeeMesh::x;
			break;
		case PropagationDir::Y_P:
			if (EPol == EFieldPol::X) {
				this->EDim = 0;
				this->BDim = 2;
				this->EGrid = Grid::FACE_X;
				this->BGrid = Grid::EDGE_Z;
				this->ESign = 1;
				this->BSign = -1;
			}
			else if (EPol == EFieldPol::Z) {
				this->EDim = 2;
				this->BDim = 0;
				this->EGrid = Grid::FACE_Z;
				this->BGrid = Grid::EDGE_X;
				this->ESign = 1;
				this->BSign = 1;
			}
			else {
				throw std::invalid_argument("FieldProperties.FieldProperties - Invalid electric field polarization");
				break;
			}
			this->MeshFunc = &YeeMesh::y;
			break;
		case PropagationDir::Y_N:
			if (EPol == EFieldPol::X) {
				this->EDim = 0;
				this->BDim = 2;
				this->EGrid = Grid::FACE_X;
				this->BGrid = Grid::EDGE_Z;
				this->ESign = 1;
				this->BSign = 1;
			}
			else if (EPol == EFieldPol::Z) {
				this->EDim = 2;
				this->BDim = 0;
				this->EGrid = Grid::FACE_Z;
				this->BGrid = Grid::EDGE_X;
				this->ESign = 1;
				this->BSign = -1;
			}
			else {
				throw std::invalid_argument("FieldProperties.FieldProperties - Invalid electric field polarization");
				break;
			}
			this->MeshFunc = &YeeMesh::y;
			break;
		case PropagationDir::Z_P:
			if (EPol == EFieldPol::X) {
				this->EDim = 0;
				this->BDim = 1;
				this->EGrid = Grid::FACE_X;
				this->BGrid = Grid::EDGE_Y;
				this->ESign = 1;
				this->BSign = -1;
			}
			else if (EPol == EFieldPol::Y) {
				this->EDim = 1;
				this->BDim = 0;
				this->EGrid = Grid::FACE_Y;
				this->BGrid = Grid::EDGE_X;
				this->ESign = 1;
				this->BSign = 1;
			}
			else {
				throw std::invalid_argument("FieldProperties.FieldProperties - Invalid electric field polarization");
				break;
			}
			this->MeshFunc = &YeeMesh::z;
			break;
		case PropagationDir::Z_N:
			if (EPol == EFieldPol::X) {
				this->EDim = 0;
				this->BDim = 1;
				this->EGrid = Grid::FACE_X;
				this->BGrid = Grid::EDGE_Y;
				this->ESign = 1;
				this->BSign = -1;
			}
			else if (EPol == EFieldPol::Y) {
				this->EDim = 1;
				this->BDim = 0;
				this->EGrid = Grid::FACE_Y;
				this->BGrid = Grid::EDGE_X;
				this->ESign = 1;
				this->BSign = 1;
			}
			else {
				throw std::invalid_argument("FieldProperties.FieldProperties - Invalid electric field polarization");
				break;
			}
			this->MeshFunc = &YeeMesh::z;
			break;
		default:
			throw std::invalid_argument("FieldProperties.FieldProperties - Invalid propagation direction");
			break;
		}

	}
	else {
		throw std::invalid_argument("FieldProperties.FieldProperties - Invalid combination of propagation direction and electric field polarization");
	}

}

void EMF::saveToTxt(const std::string saveDir, const bool SI) {
	/// Aux variables for SI conversion
	double DScaling = 1.0;
	double EScaling = 1.0;
	double BScaling = 1.0;
	double HScaling = 1.0;

	/// Convertion to SI
	if (SI) {
		/// D scaling
		std::vector<UnitsNames> DUnit = { UnitsNames::CHARGE, UnitsNames::LENGTH };
		std::vector<int> DPower = { 1, -2 };
		DScaling = this->mesh.units.scalingFactorToSI(DUnit, DPower);

		/// E scaling
		std::vector<UnitsNames> EUnit = { UnitsNames::MASS , UnitsNames::LENGTH , UnitsNames::TIME , UnitsNames::CHARGE };
		std::vector<int> EPower = { 1, 1, -2, -1 };
		EScaling = this->mesh.units.scalingFactorToSI(EUnit, EPower);

		/// B scaling
		std::vector<UnitsNames> BUnit = { UnitsNames::MASS , UnitsNames::TIME , UnitsNames::CHARGE };
		std::vector<int> BPower = { 1, -1, -1 };
		BScaling = this->mesh.units.scalingFactorToSI(BUnit, BPower);

		/// H scaling
		std::vector<UnitsNames> HUnit = { UnitsNames::CHARGE , UnitsNames::LENGTH , UnitsNames::TIME};
		std::vector<int> HPower = { 1, -1, -1 };
		HScaling = this->mesh.units.scalingFactorToSI(HUnit, HPower);
	}

	/// Create save paths
	std::string DPath = saveDir + "\\D_" + std::to_string(this->internalCounter) + ".txt";
	std::string EPath = saveDir + "\\E_" + std::to_string(this->internalCounter) + ".txt";
	std::string BPath = saveDir + "\\B_" + std::to_string(this->internalCounter) + ".txt";
	std::string HPath = saveDir + "\\H_" + std::to_string(this->internalCounter) + ".txt";

	/// Save data
	saveAFValuesToFile(DPath, DScaling * this->Dfield().values);
	saveAFValuesToFile(EPath, EScaling * this->Efield().values);
	saveAFValuesToFile(BPath, BScaling * this->Bfield().values);
	saveAFValuesToFile(HPath, HScaling * this->Hfield().values);
}

void EMF::saveToAF(const std::string saveDir, const std::vector<bool> fieldSelector, const bool SI, const af::dtype precision, FieldType fieldType) {
	/// Aux variables for SI conversion
	double DScaling = 1.0;
	double EScaling = 1.0;
	double BScaling = 1.0;
	double HScaling = 1.0;

	/// Convertion to SI
	if (SI) {
		/// D scaling
		/// D scaling
		std::vector<UnitsNames> DUnit = { UnitsNames::CHARGE, UnitsNames::LENGTH };
		std::vector<int> DPower = { 1, -2 };
		DScaling = this->mesh.units.scalingFactorToSI(DUnit, DPower);

		/// E scaling
		std::vector<UnitsNames> EUnit = { UnitsNames::MASS , UnitsNames::LENGTH , UnitsNames::TIME , UnitsNames::CHARGE };
		std::vector<int> EPower = { 1, 1, -2, -1 };
		EScaling = this->mesh.units.scalingFactorToSI(EUnit, EPower);

		/// B scaling
		std::vector<UnitsNames> BUnit = { UnitsNames::MASS , UnitsNames::TIME , UnitsNames::CHARGE };
		std::vector<int> BPower = { 1, -1, -1 };
		BScaling = this->mesh.units.scalingFactorToSI(BUnit, BPower);

		/// H scaling
		std::vector<UnitsNames> HUnit = { UnitsNames::CHARGE , UnitsNames::LENGTH , UnitsNames::TIME };
		std::vector<int> HPower = { 1, -1, -1 };
		HScaling = this->mesh.units.scalingFactorToSI(HUnit, HPower);
	}

	/// Create save paths
	std::string DPath = saveDir + "\\D_" + std::to_string(this->internalCounter) + ".af";
	std::string EPath = saveDir + "\\E_" + std::to_string(this->internalCounter) + ".af";
	std::string BPath = saveDir + "\\B_" + std::to_string(this->internalCounter) + ".af";
	std::string HPath = saveDir + "\\H_" + std::to_string(this->internalCounter) + ".af";

	/// Save data
	if(fieldSelector[0])
		saveAFValuesWithAF(DPath, "D", (DScaling * this->Dfield().as(fieldType).values).as(precision));
	if (fieldSelector[1])
		saveAFValuesWithAF(EPath, "E", (EScaling * this->Efield().as(fieldType).values).as(precision));
	if (fieldSelector[2])
		saveAFValuesWithAF(BPath, "B", (BScaling * this->Bfield().as(fieldType).values).as(precision));
	if (fieldSelector[3])
		saveAFValuesWithAF(HPath, "H", (HScaling * this->Hfield().as(fieldType).values).as(precision));
}

void EMF::saveState(const std::string saveDir) {

	/// Create save paths
	std::string DPath = saveDir + "\\D.af";
	std::string EPath = saveDir + "\\E.af";
	std::string BPath = saveDir + "\\B.af";
	std::string HPath = saveDir + "\\H.af";

	/// Save current state of the arrays
	af::saveArray(std::string("D").c_str(), this->m_Dfield.values, DPath.c_str(), false);
	af::saveArray(std::string("E").c_str(), this->m_Efield.values, EPath.c_str(), false);
	af::saveArray(std::string("B").c_str(), this->m_Bfield.values, BPath.c_str(), false);
	af::saveArray(std::string("H").c_str(), this->m_Hfield.values, HPath.c_str(), false);
}

void EMF::loadState(const std::string loadDir) {
	/// Create save paths
	std::string DPath = loadDir + "\\D.af";
	std::string EPath = loadDir + "\\E.af";
	std::string BPath = loadDir + "\\B.af";
	std::string HPath = loadDir + "\\H.af";

	this->m_Dfield.values = af::readArray(DPath.c_str(), "D");
	this->m_Efield.values = af::readArray(EPath.c_str(), "E");
	this->m_Bfield.values = af::readArray(BPath.c_str(), "B");
	this->m_Hfield.values = af::readArray(HPath.c_str(), "H");
}

void EMF::addBoundaryCondition(Boundary Bound) {
	BoundaryType A = Bound.Type();
	if (!this->boundariesLocked) {
		if (Bound.Type() == BoundaryType::UPML) {
			this->hasUPML = true;
		}
		else if (Bound.Type() == BoundaryType::ABC) {
			this->hasMultiplicative = true;
		}
		switch (Bound.Axis())
		{
		case BoundaryAxis::X_MIN:
			if (this->mesh.Nx > 1)
				this->XAxisMinBoundary = Bound;
			else
				throw(std::invalid_argument("EMF.addBoundaryCondition - Axis must have more than 1 point"));
			break;
		case BoundaryAxis::Y_MIN:
			if (this->mesh.Ny > 1)
				this->YAxisMinBoundary = Bound;
			else
				throw(std::invalid_argument("EMF.addBoundaryCondition - Axis must have more than 1 point"));
			break;
		case BoundaryAxis::Z_MIN:
			if (this->mesh.Nz > 1)
				this->ZAxisMinBoundary = Bound;
			else
				throw(std::invalid_argument("EMF.addBoundaryCondition - Axis must have more than 1 point"));
			break;
		case BoundaryAxis::X_MAX:
			if (this->mesh.Nx > 1)
				this->XAxisMaxBoundary = Bound;
			else
				throw(std::invalid_argument("EMF.addBoundaryCondition - Axis must have more than 1 point"));
			break;
		case BoundaryAxis::Y_MAX:
			if (this->mesh.Ny > 1)
				this->YAxisMaxBoundary = Bound;
			else
				throw(std::invalid_argument("EMF.addBoundaryCondition - Axis must have more than 1 point"));
			break;
		case BoundaryAxis::Z_MAX:
			if (this->mesh.Nz > 1)
				this->ZAxisMaxBoundary = Bound;
			else
				throw(std::invalid_argument("EMF.addBoundaryCondition - Axis must have more than 1 point"));
			break;
		default:
			break;
		}
	}
	else {
		throw(std::invalid_argument("EMF.addBoundaryCondition - Addition of boundary conditions is already locked"));
	}
	return;
}

#define ADD_EXTERNALS(EXT)\
void EMF::addExternal ## EXT ## (fieldFunctionTimeDep Fx, fieldFunctionTimeDep Fy, fieldFunctionTimeDep Fz) {\
	if (!this->externalLocked) {\
		std::vector<std::function<af::array(af::array, af::array, af::array, double)>> Functors = {Fx, Fy, Fz};\
		this->m_External ## EXT ## .push_back(Functors);\
	}\
	else {\
		throw(std::invalid_argument("EMF.addExternal ## EXT ## ->  External ## EXT ## addition is locked.\n"));\
	}\
}

ADD_EXTERNALS(Current)
ADD_EXTERNALS(Efield)
ADD_EXTERNALS(Bfield)

#undef ADD_EXTERNALS

af::array EMF::KX(Grid GridPos) {
	af::array k = af::constant(1.0, this->mesh.Nx, this->mesh.Ny, this->mesh.Nz, this->mesh.afType);
	if (this->XAxisMinBoundary.Type() == BoundaryType::UPML) {
		k += this->XAxisMinBoundary.UPMLKMin(this->mesh.x(GridPos), this->mesh.Lx);
	}
	if (this->XAxisMaxBoundary.Type() == BoundaryType::UPML) {
		k += this->XAxisMaxBoundary.UPMLKMax(this->mesh.x(GridPos), this->mesh.Lx);
	}
	return k;
}

af::array EMF::SigmaX(Grid GridPos) {
	af::array sigma = af::constant(0.0, this->mesh.Nx, this->mesh.Ny, this->mesh.Nz, this->mesh.afType);
	if (this->XAxisMinBoundary.Type() == BoundaryType::UPML) {
		sigma += this->XAxisMinBoundary.UPMLSigmaMin(this->mesh.x(GridPos), this->mesh.Lx);
	}
	if (this->XAxisMaxBoundary.Type() == BoundaryType::UPML) {
		sigma += this->XAxisMaxBoundary.UPMLSigmaMax(this->mesh.x(GridPos), this->mesh.Lx);

	}
	return sigma;
}

af::array EMF::KY(Grid GridPos) {
	af::array k = af::constant(1.0, this->mesh.Nx, this->mesh.Ny, this->mesh.Nz, this->mesh.afType);
	if (this->YAxisMinBoundary.Type() == BoundaryType::UPML) {
		k += this->YAxisMinBoundary.UPMLKMin(this->mesh.y(GridPos), this->mesh.Ly);
	}
	if (this->YAxisMaxBoundary.Type() == BoundaryType::UPML) {
		k += this->YAxisMaxBoundary.UPMLKMax(this->mesh.y(GridPos), this->mesh.Ly);
	}
	return k;
}

af::array EMF::SigmaY(Grid GridPos) {
	af::array sigma = af::constant(0.0, this->mesh.Nx, this->mesh.Ny, this->mesh.Nz, this->mesh.afType);
	if (this->YAxisMinBoundary.Type() == BoundaryType::UPML) {
		sigma += this->YAxisMinBoundary.UPMLSigmaMin(this->mesh.y(GridPos), this->mesh.Ly);
	}
	if (this->YAxisMaxBoundary.Type() == BoundaryType::UPML) {
		sigma += this->YAxisMaxBoundary.UPMLSigmaMax(this->mesh.y(GridPos), this->mesh.Ly);

	}
	return sigma;
}

af::array EMF::KZ(Grid GridPos) {
	af::array k = af::constant(1.0, this->mesh.Nx, this->mesh.Ny, this->mesh.Nz, this->mesh.afType);
	if (this->ZAxisMinBoundary.Type() == BoundaryType::UPML) {
		k += this->ZAxisMinBoundary.UPMLKMin(this->mesh.z(GridPos), this->mesh.Lz);
	}
	if (this->ZAxisMaxBoundary.Type() == BoundaryType::UPML) {
		k += this->ZAxisMaxBoundary.UPMLKMax(this->mesh.z(GridPos), this->mesh.Lz);
	}
	return k;
}

af::array EMF::SigmaZ(Grid GridPos) {
	af::array sigma = af::constant(0.0, this->mesh.Nx, this->mesh.Ny, this->mesh.Nz, this->mesh.afType);
	if (this->ZAxisMinBoundary.Type() == BoundaryType::UPML) {
		sigma += this->ZAxisMinBoundary.UPMLSigmaMin(this->mesh.z(GridPos), this->mesh.Lz);
	}
	if (this->ZAxisMaxBoundary.Type() == BoundaryType::UPML) {
		sigma += this->ZAxisMaxBoundary.UPMLSigmaMax(this->mesh.z(GridPos), this->mesh.Lz);

	}
	return sigma;
}

double EMF::gaussLawElectricVerify(Field chargeDensity) {
	af::array aux = (div(this->m_Efield, FieldType::CENTER) - chargeDensity).values;
	
	if (aux.type() == f32)
		return af::mean<float>(af::abs(aux));

	else if (aux.type() == f64)
		return af::mean<double>(af::abs(aux));

	else
		throw std::invalid_argument("double EMF::gaussLawElectricVerify(Field chargeDensity)  ->  Invalid data type found in either the electric field or charge density.\n");
}

double EMF::gaussLawMagneticVerify() {
	if (this->m_Bfield.values.type() == f32)
		return af::mean<float>(af::abs(div(this->m_Bfield, FieldType::CENTER).values));

	else if (this->m_Efield.values.type() == f64)
		return af::mean<double>(af::abs(div(this->m_Bfield, FieldType::CENTER).values));

	else
		throw std::invalid_argument("double EMF::gaussLawMagneticVerify()  ->  Invalid data type found in magnetic field.\n");
}

double EMF::chargeContinuityVerify(Field chargeDensity, Field chargeDensityPrev, Field currentDensity) {
	af::array aux = ((chargeDensity - chargeDensityPrev)/this->mesh.dt + div(currentDensity + this->currentSources(), FieldType::CENTER)).values;

	if (this->m_Bfield.values.type() == f32)
		return af::mean<float>(af::abs(aux));

	else if (this->m_Efield.values.type() == f64)
		return af::mean<double>(af::abs(aux));

	else
		throw std::invalid_argument("double EMF::gaussLawMagneticVerify()  ->  Invalid data type found at the end of the calculation.\n");
}

Field EMF::currentSources(double t) {
	if (!(t == t)) {
		t = (this->internalCounter + 0.5) * this->mesh.dt;
	}
	Field J = zeros(this->mesh, FieldType::FACE, this->mesh.afType);
	for (auto Functors : this->m_ExternalCurrent) {
		if (!(Functors[0] == nullptr))
			J.values(SPAN3, 0) += Functors[0](this->mesh.x(Grid::FACE_X), this->mesh.y(Grid::FACE_X), this->mesh.z(Grid::FACE_X), t);
		if (!(Functors[1] == nullptr))
			J.values(SPAN3, 1) += Functors[1](this->mesh.x(Grid::FACE_Y), this->mesh.y(Grid::FACE_Y), this->mesh.z(Grid::FACE_Y), t);
		if (!(Functors[2] == nullptr))
			J.values(SPAN3, 2) += Functors[2](this->mesh.x(Grid::FACE_Z), this->mesh.y(Grid::FACE_Z), this->mesh.z(Grid::FACE_Z), t);
	}
	return J;
}

Field EMF::Efield(bool numeric) {
	if (numeric) {
		return this->m_Efield;
	}
	else {
		double t = this->internalCounter * this->mesh.dt;
		Field EfieldTot = this->m_Efield;
		for (auto Functors : this->m_ExternalEfield) {
			if (!(Functors[0] == nullptr))
				EfieldTot.values(SPAN3, 0) += Functors[0](this->mesh.x(Grid::FACE_X), this->mesh.y(Grid::FACE_X), this->mesh.z(Grid::FACE_X), t);
			if (!(Functors[1] == nullptr))
				EfieldTot.values(SPAN3, 1) += Functors[1](this->mesh.x(Grid::FACE_Y), this->mesh.y(Grid::FACE_Y), this->mesh.z(Grid::FACE_Y), t);
			if (!(Functors[2] == nullptr))
				EfieldTot.values(SPAN3, 2) += Functors[2](this->mesh.x(Grid::FACE_Z), this->mesh.y(Grid::FACE_Z), this->mesh.z(Grid::FACE_Z), t);
		}
		return EfieldTot;
	}
}

Field EMF::EfieldPrev(bool numeric) {
	if (this->savePrevE) {
		if (numeric) {
			return this->m_EfieldPrev;
		}
		else {
			double t = (this->internalCounter - 1) * this->mesh.dt;
			Field EfieldPrevTot = this->m_EfieldPrev;
			for (auto Functors : this->m_ExternalEfield) {
				if (!(Functors[0] == nullptr))
					EfieldPrevTot.values(SPAN3, 0) += Functors[0](this->mesh.x(Grid::FACE_X), this->mesh.y(Grid::FACE_X), this->mesh.z(Grid::FACE_X), t);
				if (!(Functors[1] == nullptr))
					EfieldPrevTot.values(SPAN3, 1) += Functors[1](this->mesh.x(Grid::FACE_Y), this->mesh.y(Grid::FACE_Y), this->mesh.z(Grid::FACE_Y), t);
				if (!(Functors[2] == nullptr))
					EfieldPrevTot.values(SPAN3, 2) += Functors[2](this->mesh.x(Grid::FACE_Z), this->mesh.y(Grid::FACE_Z), this->mesh.z(Grid::FACE_Z), t);
			}
			return EfieldPrevTot;
		}
	}
	else {
		throw(std::exception("EMF.EfieldPrev - Not saving previous E so the value is not available.\n"));
	}
}

Field EMF::Bfield(bool numeric) {
	if (numeric) {
		return this->m_Bfield;
	}
	else {
		double t = (this->internalCounter + 0.5) * this->mesh.dt;
		Field BfieldTot = this->m_Bfield;
		for (auto Functors : this->m_ExternalBfield) {
			if (!(Functors[0] == nullptr))
				BfieldTot.values(SPAN3, 0) += Functors[0](this->mesh.x(Grid::EDGE_X), this->mesh.y(Grid::EDGE_X), this->mesh.z(Grid::EDGE_X), t);
			if (!(Functors[1] == nullptr))
				BfieldTot.values(SPAN3, 1) += Functors[1](this->mesh.x(Grid::EDGE_Y), this->mesh.y(Grid::EDGE_Y), this->mesh.z(Grid::EDGE_Y), t);
			if (!(Functors[2] == nullptr))
				BfieldTot.values(SPAN3, 2) += Functors[2](this->mesh.x(Grid::EDGE_Z), this->mesh.y(Grid::EDGE_Z), this->mesh.z(Grid::EDGE_Z), t);
		}
		return BfieldTot;
	}
}

Field EMF::BfieldPrev(bool numeric) {
	if (this->savePrevB) {
		if (numeric) {
			return this->m_BfieldPrev;
		}
		else {
			double t = (this->internalCounter - 0.5) * this->mesh.dt;
			Field BfieldPrevTot = this->m_BfieldPrev;
			for (auto Functors : this->m_ExternalBfield) {
				if (!(Functors[0] == nullptr))
					BfieldPrevTot.values(SPAN3, 0) += Functors[0](this->mesh.x(Grid::EDGE_X), this->mesh.y(Grid::EDGE_X), this->mesh.z(Grid::EDGE_X), t);
				if (!(Functors[1] == nullptr))
					BfieldPrevTot.values(SPAN3, 1) += Functors[1](this->mesh.x(Grid::EDGE_Y), this->mesh.y(Grid::EDGE_Y), this->mesh.z(Grid::EDGE_Y), t);
				if (!(Functors[2] == nullptr))
					BfieldPrevTot.values(SPAN3, 2) += Functors[2](this->mesh.x(Grid::EDGE_Z), this->mesh.y(Grid::EDGE_Z), this->mesh.z(Grid::EDGE_Z), t);
			}
			return BfieldPrevTot;
		}
	}
	else {
		throw(std::exception("EMF.BfieldPrev - Not saving previous E so the value is not available.\n"));
	}
}

Field EMF::Dfield(bool numeric) {
	if (numeric) {
		return this->m_Dfield;
	}
	else {
		double t = this->internalCounter * this->mesh.dt;
		Field DfieldTot = this->m_Dfield;
		for (auto Functors : this->m_ExternalEfield) {
			if (!(Functors[0] == nullptr))
				DfieldTot.values(SPAN3, 0) += this->mesh.units.eps0()*Functors[0](this->mesh.x(Grid::FACE_X), this->mesh.y(Grid::FACE_X), this->mesh.z(Grid::FACE_X), t);
			if (!(Functors[1] == nullptr))
				DfieldTot.values(SPAN3, 1) += this->mesh.units.eps0()*Functors[1](this->mesh.x(Grid::FACE_Y), this->mesh.y(Grid::FACE_Y), this->mesh.z(Grid::FACE_Y), t);
			if (!(Functors[2] == nullptr))
				DfieldTot.values(SPAN3, 2) += this->mesh.units.eps0()*Functors[2](this->mesh.x(Grid::FACE_Z), this->mesh.y(Grid::FACE_Z), this->mesh.z(Grid::FACE_Z), t);
		}
		return DfieldTot;
	}
}

Field EMF::DfieldPrev(bool numeric) {
	if (this->savePrevD) {
		if (numeric) {
			return this->m_DfieldPrev;
		}
		else {
			double t = (this->internalCounter - 1) * this->mesh.dt;
			Field DfieldPrevTot = this->m_DfieldPrev;
			for (auto Functors : this->m_ExternalEfield) {
				if (!(Functors[0] == nullptr))
					DfieldPrevTot.values(SPAN3, 0) += this->mesh.units.eps0()*Functors[0](this->mesh.x(Grid::FACE_X), this->mesh.y(Grid::FACE_X), this->mesh.z(Grid::FACE_X), t);
				if (!(Functors[1] == nullptr))
					DfieldPrevTot.values(SPAN3, 1) += this->mesh.units.eps0()*Functors[1](this->mesh.x(Grid::FACE_Y), this->mesh.y(Grid::FACE_Y), this->mesh.z(Grid::FACE_Y), t);
				if (!(Functors[2] == nullptr))
					DfieldPrevTot.values(SPAN3, 2) += this->mesh.units.eps0()*Functors[2](this->mesh.x(Grid::FACE_Z), this->mesh.y(Grid::FACE_Z), this->mesh.z(Grid::FACE_Z), t);
			}
			return DfieldPrevTot;
		}
	}
	else {
		throw(std::exception("EMF.DfieldPrev - Not saving previous E so the value is not available.\n"));
	}
}

Field EMF::Hfield(bool numeric) {
	if (numeric) {
		return this->m_Hfield;
	}
	else {
		double t = (this->internalCounter + 0.5) * this->mesh.dt;
		Field HfieldTot = this->m_Hfield;
		for (auto Functors : this->m_ExternalBfield) {
			if (!(Functors[0] == nullptr))
				HfieldTot.values(SPAN3, 0) += Functors[0](this->mesh.x(Grid::EDGE_X), this->mesh.y(Grid::EDGE_X), this->mesh.z(Grid::EDGE_X), t)/this->mesh.units.mu0();
			if (!(Functors[1] == nullptr))
				HfieldTot.values(SPAN3, 1) += Functors[1](this->mesh.x(Grid::EDGE_Y), this->mesh.y(Grid::EDGE_Y), this->mesh.z(Grid::EDGE_Y), t) / this->mesh.units.mu0();
			if (!(Functors[2] == nullptr))
				HfieldTot.values(SPAN3, 2) += Functors[2](this->mesh.x(Grid::EDGE_Z), this->mesh.y(Grid::EDGE_Z), this->mesh.z(Grid::EDGE_Z), t) / this->mesh.units.mu0();
		}
		return HfieldTot;
	}
}

Field EMF::HfieldPrev(bool numeric) {
	if (this->savePrevH) {
		if (numeric) {
			return this->m_HfieldPrev;
		}
		else {
			double t = (this->internalCounter - 0.5) * this->mesh.dt;
			Field HfieldPrevTot = this->m_HfieldPrev;
			for (auto Functors : this->m_ExternalBfield) {
				if (!(Functors[0] == nullptr))
					HfieldPrevTot.values(SPAN3, 0) += Functors[0](this->mesh.x(Grid::EDGE_X), this->mesh.y(Grid::EDGE_X), this->mesh.z(Grid::EDGE_X), t) / this->mesh.units.mu0();
				if (!(Functors[1] == nullptr))
					HfieldPrevTot.values(SPAN3, 1) += Functors[1](this->mesh.x(Grid::EDGE_Y), this->mesh.y(Grid::EDGE_Y), this->mesh.z(Grid::EDGE_Y), t) / this->mesh.units.mu0();
				if (!(Functors[2] == nullptr))
					HfieldPrevTot.values(SPAN3, 2) += Functors[2](this->mesh.x(Grid::EDGE_Z), this->mesh.y(Grid::EDGE_Z), this->mesh.z(Grid::EDGE_Z), t) / this->mesh.units.mu0();
			}
			return HfieldPrevTot;
		}
	}
	else {
		throw(std::exception("EMF.HfieldPrev - Not saving previous E so the value is not available.\n"));
	}
}