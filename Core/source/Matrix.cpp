#include <Matrix.h>
#include <vector>
#include <TensorField.h>

///////////////////// TOXIC WASTE DO NOT TOUCH - YOU HAVE BEEN WARNED /////////////////////
Matrix::Proxy::Proxy(std::vector<double> column) : column(column) {};
double Matrix::Proxy::operator[] (int index) { return this->column[index]; }
///////////////////////////////////////////////////////////////////////////////////////////

Matrix::Matrix(const std::vector<std::vector<double>> values) {
	if (values.size() > 0 && values[0].size() > 0) {
		this->rows = values.size();
		this->cols = values[0].size();
		for (int i = 0; i < this->rows; i++) {
			std::vector<double> auxRow;
			this->values.push_back(auxRow);
			for (int j = 0; j < this->cols; j++) {
				this->values[i].push_back(values[i][j]);
			}
		}
	}
	else {
		throw std::invalid_argument("Matrix.Matrix - Given sequence is not a matrix");
	}
};

void Matrix::clear() {
	for (int i = 0; i < this->rows; i++)
		for (int j = 0; j < this->cols; j++)
			this->values[i][j] = 0.0;
}

Matrix::Proxy Matrix::operator[] (int index) const {
	return Matrix::Proxy(this->values[index]);
}

void Matrix::operator= (const Matrix &rhs) {
	if (this->rows == rhs.rows && this->cols == rhs.cols)
		this->values = rhs.values;
	else
		throw std::invalid_argument("Matrix.operator= - Dimensions mismatch");
}

Matrix Matrix::operator+ (const Matrix& rhs) const {
	if (this->rows == rhs.rows && this->cols && rhs.cols) {
		Matrix output = Matrix(this->values);
		for (int i = 0; i < this->rows; i++) {
			for (int j = 0; j < this->cols; j++) {
				output.values[i][j] += rhs.values[i][j];
			}
		}
		return output;
	}
	else {
		throw std::invalid_argument("Matrix.operator+ - Incompatible dimensions");
	}
}

Matrix Matrix::operator+ (const double rhs) const {
	Matrix output = Matrix(this->values);
	for (int i = 0; i < output.rows; i++)
		for (int j = 0; j < output.cols; j++)
			output.values[i][j] += rhs;
	return output;
}

Matrix operator+ (const double lhs, const Matrix& rhs) {
	Matrix output = Matrix(rhs.values);
	for (int i = 0; i < output.rows; i++)
		for (int j = 0; j < output.cols; j++)
			output.values[i][j] += lhs;
	return output;
}

Matrix Matrix::operator- (const Matrix& rhs) const {
	if (this->rows == rhs.rows && this->cols && rhs.cols) {
		Matrix output = Matrix(this->values);
		for (int i = 0; i < this->rows; i++) {
			for (int j = 0; j < this->cols; j++) {
				output.values[i][j] -= rhs.values[i][j];
			}
		}
		return output;
	}
	else {
		throw std::invalid_argument("Matrix.operator- - Incompatible dimensions");
	}
}

Matrix Matrix::operator- (const double rhs) const {
	Matrix output = Matrix(this->values);
	for (int i = 0; i < output.rows; i++)
		for (int j = 0; j < output.cols; j++)
			output.values[i][j] -= rhs;
	return output;
}

Matrix operator- (const double lhs, const Matrix& rhs) {
	Matrix output = Matrix(rhs.values);
	for (int i = 0; i < output.rows; i++)
		for (int j = 0; j < output.cols; j++)
			output.values[i][j] -= lhs;
	return output;
}

Matrix Matrix::operator* (const Matrix& rhs) const {
	if (this->rows == rhs.cols) {

		std::vector<std::vector<double>> auxVals;
		for (int i = 0; i < this->rows; i++) {
			std::vector<double> auxRow;
			auxVals.push_back(auxRow);
			for (int j = 0; j < rhs.cols; j++)
				auxVals[i].push_back(0.0);
		}

		Matrix output = Matrix(auxVals);
		for (int i = 0; i < output.rows; i++)
			for (int j = 0; j < output.cols; j++)
				for (int k = 0; k < rhs.rows; k++)
					output.values[i][j] += this->values[i][k] * rhs.values[k][j];
		return output;
	}
	else {
		throw std::invalid_argument("Matrix.operator* - Given sequence is not a matrix");
	}
}

Matrix Matrix::operator* (const double rhs) const {
	Matrix output = Matrix(this->values);
	for (int i = 0; i < output.rows; i++)
		for (int j = 0; j < output.cols; j++)
			output.values[i][j] *= rhs;
	return output;
}

Matrix operator* (const double lhs, const Matrix& rhs) {
	Matrix output = Matrix(rhs.values);
	for (int i = 0; i < output.rows; i++)
		for (int j = 0; j < output.cols; j++)
			output.values[i][j] *= lhs;
	return output;
}

TensorField Matrix::operator* (const TensorField& rhs) const {
	if (this->cols == rhs.rows) {
		TensorField output = TensorField(rhs.type, this->rows, rhs.cols, rhs.mesh);
		for (int i = 0; i < output.rows; i++) 
			for (int j = 0; j < output.cols; j++) {
				for (int k = 0; k < rhs.rows; k++) {
					if (!rhs.values[k][j].isempty()) {
						if (output.values[i][j].isempty())
							output.values[i][j] = this->values[i][k] * rhs.values[k][j];
						else
							output.values[i][j] += this->values[i][k] * rhs.values[k][j];
					}
				}
				output.values[i][j].eval();
			}
		return output;
	}
	else {
		throw std::logic_error("Matrix.operator* - Dimension of matrix incompatible with tensor field");
	}
}

Matrix Matrix::operator/ (const double rhs) const {
	Matrix output = Matrix(this->values);
	for (int i = 0; i < output.rows; i++)
		for (int j = 0; j < output.cols; j++)
			output.values[i][j] /= rhs;
	return output;
}

Matrix operator/ (const double lhs, const Matrix& rhs) {
	Matrix output = Matrix(rhs.values);
	for (int i = 0; i < output.rows; i++)
		for (int j = 0; j < output.cols; j++)
			output.values[i][j] = lhs / rhs.values[i][j];
	return output;
}

void Matrix::operator+= (const Matrix& rhs) {
	if (this->rows == rhs.rows && this->cols && rhs.cols) {
		for (int i = 0; i < this->rows; i++) {
			for (int j = 0; j < this->cols; j++) {
				this->values[i][j] += rhs.values[i][j];
			}
		}
	}
	else {
		throw std::invalid_argument("Matrix.operator+= - Incompatible dimensions");
	}
}

void Matrix::operator+= (const double rhs) {
	for (int i = 0; i <this->rows; i++)
		for (int j = 0; j < this->cols; j++)
			this->values[i][j] += rhs;
}

void Matrix::operator-= (const Matrix& rhs) {
	if (this->rows == rhs.rows && this->cols && rhs.cols) {
		for (int i = 0; i < this->rows; i++) {
			for (int j = 0; j < this->cols; j++) {
				this->values[i][j] -= rhs.values[i][j];
			}
		}
	}
	else {
		throw std::invalid_argument("Matrix.operator+= - Incompatible dimensions");
	}
}

void Matrix::operator-= (const double rhs) {
	for (int i = 0; i <this->rows; i++)
		for (int j = 0; j < this->cols; j++)
			this->values[i][j] -= rhs;
}

void Matrix::operator*= (const double rhs) {
	for (int i = 0; i <this->rows; i++)
		for (int j = 0; j < this->cols; j++)
			this->values[i][j] *= rhs;
}

void Matrix::operator/= (const double rhs) {
	for (int i = 0; i <this->rows; i++)
		for (int j = 0; j < this->cols; j++)
			this->values[i][j] /= rhs;
}