# Thesis Source Code - Maxwell-Bloch equations solver

# Introduction
This repository contains parts of the code of my **Master's Thesis in Engineering Physics**.

**Due to the nature of this software being developed as part of an academic research project, some parts of the code were removed.**

As so, **the project will not build**. However, the software architecture remains unchanged as well as the equation solvers

# Project details

The main purpose of the thesis was to create a **high-performance simulation framework for Light-Matter interactions**.
Due to the computationally demanding nature of this phenomena, **the intensive calculations are performed on the GPU using the ArrayFire library**

The project is architectured as a framework/library that can be re-used or built upon to study many physical phenomena.
The main set of equations that can be studied using the source code on this repository are the **Maxwell-Bloch equations**.
However, many "primitive" mathematical and physical entities are implemented in the core library and can be used to develop other solvers.
These equations describe how **electromagnetic fields** and matter described as a group of **N-level atoms** interact with each other at a quantum level.
The novelty of this code is that using GPUs it is possible to disregard many of the standard models' approximations and better understand phenomena that occur at the transient regime, for example.

The algorithms to study the interaction are present on this repository and are based on a **Yee-cell grid and FDTD methods** to solve the electromagnetic field while the atomic states are studied using **Runge-Kutta methods**.

Another part of this project that had to be removed was a **PIC solver** to study the dynamics of plasmas.
As an example of the modularity of the framework, it was/is possible to combine the solvers and study the Maxwell-Bloch equations alongside the particle dynamics and observe how the phenomena affect each other.
This feature was also part of the novelty that this projected introduced when compared to the state of the art at the time.

# Source code organization
The source code here presented is organized as follows:

*  **Core** folder: Contains the base mathematical and physical classes used to described the system's states. In this folder, there are also the implementations of the solvers of the Maxwell and Bloch equations
*  **MaxwellBloch** folder: Contains a single source field with tests an examples that show how the code can be used to study the Maxwell_bloch dynamics
*  **Python** folder: Contains a set of extensive scripts the create plots using the simulations data-sets


