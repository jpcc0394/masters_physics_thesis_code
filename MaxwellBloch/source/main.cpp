#include <core.h>
#include <fstream>
#include <chrono>

void Bloch2Level() {

	/// Sim props
	int saveInterval = 100;
	int plotInterval = 1;
	bool save = false;
	bool plot = true;
	Units units = Units(UnitSystems::NATURAL_LH);	
	std::string resultsDirectory;

	/// YeeMesh constants
	int Nx = 1000;
	int Ny = 1;
	int Nz = 1;
	double dx = 1.0e-8;
	double dy = 1.0e-8;
	double dz = 1.0e-8;
	double dt = (dx / Units::si_c()) / 2.0;
	double Tmax = 3.0e-15;

	/// Eletro-magnetic field constans
	PropagationDir direction = PropagationDir::X_P;
	EFieldPol EPol = EFieldPol::Z;
	double R0 = Nx*dx / 2.0;
	double waveNumber = 2.0 * PI /(50.0*dx);
	double phase = 0.0;
	double FWHM = Nx*dx / 10.0;
	double intensity = 1.0e10;
	std::vector<bool> fieldSaveSelector = { false, true, false, false };

	/// Bloch object initialization
	int levels = 2;
	double mu12 = 1.0e-29;
	double eta = (intensity / mu12) / 1.0e12;
	double omega12 = Units::si_c() *  waveNumber;
	double gamma12 = (intensity*mu12/Units::si_hbar())/100.0;
	double alpha = 0.0;
	double beta = 1.0 / (intensity*dt) / 100.0;;
	DipoleDir initialPolDir = DipoleDir::Z;
	bool useRK4 = true;
	bool useInterpolation = true;

	std::vector<std::vector<double>> mu = {
		{ 0.0, -mu12, mu12, 0.0 },
		{ -mu12, 0.0, 0.0, mu12 },
		{ mu12, 0.0, 0.0, -mu12 },
		{ 0.0, mu12, -mu12, 0.0 }
	};

	std::vector<std::vector<double>> omega = {
		{ 0.0, 0.0, 0.0, 0.0 },
		{ 0.0, omega12, 0.0, 0.0 },
		{ 0.0, 0.0, -omega12, 0.0 },
		{ 0.0, 0.0, 0.0, 0.0 },
	};

	std::vector<std::vector<double>> gamma = {
		{ 0.0, 0.0, 0.0, gamma12 },
		{ 0.0, -gamma12 / 2.0, 0.0, 0.0 },
		{ 0.0, 0.0, -gamma12 / 2.0, 0.0 },
		{ 0.0, 0.0, 0.0, -gamma12 },
	};

	std::vector<std::vector<double>> initValues = { 
		{ 1.0, 0.0 },
		{ 0.0, 0.0 },
		{ 0.0, 0.0 },
		{ 0.0, 0.0 }, 
	};

	std::vector<double> polTerms = { 0.0, mu12, mu12, 0.0 };
	std::vector<double> dipoleForceTerms = { 0.0, mu12, mu12, 0.0 };

	/// Chose which values to save
	std::vector<bool> saveBlochReal = { true, true, true, true };
	std::vector<bool> saveBlochImag = { false, false, true, true };
	bool savePol = true;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::cout << "Converting constants to natural units" << std::endl;

	std::vector<UnitsNames> lengthUnit = { UnitsNames::LENGTH };
	std::vector<int> lengthPower = { 1 };
	std::vector<int> densityPower = { -3 };

	std::vector<UnitsNames> timeUnit = { UnitsNames::TIME };
	std::vector<int> timePower = { 1 };
	std::vector<int> frequencyPower = { -1 };

	std::vector<UnitsNames> intensityUnit = { UnitsNames::MASS , UnitsNames::LENGTH , UnitsNames::TIME , UnitsNames::CHARGE };
	std::vector<int> intensityPower = { 1, 1, -2, -1 };

	std::vector<UnitsNames> dipoleUnit = { UnitsNames::CHARGE , UnitsNames::LENGTH };
	std::vector<int> dipolePower = { 1, 1 };

	std::vector<UnitsNames> dipoleVersorUnit = { UnitsNames::MASS, UnitsNames::LENGTH, UnitsNames::TIME, UnitsNames::CHARGE };
	std::vector<int> dipoleVersorPower = { -1, -1, 1, 1 };

	/// Mesh constants
	dx = units.convertToNatural(dx, lengthUnit, lengthPower);
	dy = units.convertToNatural(dy, lengthUnit, lengthPower);
	dz = units.convertToNatural(dz, lengthUnit, lengthPower);
	dt = units.convertToNatural(dt, timeUnit, timePower);
	Tmax = units.convertToNatural(Tmax, timeUnit, timePower);

	/// EMF constants
	R0 = units.convertToNatural(R0, lengthUnit, lengthPower);
	waveNumber = units.convertToNatural(waveNumber, lengthUnit, frequencyPower);
	FWHM = units.convertToNatural(FWHM, lengthUnit, lengthPower);
	intensity = units.convertToNatural(intensity, intensityUnit, intensityPower);

	/// Density operator constants
	for (int i = 0; i < mu.size(); i++) {
		polTerms[i] = units.convertToNatural(polTerms[i], dipoleUnit, dipolePower);
		for (int j = 0; j < mu[0].size(); j++) {
			mu[i][j] = units.convertToNatural(mu[i][j], dipoleUnit, dipolePower);
			omega[i][j] = units.convertToNatural(omega[i][j], timeUnit, frequencyPower);
			gamma[i][j] = units.convertToNatural(gamma[i][j], timeUnit, frequencyPower);
		}
	}

	eta = units.convertToNatural(eta, lengthUnit, densityPower);
	alpha = units.convertToNatural(alpha, dipoleVersorUnit, dipoleVersorPower);
	beta = units.convertToNatural(beta, dipoleVersorUnit, dipoleVersorPower);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::cout << "Starting simulation\n";

	/// Mesh initialization
	YeeMesh M = YeeMesh(units, Nx, Ny, Nz, dx, dy, dz, dt);

	/// Field initialization
	EMF EMField = EMF(M, true, false);
	EMField.addGaussianPulsePlaneWave(direction, EPol, R0, waveNumber, phase, FWHM, intensity);
	EMField.initialize();

	/// Densop initialization
	Bloch densOp = Bloch(levels, eta, alpha, beta, M, mu, gamma, omega);
	densOp.Initialize(initValues, initialPolDir);

	/// Aux fields
	Field JField = zeros(EMField.mesh, FieldType::FACE, M.dtype());
	Field MField = zeros(EMField.mesh, FieldType::EDGE, M.dtype());

	/// start with 0 for correct time counting
	int i = 0;

	if (save && resultsDirectory.empty()) {
		/// Create dir
		try {
			resultsDirectory = generateDirectoryPath("Maxwell-Bloch");
			MKDirectory(resultsDirectory);
		}
		catch (std::invalid_argument e) {
			std::cout << e.what() << std::endl;
			system("pause");
			return;
		}
	}

	///Initial saves
	if (save) {
		M.save(resultsDirectory, true);
		densOp.saveProperties(resultsDirectory, true);
	}

	/// Windows for plots
	af::Window EWnd("E");
	af::Window Rho11RealWnd("Rho11R");

	while (true) {
		//auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - start);
		//std::cout << (float(i)) / elapsed.count() << '\r' << std::flush;
		std::cout << '\r' << i*dt/Tmax * 100 << std::flush;

		if (i % saveInterval == 0 && save) {
			densOp.saveToAF(resultsDirectory, saveBlochReal, saveBlochImag);
			EMField.saveToAF(resultsDirectory, fieldSaveSelector, true);
		}

		if (i % plotInterval == 0 && plot) {
			EWnd.plot(af::range(M.Nx), EMField.Efield().values(SPAN3, 2).as(f32));
			Rho11RealWnd.plot(af::range(M.Nx), (af::real(densOp.Rho[0][0]).as(f32))(af::span, 0, 0, 0));
		}

		EMField.push(JField, densOp.calculatePolarization(polTerms), MField);
		densOp.push(EMField.EfieldPrev(), EMField.Efield(), useRK4, useInterpolation);

		i++;
	}

	std::cout << std::endl;
}

void speedUpMetrics() {
	std::vector<int> NxArray = { 1000, 1000, 5000, 10000, 50000, 100000, 500000, 1000000};
	Units units = Units(UnitSystems::NATURAL_LH);

	for (auto Nx : NxArray) {
		/// YeeMesh constants
		int Ny = 1;
		int Nz = 1;
		double dx = 1.0e-9;
		double dy = 1.0e-9;
		double dz = 1.0e-9;
		double dt = dx / Units::si_c() / 2.0;

		/// Eletro-magnetic field constans
		PropagationDir FieldDirection = PropagationDir::X_P;
		EFieldPol ElectricFieldPolarization = EFieldPol::Z;
		double r0 = Nx*dx / 2.0;
		double waveNumber = 2.0 * PI / (5.0e-7);;
		double phase = 0.0;
		double FWHM = Nx*dx / 15.0;
		double intensity = 1.0e10;

		/// Bloch object initialization
		int levels = 2;
		double mu12 = 1.0e-29;
		double eta = (intensity / mu12) / 1.0e12;
		double omega12 = Units::si_c() *  waveNumber;
		double gamma12 = (intensity*mu12 / Units::si_hbar()) / 100.0;
		double alpha = 0.0;
		double beta = 1.0 / (intensity*dt) / 100.0;;
		DipoleDir initialPolDir = DipoleDir::Z;
		bool useRK4 = true;
		bool useInterpolation = true;

		std::vector<std::vector<double>> mu = {
			{ 0.0, -mu12, mu12, 0.0 },
			{ -mu12, 0.0, 0.0, mu12 },
			{ mu12, 0.0, 0.0, -mu12 },
			{ 0.0, mu12, -mu12, 0.0 }
		};

		std::vector<std::vector<double>> omega = {
			{ 0.0, 0.0, 0.0, 0.0 },
			{ 0.0, omega12, 0.0, 0.0 },
			{ 0.0, 0.0, -omega12, 0.0 },
			{ 0.0, 0.0, 0.0, 0.0 },
		};

		std::vector<std::vector<double>> gamma = {
			{ 0.0, 0.0, 0.0, gamma12 },
			{ 0.0, -gamma12 / 2.0, 0.0, 0.0 },
			{ 0.0, 0.0, -gamma12 / 2.0, 0.0 },
			{ 0.0, 0.0, 0.0, -gamma12 },
		};

		std::vector<std::vector<double>> initValues = {
			{ 1.0, 0.0 },
			{ 0.0, 0.0 },
			{ 0.0, 0.0 },
			{ 0.0, 0.0 },
		};

		std::vector<double> polTerms = { 0.0, mu12, mu12, 0.0 };

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//std::cout << "Converting constants to natural units" << std::endl;

		std::vector<UnitsNames> lengthUnit = { UnitsNames::LENGTH };
		std::vector<int> lengthPower = { 1 };
		std::vector<int> densityPower = { -3 };

		std::vector<UnitsNames> timeUnit = { UnitsNames::TIME };
		std::vector<int> timePower = { 1 };
		std::vector<int> frequencyPower = { -1 };

		std::vector<UnitsNames> intensityUnit = { UnitsNames::MASS , UnitsNames::LENGTH , UnitsNames::TIME , UnitsNames::CHARGE };
		std::vector<int> intensityPower = { 1, 1, -2, -1 };

		std::vector<UnitsNames> dipoleUnit = { UnitsNames::CHARGE , UnitsNames::LENGTH };
		std::vector<int> dipolePower = { 1, 1 };

		std::vector<UnitsNames> dipoleVersorUnit = { UnitsNames::MASS, UnitsNames::LENGTH, UnitsNames::TIME, UnitsNames::CHARGE };
		std::vector<int> dipoleVersorPower = { -1, -1, 1, 1 };

		/// Mesh constants
		dx = units.convertToNatural(dx, lengthUnit, lengthPower);
		dy = units.convertToNatural(dy, lengthUnit, lengthPower);
		dz = units.convertToNatural(dz, lengthUnit, lengthPower);
		dt = units.convertToNatural(dt, timeUnit, timePower);

		/// EMF constants
		r0 = units.convertToNatural(r0, lengthUnit, lengthPower);
		waveNumber = units.convertToNatural(waveNumber, timeUnit, frequencyPower);
		FWHM = units.convertToNatural(FWHM, lengthUnit, lengthPower);
		intensity = units.convertToNatural(intensity, intensityUnit, intensityPower);

		/// Density operator constants
		for (int i = 0; i < mu.size(); i++) {
			polTerms[i] = units.convertToNatural(polTerms[i], dipoleUnit, dipolePower);
			for (int j = 0; j < mu[0].size(); j++) {
				mu[i][j] = units.convertToNatural(mu[i][j], dipoleUnit, dipolePower);
				omega[i][j] = units.convertToNatural(omega[i][j], timeUnit, frequencyPower);
				gamma[i][j] = units.convertToNatural(gamma[i][j], timeUnit, frequencyPower);
			}
		}

		eta = units.convertToNatural(eta, lengthUnit, densityPower);
		alpha = units.convertToNatural(alpha, dipoleVersorUnit, dipoleVersorPower);
		beta = units.convertToNatural(beta, dipoleVersorUnit, dipoleVersorPower);

		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		//std::cout << "Starting simulation\n";

		/// Mesh initialization
		YeeMesh M = YeeMesh(Nx, Ny, Nz, dx, dy, dz, dt);

		/// Field initialization
		EMF EMField = EMF(M, true);
		EMField.addGaussianPulsePlaneWave(FieldDirection, ElectricFieldPolarization, r0, waveNumber, phase, FWHM, intensity);
		EMField.initialize();

		/// Densop initialization
		Bloch densOp = Bloch(levels, eta, alpha, beta, M, mu, gamma, omega);
		densOp.Initialize(initValues, initialPolDir);

		/// Aux fields
		Field JField = zeros(EMField.mesh, FieldType::FACE, f64);
		Field MField = zeros(EMField.mesh, FieldType::EDGE, f64);

		/// start with 0 for correct time counting
		int i = 0;
		auto start = std::chrono::high_resolution_clock::now();
		while (i < 50) {
			EMField.push(JField, densOp.calculatePolarization(polTerms), MField);
			densOp.push(EMField.EfieldPrev(), EMField.Efield(), false, true);
			i++;
		}
		auto end = std::chrono::high_resolution_clock::now();
		auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
		std::cout << Nx << "\t" << elapsed.count() << std::endl;
	}

	return;
}

void testABC() {

	Units units = Units(UnitSystems::SI);

	/// YeeMesh constants
	int Nx = 1000;
	int Ny = 1;
	int Nz = 1;
	double dx = 1.0e-7;
	double dy = dx;
	double dz = dx;
	double dt = (dx / Units::si_c()) / 10.0;

	YeeMesh M = YeeMesh(units, Nx, Ny, Nz, dx, dy, dz, dt);

	/// EField constants
	PropagationDir direction1 = PropagationDir::X_P;
	EFieldPol EPol1 = EFieldPol::Z;
	double r01 = Nx*dx / 2.0;
	double fieldFrequency1 = 1.0 / (5000.0e-9);
	double phase1 = 0.0;
	double FWHM1 = Nx*dx / 15.0;
	double intensity1 = 1.0;

	/// ABC Boundary constants
	double LMin = 0.1*M.Lx;
	double LMax = 0.1*M.Lx;
	double C = 0.1;
	int Pow = 2;

	EMF EMField = EMF(M, false, false);
	EMField.addGaussianPulsePlaneWave(direction1, EPol1, r01, fieldFrequency1, phase1, FWHM1, intensity1);
	EMField.addBoundaryCondition(makeABCBoundary(BoundaryAxis::X_MIN, LMin, C, Pow));
	EMField.addBoundaryCondition(makeABCBoundary(BoundaryAxis::X_MAX, LMin, C, Pow));
	EMField.initialize();

	af::Window EWnd("E");
	while (1) {
		EWnd.plot(af::range(M.Nx), EMField.Efield().values(SPAN3, 2).as(f32));
		EMField.push();
	}
}

void testUPML1D() {
	/// YeeMesh constants
	int Nx = 1000;
	int Ny = 1;
	int Nz = 1;
	double dx = 0.001;
	double dy = 0.001;
	double dz = 0.001;
	double dt = 0.0002;

	/// EField constants
	PropagationDir direction = PropagationDir::X_P;
	EFieldPol EPol = EFieldPol::Z;
	double r0 = Nx*dx / 2.0;
	double waveNumber = 10.0;
	double phase = 0.0;
	double FWHM = Nx*dx / 15.0;
	double intensity = 1.0;

	/// Unit conversion for UPML
	Units units = Units(UnitSystems::NATURAL_LH);
	std::vector<UnitsNames> names = { UnitsNames::MASS, UnitsNames::LENGTH, UnitsNames::TIME, UnitsNames::CHARGE };
	std::vector<int> powers = { 1, 2, -1, -2 };
	double eta0 = units.convertToNatural(377, names, powers);
	std::cout << eta0 << std::endl;

	/// UPML Boundary constants
	double L = 0.1;
	int Pow = 4;
	double K = 1.0;
	double Sigma = 2.0*0.8*(Pow + 1) / (eta0 * dx);
	//double Sigma = 0.0;
	std::cout << Sigma << std::endl;

	YeeMesh M = YeeMesh(units, Nx, Ny, Nz, dx, dy, dz, dt);
	EMF EMField = EMF(M, true, true, true, true);
	EMField.addGaussianPulsePlaneWave(direction, EPol, r0, waveNumber, phase, FWHM, intensity);
	EMField.addBoundaryCondition(makeUPMLBoundary(BoundaryAxis::X_MIN, L, Sigma, K, Pow));
	EMField.addBoundaryCondition(makeUPMLBoundary(BoundaryAxis::X_MAX, L, Sigma, K, Pow));

	EMField.initialize();

	af::Window EWnd("E");
	while (1) {
		EMField.push();
		EWnd.plot(M.dx * af::range(M.Nx), EMField.Efield().values(SPAN3, 2).as(f32));
		//af::array abs = af::abs(EMField.Efield().values(SPAN3, 2).as(f32));
		//std::cout << std::log10(af::max<double>(abs) + 1.0e-13) << '\n';
		//EWnd.plot(M.dx * af::range(M.Nx), af::log10(abs + 1e-4*af::max<float>(abs)));
	}
}

void testSimpleField() {
	/// YeeMesh constants
	int Nx = 1000;
	int Ny = 1;
	int Nz = 1;
	double dx = 1.0e-7;
	double dy = dx;
	double dz = dx;
	double dt = (dx/Units::si_c()) / 10.0;

	/// EField constants
	PropagationDir direction1 = PropagationDir::X_P;
	EFieldPol EPol1 = EFieldPol::Z;
	double r01 = Nx*dx / 2.0;
	double fieldFrequency1 = 2.0 * PI /(5000.0e-9);
	double phase1 = 0.0;
	double FWHM1 = Nx*dx / 15.0;
	double intensity1 = 1.0;

	Units units = Units(UnitSystems::SI);
	YeeMesh M = YeeMesh(units, Nx, Ny, Nz, dx, dy, dz, dt);
	EMF EMField = EMF(M, false, false);
	EMField.addGaussianPulsePlaneWave(direction1, EPol1, r01, fieldFrequency1, phase1, FWHM1, intensity1);
	EMField.initialize();

	af::Window EWnd("E");
	while (1) {
		EWnd.plot(af::range(M.Nx), EMField.Efield().values(SPAN3, 2).as(f32));
		EMField.push();
	}
}

void testFieldSource1D() {

	/// YeeMesh constants
	int Nx = 1000;
	int Ny = 1;
	int Nz = 1;
	double dx = 0.001;
	double dy = 0.001;
	double dz = 0.001;
	double dt = 0.0005;

	/// Current field source constants
	double f = 100.0;
	double X0 = Nx*dx / 8.0;
	double sigma = 0.01;
	double alpha = 1.0;

	/// Current functions
	auto JzSource = [&](af::array X, af::array Y, af::array Z, double t) {
		af::array J = af::exp(-af::pow(X - X0, 2.0) / (sigma*sigma))*std::sin(f*t);
		return J;
	};

	/// ABC Boundary constants
	double LX = 0.1;
	double C = 0.1;
	int Pow = 3;

	Units units = Units(UnitSystems::NATURAL_LH);
	YeeMesh M = YeeMesh(units, Nx, Ny, Nz, dx, dy, dz, dt);
	EMF EMField = EMF(M, false, false);

	/// Add boundaries, current source and initialize
	EMField.addBoundaryCondition(makeABCBoundary(BoundaryAxis::X_MAX, LX, C, Pow));
	EMField.addBoundaryCondition(makeABCBoundary(BoundaryAxis::X_MIN, LX, C, Pow));
	EMField.addExternalCurrent(nullptr, nullptr, JzSource);
	EMField.initialize();

	af::Window EzWnd("Ez");
	int i = 0;
	while (1) {
		EMField.push();

		if (i % 1 == 0) {
			EzWnd.plot(M.dx * af::range(M.Nx), EMField.Efield().values(SPAN3, 2).as(f32));
		}

		i++;
	}
}

void testExternalFields() {

	/// YeeMesh constants
	int Nx = 1000;
	int Ny = 1;
	int Nz = 1;
	double dx = 0.001;
	double dy = 0.001;
	double dz = 0.001;
	double dt = 0.0005;

	/// EField constants
	PropagationDir direction = PropagationDir::X_P;
	EFieldPol EPol = EFieldPol::Z;
	double r0 = Nx*dx / 4.0;
	double waveNumber = 2.0 * PI / (0.1);
	double phase = 0.0;
	double FWHM = Nx*dx / 40.0;
	double intensity = 1.0;

	/// Electric field functions
	auto ExternalEz = [&](af::array X, af::array Y, af::array Z, double t) {
		af::array E = af::sin(2*PI*X);;
		return E;
	};

	Units units = Units(UnitSystems::NATURAL_LH);
	YeeMesh Mesh = YeeMesh(units, Nx, Ny, Nz, dx, dy, dz, dt);
	EMF EMField = EMF(Mesh);

	/// Add external field sources and initialize
	EMField.addGaussianPulsePlaneWave(direction, EPol, r0, waveNumber, phase, FWHM, intensity);
	EMField.addExternalEfield(nullptr, nullptr, ExternalEz);
	EMField.initialize();

	af::Window EzWnd("Ez");
	int i = 0;
	while (1) {
		EMField.push();
		if (i % 1 == 0) {
			EzWnd.plot(Mesh.dx * af::range(Mesh.Nx), EMField.Efield().values(SPAN3, 2).as(f32));
		}
		i++;
	}
}

void testTrace() {
	/// Sim props
	int saveInterval = 100;
	int plotInterval = 1;
	bool save = true;
	bool plot = false;
	Units units = Units(UnitSystems::NATURAL_LH);
	std::string resultsDirectory;

	/// YeeMesh constants
	int Nx = 1000;
	int Ny = 1;
	int Nz = 1;
	double dx = 1.0e-9;
	double dy = 1.0e-9;
	double dz = 1.0e-9;
	double dt = (dx / Units::si_c()) / 2.0;
	double Tmax = 5.0e-15;

	/// Eletro-magnetic field constans
	PropagationDir direction = PropagationDir::X_P;
	EFieldPol EPol = EFieldPol::Z;
	double R0 = Nx*dx / 4.0;
	double waveNumber = 2.0 * PI /(50.0*dx);
	double phase = 0.0;
	double FWHM = Nx*dx / 15.0;
	double intensity = 1.0e10;

	/// Bloch object initialization
	int levels = 2;
	double mu12 = 1.0e-29;
	double eta = (intensity / mu12) / 1.0e12;
	double omega12 = Units::si_c() *  waveNumber;
	double gamma12 = (intensity*mu12 / Units::si_hbar()) / 100.0;
	double alpha = 0.0;
	double beta = 1.0 / (intensity*dt) / 100.0;;
	DipoleDir initialPolDir = DipoleDir::Z;
	bool useRK4 = true;
	bool useInterpolation = true;

	std::vector<std::vector<double>> mu = {
		{ 0.0, -mu12, mu12, 0.0 },
		{ -mu12, 0.0, 0.0, mu12 },
		{ mu12, 0.0, 0.0, -mu12 },
		{ 0.0, mu12, -mu12, 0.0 }
	};

	std::vector<std::vector<double>> omega = {
		{ 0.0, 0.0, 0.0, 0.0 },
		{ 0.0, omega12, 0.0, 0.0 },
		{ 0.0, 0.0, -omega12, 0.0 },
		{ 0.0, 0.0, 0.0, 0.0 },
	};

	std::vector<std::vector<double>> gamma = {
		{ 0.0, 0.0, 0.0, gamma12 },
		{ 0.0, -gamma12 / 2.0, 0.0, 0.0 },
		{ 0.0, 0.0, -gamma12 / 2.0, 0.0 },
		{ 0.0, 0.0, 0.0, -gamma12 },
	};

	std::vector<std::vector<double>> initValues = { 
		{ 1.0, 0.0 },
		{ 0.0, 0.0 },
		{ 0.0, 0.0 },
		{ 0.0, 0.0 }, 
	};

	std::vector<double> polTerms = { 0.0, mu12, mu12, 0.0 };
	std::vector<double> dipoleForceTerms = { 0.0, mu12, mu12, 0.0 };

	/// Chose which values to save
	std::vector<bool> saveBlochReal = { true, false, false, true };
	std::vector<bool> saveBlochImag = { false, false, false, false };

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::cout << "Converting constants to natural units" << std::endl;

	std::vector<UnitsNames> lengthUnit = { UnitsNames::LENGTH };
	std::vector<int> lengthPower = { 1 };
	std::vector<int> densityPower = { -3 };

	std::vector<UnitsNames> timeUnit = { UnitsNames::TIME };
	std::vector<int> timePower = { 1 };
	std::vector<int> frequencyPower = { -1 };

	std::vector<UnitsNames> intensityUnit = { UnitsNames::MASS , UnitsNames::LENGTH , UnitsNames::TIME , UnitsNames::CHARGE };
	std::vector<int> intensityPower = { 1, 1, -2, -1 };

	std::vector<UnitsNames> dipoleUnit = { UnitsNames::CHARGE , UnitsNames::LENGTH };
	std::vector<int> dipolePower = { 1, 1 };

	std::vector<UnitsNames> dipoleVersorUnit = { UnitsNames::MASS, UnitsNames::LENGTH, UnitsNames::TIME, UnitsNames::CHARGE };
	std::vector<int> dipoleVersorPower = { -1, -1, 1, 1 };

	/// Mesh constants
	dx = units.convertToNatural(dx, lengthUnit, lengthPower);
	dy = units.convertToNatural(dy, lengthUnit, lengthPower);
	dz = units.convertToNatural(dz, lengthUnit, lengthPower);
	dt = units.convertToNatural(dt, timeUnit, timePower);
	Tmax = units.convertToNatural(Tmax, timeUnit, timePower);

	/// EMF constants
	R0 = units.convertToNatural(R0, lengthUnit, lengthPower);
	waveNumber = units.convertToNatural(waveNumber, lengthUnit, frequencyPower);
	FWHM = units.convertToNatural(FWHM, lengthUnit, lengthPower);
	intensity = units.convertToNatural(intensity, intensityUnit, intensityPower);

	/// Density operator constants
	for (int i = 0; i < mu.size(); i++) {
		polTerms[i] = units.convertToNatural(polTerms[i], dipoleUnit, dipolePower);
		for (int j = 0; j < mu[0].size(); j++) {
			mu[i][j] = units.convertToNatural(mu[i][j], dipoleUnit, dipolePower);
			omega[i][j] = units.convertToNatural(omega[i][j], timeUnit, frequencyPower);
			gamma[i][j] = units.convertToNatural(gamma[i][j], timeUnit, frequencyPower);
		}
	}

	eta = units.convertToNatural(eta, lengthUnit, densityPower);
	alpha = units.convertToNatural(alpha, dipoleVersorUnit, dipoleVersorPower);
	beta = units.convertToNatural(beta, dipoleVersorUnit, dipoleVersorPower);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::cout << "Starting Test trace simulation\n";

	/// Mesh initialization
	YeeMesh M = YeeMesh(units, Nx, Ny, Nz, dx, dy, dz, dt);

	/// Field initialization
	EMF EMField = EMF(M, true, false);
	EMField.addGaussianPulsePlaneWave(direction, EPol, R0, waveNumber, phase, FWHM, intensity);
	EMField.initialize();

	/// Densop initialization
	Bloch densOp = Bloch(levels, eta, alpha, beta, M, mu, gamma, omega);
	densOp.Initialize(initValues, initialPolDir);

	/// Aux fields
	Field JField = zeros(EMField.mesh, FieldType::FACE, M.dtype());
	Field MField = zeros(EMField.mesh, FieldType::EDGE, M.dtype());

	/// start with 0 for correct time counting
	int i = 0;

	if (save && resultsDirectory.empty()) {
		/// Create dir
		try {
			if (useRK4) {
				resultsDirectory = generateDirectoryPath("Maxwell-Bloch-Trace-RK4");
			}
			else {
				resultsDirectory = generateDirectoryPath("Maxwell-Bloch-Trace-RK2");
			}
			MKDirectory(resultsDirectory);
		}
		catch (std::invalid_argument e) {
			std::cout << e.what() << std::endl;
			system("pause");
			return;
		}
	}

	///Initial saves
	if (save) {
		M.save(resultsDirectory, true);
		densOp.saveProperties(resultsDirectory, true);
	}

	/// Windows for plots
	//af::Window TraceWnd("Trace");
	while (i*dt < Tmax) {
		std::cout << '\r' << i*dt/Tmax * 100 << std::flush;

		if (i % saveInterval == 0 && save) {
			densOp.saveToAF(resultsDirectory, saveBlochReal, saveBlochImag, f64);
		}

		if (i % plotInterval == 0 && plot) {
			//TraceWnd.plot(af::range(M.Nx), (af::real(densOp.Rho[0][0]) + af::real(densOp.Rho[3][0])).as(f32)(SPAN));
		}

		EMField.push(JField, densOp.calculatePolarization(polTerms), MField);
		densOp.push(EMField.EfieldPrev(), EMField.Efield(), useRK4, useInterpolation);

		i++;
	}

	std::cout << std::endl;
}

void testDipoleVersorNorm() {
	/// Sim props
	int plotInterval = 1;
	bool plot = true;
	Units units = Units(UnitSystems::NATURAL_LH);

	/// YeeMesh constants
	int Nx = 1000;
	int Ny = 1;
	int Nz = 1;
	double dx = 1.0e-9;
	double dy = 1.0e-9;
	double dz = 1.0e-9;
	double dt = (dx / Units::si_c()) / 2.0;

	/// Eletro-magnetic field constans
	PropagationDir direction = PropagationDir::X_P;
	EFieldPol EPol = EFieldPol::Z;
	double R0 = Nx*dx / 2.0;
	double waveNumber = 2.0 * PI / (50.0*dx);
	double phase = 0.0;
	double FWHM = Nx*dx / 15.0;
	double intensity = 1.0e12;

	/// Bloch object initialization
	int levels = 2;
	double mu12 = 1.0e-29;
	double eta = (intensity / mu12) / 1.0e12;
	double omega12 = Units::si_c() *  waveNumber;
	double gamma12 = (intensity*mu12 / Units::si_hbar()) / 100.0;
	double alpha = 0.0;
	double beta = 1.0 / (intensity*dt) / 100.0;;
	DipoleDir initialPolDir = DipoleDir::Y;

	std::vector<std::vector<double>> mu = {
		{ 0.0, -mu12, mu12, 0.0 },
		{ -mu12, 0.0, 0.0, mu12 },
		{ mu12, 0.0, 0.0, -mu12 },
		{ 0.0, mu12, -mu12, 0.0 }
	};

	std::vector<std::vector<double>> omega = {
		{ 0.0, 0.0, 0.0, 0.0 },
		{ 0.0, omega12, 0.0, 0.0 },
		{ 0.0, 0.0, -omega12, 0.0 },
		{ 0.0, 0.0, 0.0, 0.0 },
	};

	std::vector<std::vector<double>> gamma = {
		{ 0.0, 0.0, 0.0, gamma12 },
		{ 0.0, -gamma12 / 2.0, 0.0, 0.0 },
		{ 0.0, 0.0, -gamma12 / 2.0, 0.0 },
		{ 0.0, 0.0, 0.0, -gamma12 },
	};

	std::vector<std::vector<double>> initValues = {
		{ 1.0, 0.0 },
		{ 0.0, 0.0 },
		{ 0.0, 0.0 },
		{ 0.0, 0.0 },
	};

	std::vector<double> polTerms = { 0.0, mu12, mu12, 0.0 };
	std::vector<double> dipoleForceTerms = { 0.0, mu12, mu12, 0.0 };

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::cout << "Converting constants to natural units" << std::endl;

	std::vector<UnitsNames> lengthUnit = { UnitsNames::LENGTH };
	std::vector<int> lengthPower = { 1 };
	std::vector<int> densityPower = { -3 };

	std::vector<UnitsNames> timeUnit = { UnitsNames::TIME };
	std::vector<int> timePower = { 1 };
	std::vector<int> frequencyPower = { -1 };

	std::vector<UnitsNames> intensityUnit = { UnitsNames::MASS , UnitsNames::LENGTH , UnitsNames::TIME , UnitsNames::CHARGE };
	std::vector<int> intensityPower = { 1, 1, -2, -1 };

	std::vector<UnitsNames> dipoleUnit = { UnitsNames::CHARGE , UnitsNames::LENGTH };
	std::vector<int> dipolePower = { 1, 1 };

	std::vector<UnitsNames> dipoleVersorUnit = { UnitsNames::MASS, UnitsNames::LENGTH, UnitsNames::TIME, UnitsNames::CHARGE };
	std::vector<int> dipoleVersorPower = { -1, -1, 1, 1 };

	/// Mesh constants
	dx = units.convertToNatural(dx, lengthUnit, lengthPower);
	dy = units.convertToNatural(dy, lengthUnit, lengthPower);
	dz = units.convertToNatural(dz, lengthUnit, lengthPower);
	dt = units.convertToNatural(dt, timeUnit, timePower);

	/// EMF constants
	R0 = units.convertToNatural(R0, lengthUnit, lengthPower);
	waveNumber = units.convertToNatural(waveNumber, lengthUnit, frequencyPower);
	FWHM = units.convertToNatural(FWHM, lengthUnit, lengthPower);
	intensity = units.convertToNatural(intensity, intensityUnit, intensityPower);

	/// Density operator constants
	for (int i = 0; i < mu.size(); i++) {
		polTerms[i] = units.convertToNatural(polTerms[i], dipoleUnit, dipolePower);
		for (int j = 0; j < mu[0].size(); j++) {
			mu[i][j] = units.convertToNatural(mu[i][j], dipoleUnit, dipolePower);
			omega[i][j] = units.convertToNatural(omega[i][j], timeUnit, frequencyPower);
			gamma[i][j] = units.convertToNatural(gamma[i][j], timeUnit, frequencyPower);
		}
	}

	eta = units.convertToNatural(eta, lengthUnit, densityPower);
	alpha = units.convertToNatural(alpha, dipoleVersorUnit, dipoleVersorPower);
	beta = units.convertToNatural(beta, dipoleVersorUnit, dipoleVersorPower);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::cout << "Starting simulation\n";

	/// Mesh initialization
	YeeMesh M = YeeMesh(units, Nx, Ny, Nz, dx, dy, dz, dt);

	/// Field initialization
	EMF EMField = EMF(M, true, false);
	EMField.addGaussianPulsePlaneWave(direction, EPol, R0, waveNumber, phase, FWHM, intensity);
	EMField.initialize();

	/// Densop initialization
	Bloch densOp = Bloch(levels, eta, alpha, beta, M, mu, gamma, omega);
	densOp.Initialize(initValues, initialPolDir);

	/// Aux fields
	Field JField = zeros(EMField.mesh, FieldType::FACE, M.dtype());
	Field MField = zeros(EMField.mesh, FieldType::EDGE, M.dtype());

	/// start with 0 for correct time counting
	int i = 0;

	/// Windows for plots
	af::Window PolVersorNormWnd("Pol versor norm");
	while (true) {

		if (i % plotInterval == 0 && plot) {
			std::cout << af::max<double>(af::abs(1.0 - af::sqrt(af::sum(af::pow(densOp.dipoleVersor.values, 2), 3)))) << std::endl;
			PolVersorNormWnd.plot(af::range(M.Nx), af::sqrt(af::sum(af::pow(densOp.dipoleVersor.values, 2), 3)).as(f32));
		}

		EMField.push(JField, densOp.calculatePolarization(polTerms), MField);
		densOp.push(EMField.EfieldPrev(), EMField.Efield(), false, true);

		i++;
	}

	std::cout << std::endl;
}

void testMaxwellBloch2D() {
	/// Sim props
	int saveInterval = 100;
	int plotInterval = 1;
	bool save = false;
	bool plot = true;
	Units units = Units(UnitSystems::NATURAL_LH);
	std::string resultsDirectory;

	/// YeeMesh constants
	int Nx = 100;
	int Ny = 100;
	int Nz = 1;
	double dx = 1.0e-9;
	double dy = 1.0e-9;
	double dz = 1.0e-9;
	double dt = (dx / Units::si_c()) / 2.0;
	double Tmax = 3.0e-15;

	/// Eletro-magnetic field constans
	double waveNumber = 2.0 * PI / (50.0*dx);
	double R0Y = Ny*dy / 2.0;
	double R0Z = Nz*dz / 2.0;
	double SigmaY = Ny*dy / 10.0;
	double SigmaZ = Nz*dz / 10.0;
	double intensity = 5.0e10;
	std::vector<bool> fieldSaveSelector = { false, true, false, true };

	/// Bloch object initialization
	int levels = 2;
	double mu12 = 1.0e-29;
	double eta = (intensity / mu12) / 1.0e12;
	double omega12 = Units::si_c() *  waveNumber;
	double gamma12 = (intensity*mu12 / Units::si_hbar()) / 100.0;
	double alpha = 0.0;
	double beta = 1.0 / (intensity*dt) / 100.0;;
	DipoleDir initialPolDir = DipoleDir::Z;
	bool useRK4 = false;
	bool useInterpolation = true;

	std::vector<std::vector<double>> mu = {
		{ 0.0, -mu12, mu12, 0.0 },
		{ -mu12, 0.0, 0.0, mu12 },
		{ mu12, 0.0, 0.0, -mu12 },
		{ 0.0, mu12, -mu12, 0.0 }
	};

	std::vector<std::vector<double>> omega = {
		{ 0.0, 0.0, 0.0, 0.0 },
		{ 0.0, omega12, 0.0, 0.0 },
		{ 0.0, 0.0, -omega12, 0.0 },
		{ 0.0, 0.0, 0.0, 0.0 },
	};

	std::vector<std::vector<double>> gamma = {
		{ 0.0, 0.0, 0.0, gamma12 },
		{ 0.0, -gamma12 / 2.0, 0.0, 0.0 },
		{ 0.0, 0.0, -gamma12 / 2.0, 0.0 },
		{ 0.0, 0.0, 0.0, -gamma12 },
	};

	std::vector<std::vector<double>> initValues = {
		{ 1.0, 0.0 },
		{ 0.0, 0.0 },
		{ 0.0, 0.0 },
		{ 0.0, 0.0 },
	};

	std::vector<double> polTerms = { 0.0, mu12, mu12, 0.0 };
	std::vector<double> dipoleForceTerms = { 0.0, mu12, mu12, 0.0 };

	/// Chose which values to save
	std::vector<bool> saveBlochReal = { true, true, true, true };
	std::vector<bool> saveBlochImag = { false, false, true, true };
	bool savePol = true;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::cout << "Converting constants to natural units" << std::endl;

	std::vector<UnitsNames> lengthUnit = { UnitsNames::LENGTH };
	std::vector<int> lengthPower = { 1 };
	std::vector<int> densityPower = { -3 };

	std::vector<UnitsNames> timeUnit = { UnitsNames::TIME };
	std::vector<int> timePower = { 1 };
	std::vector<int> frequencyPower = { -1 };

	std::vector<UnitsNames> intensityUnit = { UnitsNames::MASS , UnitsNames::LENGTH , UnitsNames::TIME , UnitsNames::CHARGE };
	std::vector<int> intensityPower = { 1, 1, -2, -1 };

	std::vector<UnitsNames> dipoleUnit = { UnitsNames::CHARGE , UnitsNames::LENGTH };
	std::vector<int> dipolePower = { 1, 1 };

	std::vector<UnitsNames> dipoleVersorUnit = { UnitsNames::MASS, UnitsNames::LENGTH, UnitsNames::TIME, UnitsNames::CHARGE };
	std::vector<int> dipoleVersorPower = { -1, -1, 1, 1 };

	/// Mesh constants
	dx = units.convertToNatural(dx, lengthUnit, lengthPower);
	dy = units.convertToNatural(dy, lengthUnit, lengthPower);
	dz = units.convertToNatural(dz, lengthUnit, lengthPower);
	dt = units.convertToNatural(dt, timeUnit, timePower);
	Tmax = units.convertToNatural(Tmax, timeUnit, timePower);

	/// EMF constants
	R0Y = units.convertToNatural(R0Y, lengthUnit, lengthPower);
	R0Z = units.convertToNatural(R0Z, lengthUnit, lengthPower);
	SigmaY = units.convertToNatural(SigmaY, lengthUnit, lengthPower);
	SigmaZ = units.convertToNatural(SigmaZ, lengthUnit, lengthPower);
	waveNumber = units.convertToNatural(waveNumber, lengthUnit, frequencyPower);
	intensity = units.convertToNatural(intensity, intensityUnit, intensityPower);

	/// Density operator constants
	for (int i = 0; i < mu.size(); i++) {
		polTerms[i] = units.convertToNatural(polTerms[i], dipoleUnit, dipolePower);
		for (int j = 0; j < mu[0].size(); j++) {
			mu[i][j] = units.convertToNatural(mu[i][j], dipoleUnit, dipolePower);
			omega[i][j] = units.convertToNatural(omega[i][j], timeUnit, frequencyPower);
			gamma[i][j] = units.convertToNatural(gamma[i][j], timeUnit, frequencyPower);
		}
	}

	eta = units.convertToNatural(eta, lengthUnit, densityPower);
	alpha = units.convertToNatural(alpha, dipoleVersorUnit, dipoleVersorPower);
	beta = units.convertToNatural(beta, dipoleVersorUnit, dipoleVersorPower);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::cout << "Starting simulation\n";

	auto ExternalEz = [&](af::array X, af::array Y, af::array Z, double t) {
		af::array E = intensity*af::sin(omega[1][1] * t - waveNumber*X)*af::exp(-af::pow(Y - R0Y, 2) / (SigmaY*SigmaY));
		return E;
	};

	auto ExternalBy = [&](af::array X, af::array Y, af::array Z, double t) {
		af::array B = intensity / units.c()*af::cos(omega[1][1] * t - waveNumber*X)*af::exp(-af::pow(Y - R0Y, 2) / (SigmaY*SigmaY));
		return B;
	};

	/// Mesh initialization
	YeeMesh M = YeeMesh(units, Nx, Ny, Nz, dx, dy, dz, dt);

	/// Field initialization
	EMF EMField = EMF(M, true);
	EMField.addExternalEfield(nullptr, nullptr, ExternalEz);
	EMField.addExternalBfield(nullptr, ExternalBy, nullptr);
	EMField.initialize();

	/// Densop initialization
	Bloch densOp = Bloch(levels, eta, alpha, beta, M, mu, gamma, omega);
	densOp.Initialize(initValues, initialPolDir);

	/// Aux fields
	Field JField = zeros(EMField.mesh, FieldType::FACE, M.dtype());
	Field MField = zeros(EMField.mesh, FieldType::EDGE, M.dtype());

	/// start with 0 for correct time counting
	int i = 0;

	if (save && resultsDirectory.empty()) {
		/// Create dir
		try {
			resultsDirectory = generateDirectoryPath("Maxwell-Bloch");
			MKDirectory(resultsDirectory);
		}
		catch (std::invalid_argument e) {
			std::cout << e.what() << std::endl;
			system("pause");
			return;
		}
	}

	///Initial saves
	if (save) {
		M.save(resultsDirectory, true);
		densOp.saveProperties(resultsDirectory, true);
	}

	/// Windows for plots
	af::Window EZWnd("EZ");
	af::Window PolZWnd("PolZ");
	af::Window RhoWnd("Rho");
	while (true) {
		std::cout << '\r' << i*dt / Tmax * 100 << std::flush;

		if (i % saveInterval == 0 && save) {
			densOp.saveToAF(resultsDirectory, saveBlochReal, saveBlochImag);
			EMField.saveToAF(resultsDirectory, fieldSaveSelector, true);
		}

		if (i % plotInterval == 0 && plot) {
			EZWnd.image(normalizeForImage(EMField.Efield().values.as(f32)(SPAN2, 0, 2)));
			PolZWnd.image(normalizeForImage((densOp.calculatePolarization(polTerms).values.as(f32))(SPAN2, 0, 2)));
			RhoWnd.image(normalizeForImage(af::real(densOp.Rho[0][0]).as(f32)(SPAN2, 0, 0)));
		}

		EMField.push(JField, densOp.calculatePolarization(polTerms), MField);
		densOp.push(EMField.EfieldPrev(), EMField.Efield(), useRK4, useInterpolation);

		i++;
	}

	std::cout << std::endl;
}

void testMaxwellBloch3D() {

	/// Sim props
	int saveInterval = 2;
	int plotInterval = 1;
	bool save = true;
	bool plot = false;
	Units units = Units(UnitSystems::NATURAL_LH);
	std::string resultsDirectory;

	/// YeeMesh constants
	int Nx = 100;
	int Ny = 100;
	int Nz = 100;
	double dx = 1.0e-8;
	double dy = 1.0e-8;
	double dz = 1.0e-8;
	double dt = (dx / Units::si_c()) / 2.0;
	double Tmax = 2.5e-15;

	/// Eletro-magnetic field constans
	PropagationDir direction = PropagationDir::X_P;
	EFieldPol EPol = EFieldPol::Z;
	double R0 = Nx*dx / 4.0;
	double waveNumber = 2.0 * PI / (50.0*dx);
	double phase = 0.0;
	double FWHM = Nx*dx / 10.0;
	double intensity = 1.0e10;
	std::vector<bool> fieldSaveSelector = { false, true, false, false };

	/// Bloch object initialization
	int levels = 2;
	double mu12 = 1.0e-29;
	double eta = (intensity / mu12) / 1.0e12;
	double omega12 = Units::si_c() *  waveNumber;
	double gamma12 = (intensity*mu12 / Units::si_hbar()) / 100.0;
	double alpha = 0.0;
	double beta = 1.0 / (intensity*dt) / 100.0;;
	DipoleDir initialPolDir = DipoleDir::Z;
	bool useRK4 = false;
	bool useInterpolation = true;

	std::vector<std::vector<double>> mu = {
		{ 0.0, -mu12, mu12, 0.0 },
		{ -mu12, 0.0, 0.0, mu12 },
		{ mu12, 0.0, 0.0, -mu12 },
		{ 0.0, mu12, -mu12, 0.0 }
	};

	std::vector<std::vector<double>> omega = {
		{ 0.0, 0.0, 0.0, 0.0 },
		{ 0.0, omega12, 0.0, 0.0 },
		{ 0.0, 0.0, -omega12, 0.0 },
		{ 0.0, 0.0, 0.0, 0.0 },
	};

	std::vector<std::vector<double>> gamma = {
		{ 0.0, 0.0, 0.0, gamma12 },
		{ 0.0, -gamma12 / 2.0, 0.0, 0.0 },
		{ 0.0, 0.0, -gamma12 / 2.0, 0.0 },
		{ 0.0, 0.0, 0.0, -gamma12 },
	};

	std::vector<std::vector<double>> initValues = {
		{ 1.0, 0.0 },
		{ 0.0, 0.0 },
		{ 0.0, 0.0 },
		{ 0.0, 0.0 },
	};

	std::vector<double> polTerms = { 0.0, mu12, mu12, 0.0 };
	std::vector<double> dipoleForceTerms = { 0.0, mu12, mu12, 0.0 };

	/// Chose which values to save
	std::vector<bool> saveBlochReal = { true, true, true, true };
	std::vector<bool> saveBlochImag = { false, true, true, false };

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::cout << "Converting constants to natural units" << std::endl;

	std::vector<UnitsNames> lengthUnit = { UnitsNames::LENGTH };
	std::vector<int> lengthPower = { 1 };
	std::vector<int> densityPower = { -3 };

	std::vector<UnitsNames> timeUnit = { UnitsNames::TIME };
	std::vector<int> timePower = { 1 };
	std::vector<int> frequencyPower = { -1 };

	std::vector<UnitsNames> intensityUnit = { UnitsNames::MASS , UnitsNames::LENGTH , UnitsNames::TIME , UnitsNames::CHARGE };
	std::vector<int> intensityPower = { 1, 1, -2, -1 };

	std::vector<UnitsNames> dipoleUnit = { UnitsNames::CHARGE , UnitsNames::LENGTH };
	std::vector<int> dipolePower = { 1, 1 };

	std::vector<UnitsNames> dipoleVersorUnit = { UnitsNames::MASS, UnitsNames::LENGTH, UnitsNames::TIME, UnitsNames::CHARGE };
	std::vector<int> dipoleVersorPower = { -1, -1, 1, 1 };

	/// Mesh constants
	dx = units.convertToNatural(dx, lengthUnit, lengthPower);
	dy = units.convertToNatural(dy, lengthUnit, lengthPower);
	dz = units.convertToNatural(dz, lengthUnit, lengthPower);
	dt = units.convertToNatural(dt, timeUnit, timePower);
	Tmax = units.convertToNatural(Tmax, timeUnit, timePower);

	/// EMF constants
	R0 = units.convertToNatural(R0, lengthUnit, lengthPower);
	waveNumber = units.convertToNatural(waveNumber, lengthUnit, frequencyPower);
	FWHM = units.convertToNatural(FWHM, lengthUnit, lengthPower);
	intensity = units.convertToNatural(intensity, intensityUnit, intensityPower);

	/// Density operator constants
	for (int i = 0; i < mu.size(); i++) {
		polTerms[i] = units.convertToNatural(polTerms[i], dipoleUnit, dipolePower);
		for (int j = 0; j < mu[0].size(); j++) {
			mu[i][j] = units.convertToNatural(mu[i][j], dipoleUnit, dipolePower);
			omega[i][j] = units.convertToNatural(omega[i][j], timeUnit, frequencyPower);
			gamma[i][j] = units.convertToNatural(gamma[i][j], timeUnit, frequencyPower);
		}
	}

	eta = units.convertToNatural(eta, lengthUnit, densityPower);
	alpha = units.convertToNatural(alpha, dipoleVersorUnit, dipoleVersorPower);
	beta = units.convertToNatural(beta, dipoleVersorUnit, dipoleVersorPower);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::cout << "Starting simulation\n";

	/// Mesh initialization
	YeeMesh M = YeeMesh(units, Nx, Ny, Nz, dx, dy, dz, dt);

	/// Field initialization
	EMF EMField = EMF(M, true);
	EMField.addGaussianPulsePlaneWave(direction, EPol, R0, waveNumber, phase, FWHM, intensity);
	EMField.initialize();

	/// Densop initialization
	Bloch densOp = Bloch(levels, eta, alpha, beta, M, mu, gamma, omega);
	densOp.Initialize(initValues, initialPolDir);

	/// Aux fields
	Field JField = zeros(EMField.mesh, FieldType::FACE, M.dtype());
	Field MField = zeros(EMField.mesh, FieldType::EDGE, M.dtype());

	/// start with 0 for correct time counting
	int i = 0;

	if (save && resultsDirectory.empty()) {
		/// Create dir
		try {
			resultsDirectory = generateDirectoryPath("Maxwell-Bloch-3D");
			MKDirectory(resultsDirectory);
		}
		catch (std::invalid_argument e) {
			std::cout << e.what() << std::endl;
			system("pause");
			return;
		}
	}

	///Initial saves
	if (save) {
		M.save(resultsDirectory, true);
		densOp.saveProperties(resultsDirectory, true);
	}

	/// Windows for plots
	af::Window PolZWnd("PolZ");
	af::Window EZWnd("EZ");
	while (i*dt <= Tmax) {
		std::cout << '\r' << i*dt / Tmax * 100 << std::flush;

		if (i % saveInterval == 0 && save) {
			densOp.saveToAF(resultsDirectory, saveBlochReal, saveBlochImag);
			densOp.savePolToAF(resultsDirectory, polTerms, true);
			EMField.saveToAF(resultsDirectory, fieldSaveSelector, true);
		}

		if (i % plotInterval == 0 && plot) {
			//PolZWnd.image(normalizeForImage((densOp.calculatePolarization(polTerms).values.as(f32))(SPAN2, 0, 2)));
			EZWnd.image(normalizeForImage((EMField.Efield().values.as(f32))(SPAN2, 0, 2)));
			//EZWnd.plot(af::range(M.Nx), (EMField.Efield().values.as(f32))(SPAN, 0, 0, 2));
			//PolZWnd.plot(af::range(M.Nx), af::real(densOp.Rho[3][0]).as(f32)(SPAN, 0, 0, 0));
		}

		EMField.push(JField, densOp.calculatePolarization(polTerms), MField);
		densOp.push(EMField.EfieldPrev(), EMField.Efield(), useRK4, useInterpolation);

		i++;
	}

	std::cout << std::endl;
}

void MaxwellBloch3DConfined() {
	/// Sim props
	int saveInterval = 5;
	int plotInterval = 1;
	bool save = true;
	bool plot = false;
	Units units = Units(UnitSystems::NATURAL_LH);
	std::string resultsDirectory = "E:\\Joao Paulo\\Results\\Maxwell-Bloch-3D-Thin2";

	/// YeeMesh constants
	int Nx = 101;
	int Ny = 1001;
	int Nz = 11;
	double dx = 1.0e-8;
	double dy = 1.0e-8;
	double dz = 1.0e-8;
	double dt = (dx / Units::si_c()) / 2.0;
	double Tmax = 2.0e-15;

	/// Eletro-magnetic field constans
	double waveNumber = 2.0 * PI / (50.0*dx);
	double R0Y = Ny*dy / 2.0;
	double R0Z = Nz*dz / 2.0;
	double SigmaY = Ny*dy / 10.0;
	double SigmaZ = Nz*dz / 10.0;
	double intensity = 1.0e10;
	std::vector<bool> fieldSaveSelector = { false, true, false, false };

	/// Bloch object initialization
	int levels = 2;
	double mu12 = 1.0e-29;
	double eta = (intensity / mu12) / 1.0e12;
	double omega12 = Units::si_c() *  waveNumber;
	double gamma12 = (intensity*mu12 / Units::si_hbar()) / 100.0;
	double alpha = 0.0;
	double beta = 1.0 / (intensity*dt) / 100.0;;
	DipoleDir initialPolDir = DipoleDir::Z;
	bool useRK4 = false;
	bool useInterpolation = true;

	std::vector<std::vector<double>> mu = {
		{ 0.0, -mu12, mu12, 0.0 },
		{ -mu12, 0.0, 0.0, mu12 },
		{ mu12, 0.0, 0.0, -mu12 },
		{ 0.0, mu12, -mu12, 0.0 }
	};

	std::vector<std::vector<double>> omega = {
		{ 0.0, 0.0, 0.0, 0.0 },
		{ 0.0, omega12, 0.0, 0.0 },
		{ 0.0, 0.0, -omega12, 0.0 },
		{ 0.0, 0.0, 0.0, 0.0 },
	};

	std::vector<std::vector<double>> gamma = {
		{ 0.0, 0.0, 0.0, gamma12 },
		{ 0.0, -gamma12 / 2.0, 0.0, 0.0 },
		{ 0.0, 0.0, -gamma12 / 2.0, 0.0 },
		{ 0.0, 0.0, 0.0, -gamma12 },
	};

	std::vector<std::vector<double>> initValues = {
		{ 1.0, 0.0 },
		{ 0.0, 0.0 },
		{ 0.0, 0.0 },
		{ 0.0, 0.0 },
	};

	std::vector<double> polTerms = { 0.0, mu12, mu12, 0.0 };
	std::vector<double> dipoleForceTerms = { 0.0, mu12, mu12, 0.0 };

	/// Chose which values to save
	std::vector<bool> saveBlochReal = { true, true, true, true };
	std::vector<bool> saveBlochImag = { false, true, true, false };

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::cout << "Converting constants to natural units" << std::endl;

	std::vector<UnitsNames> lengthUnit = { UnitsNames::LENGTH };
	std::vector<int> lengthPower = { 1 };
	std::vector<int> densityPower = { -3 };

	std::vector<UnitsNames> timeUnit = { UnitsNames::TIME };
	std::vector<int> timePower = { 1 };
	std::vector<int> frequencyPower = { -1 };

	std::vector<UnitsNames> intensityUnit = { UnitsNames::MASS , UnitsNames::LENGTH , UnitsNames::TIME , UnitsNames::CHARGE };
	std::vector<int> intensityPower = { 1, 1, -2, -1 };

	std::vector<UnitsNames> dipoleUnit = { UnitsNames::CHARGE , UnitsNames::LENGTH };
	std::vector<int> dipolePower = { 1, 1 };

	std::vector<UnitsNames> dipoleVersorUnit = { UnitsNames::MASS, UnitsNames::LENGTH, UnitsNames::TIME, UnitsNames::CHARGE };
	std::vector<int> dipoleVersorPower = { -1, -1, 1, 1 };

	/// Mesh constants
	dx = units.convertToNatural(dx, lengthUnit, lengthPower);
	dy = units.convertToNatural(dy, lengthUnit, lengthPower);
	dz = units.convertToNatural(dz, lengthUnit, lengthPower);
	dt = units.convertToNatural(dt, timeUnit, timePower);
	Tmax = units.convertToNatural(Tmax, timeUnit, timePower);

	/// EMF constants
	R0Y = units.convertToNatural(R0Y, lengthUnit, lengthPower);
	R0Z = units.convertToNatural(R0Z, lengthUnit, lengthPower);
	SigmaY = units.convertToNatural(SigmaY, lengthUnit, lengthPower);
	SigmaZ = units.convertToNatural(SigmaZ, lengthUnit, lengthPower);
	waveNumber = units.convertToNatural(waveNumber, lengthUnit, frequencyPower);
	intensity = units.convertToNatural(intensity, intensityUnit, intensityPower);

	/// Density operator constants
	for (int i = 0; i < mu.size(); i++) {
		polTerms[i] = units.convertToNatural(polTerms[i], dipoleUnit, dipolePower);
		for (int j = 0; j < mu[0].size(); j++) {
			mu[i][j] = units.convertToNatural(mu[i][j], dipoleUnit, dipolePower);
			omega[i][j] = units.convertToNatural(omega[i][j], timeUnit, frequencyPower);
			gamma[i][j] = units.convertToNatural(gamma[i][j], timeUnit, frequencyPower);
		}
	}

	eta = units.convertToNatural(eta, lengthUnit, densityPower);
	alpha = units.convertToNatural(alpha, dipoleVersorUnit, dipoleVersorPower);
	beta = units.convertToNatural(beta, dipoleVersorUnit, dipoleVersorPower);

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	std::cout << "Starting simulation\n";

	//auto ExternalEz = [&](af::array X, af::array Y, af::array Z, double t) {
	//	af::array E = intensity*af::sin(omega[1][1] * t - waveNumber*X)*af::exp(-af::pow(Y - R0Y, 2) / (SigmaY*SigmaY))*af::exp(-af::pow(Z - R0Z, 2) / (SigmaZ*SigmaZ));
	//	return E;
	//};

	//auto ExternalBy = [&](af::array X, af::array Y, af::array Z, double t) {
	//	af::array B = intensity / units.c()*af::cos(omega[1][1] * t - waveNumber*X)*af::exp(-af::pow(Y - R0Y, 2) / (SigmaY*SigmaY))*af::exp(-af::pow(Z - R0Z, 2) / (SigmaZ*SigmaZ));
	//	return B;
	//};

	auto ExternalEz = [&](af::array X, af::array Y, af::array Z, double t) {
		af::array E = intensity*af::sin(omega[1][1] * t - waveNumber*X)*af::exp(-af::pow(Y - R0Y, 2) / (SigmaY*SigmaY));
		return E;
	};

	auto ExternalBy = [&](af::array X, af::array Y, af::array Z, double t) {
		af::array B = intensity / units.c()*af::cos(omega[1][1] * t - waveNumber*X)*af::exp(-af::pow(Y - R0Y, 2) / (SigmaY*SigmaY));
		return B;
	};

	/// Mesh initialization
	YeeMesh M = YeeMesh(units, Nx, Ny, Nz, dx, dy, dz, dt);

	/// Field initialization
	EMF EMField = EMF(M, true);
	EMField.addExternalEfield(nullptr, nullptr, ExternalEz);
	EMField.addExternalBfield(nullptr, ExternalBy, nullptr);
	EMField.initialize();

	/// Densop initialization
	Bloch densOp = Bloch(levels, eta, alpha, beta, M, mu, gamma, omega);
	densOp.Initialize(initValues, initialPolDir);

	/// Aux fields
	Field JField = zeros(EMField.mesh, FieldType::FACE, M.dtype());
	Field MField = zeros(EMField.mesh, FieldType::EDGE, M.dtype());

	/// start with 0 for correct time counting
	int i = 0;

	if (save && resultsDirectory.empty()) {
		/// Create dir
		try {
			resultsDirectory = generateDirectoryPath("Maxwell-Bloch-3D");
			MKDirectory(resultsDirectory);
		}
		catch (std::invalid_argument e) {
			std::cout << e.what() << std::endl;
			system("pause");
			return;
		}
	}

	///Initial saves
	if (save) {
		M.save(resultsDirectory, true);
		densOp.saveProperties(resultsDirectory, true);
	}

	/// Windows for plots
	af::Window PolZWnd("PolZ");
	af::Window EZWnd("EZ");
	while (i*dt <= Tmax+dt) {
		std::cout << '\r' << i*dt / Tmax * 100 << std::flush;

		if (i % saveInterval == 0 && save) {
			densOp.saveToAF(resultsDirectory, saveBlochReal, saveBlochImag);
			densOp.savePolToAF(resultsDirectory, polTerms, true);
			EMField.saveToAF(resultsDirectory, fieldSaveSelector, true);
		}

		if (i % plotInterval == 0 && plot) {
			//PolZWnd.image(normalizeForImage((densOp.calculatePolarization(polTerms).values.as(f32))(SPAN2, 0, 2)));
			EZWnd.image(normalizeForImage((EMField.Efield().values.as(f32))(SPAN2, 0, 2)));
			//EZWnd.plot(af::range(M.Nx), (EMField.Efield().values.as(f32))(SPAN, 0, 0, 2));
			//PolZWnd.plot(af::range(M.Nx), af::real(densOp.Rho[3][0]).as(f32)(SPAN, 0, 0, 0));
		}

		EMField.push(JField, densOp.calculatePolarization(polTerms), MField);
		densOp.push(EMField.EfieldPrev(), EMField.Efield(), useRK4, useInterpolation);

		i++;
	}

	std::cout << std::endl;
}

void main(int argc, char *argv[]) {

	Bloch2Level();

	//testABC();

	//testUPML1D();

	//testSimpleField();

	//testFieldSource1D();

	//testExternalFields();

	//speedUpMetrics();

	//testTrace();

	//testDipoleVersorNorm();

	//testMaxwellBloch2D();

	//testMaxwellBloch3D();

	//MaxwellBloch3DConfined();

	system("pause");
}
